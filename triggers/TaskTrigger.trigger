trigger TaskTrigger on VDLC_Task__c(after insert, after update) {
    // 041019 - T-000669 - VennScience_BFL_Amruta - Check if this is an after insert event
    if(Trigger.isInsert && Trigger.isAfter) {
        // 041019 - T-000669 - VennScience_BFL_Amruta - Call Handler
        TaskTriggerHandler.filterTaskRecords(Trigger.New);
    } // End of after insert if
}