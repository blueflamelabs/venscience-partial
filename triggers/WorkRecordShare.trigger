trigger WorkRecordShare on agf__ADM_Work__c (after insert,after update) {
    ShareProjectLabelRecordCTRL.WorkRecordShare(trigger.new);

    // 270619 - T - 00363 - Share work record with Partner Users
    if(Trigger.isAfter && Trigger.isInsert) {
    	// System.debug('Inside after insert');
    	ShareProjectLabelRecordCTRL.WorkRecordShareForPartnerUsers(Trigger.New, new Map<Id,agf__ADM_Work__c>());
    }
    // 270619 - T - 00363 - Share work record with Partner Users
    if(Trigger.isAfter && Trigger.isUpdate) {
    	// System.debug('Inside after update');
    	// System.debug('Trigger.New========='+Trigger.New);
    	ShareProjectLabelRecordCTRL.WorkRecordShareForPartnerUsers(Trigger.New,Trigger.oldMap);
    }
}