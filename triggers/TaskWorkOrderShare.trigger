trigger TaskWorkOrderShare on agf__ADM_Task__c (after insert,after update) {

	// 270619 - T - 00278 - Check if is after insert trigger
	if(Trigger.isAfter && Trigger.isInsert) {
		ShareProjectLabelRecordCTRL.TaskWorkOrderRecordShare(Trigger.new);
		// 100719 - T - 00278 - Call method to share Content Document(files) with Community users
		ShareProjectLabelRecordCTRL.updateFileVisibility(Trigger.new, new Map<Id,agf__ADM_Task__c>());
	}
	// 270619 - T - 00278 - Check if is after update trigger
	if(Trigger.isAfter && Trigger.isUpdate) {
		ShareProjectLabelRecordCTRL.filterTaskRecords(Trigger.new,Trigger.oldMap);
		// 100719 - T - 00278 - Call method to share Content Document(files) with Community users
		ShareProjectLabelRecordCTRL.updateFileVisibility(Trigger.new, Trigger.oldMap);
	}
    
}