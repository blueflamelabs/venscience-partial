trigger ShareProjectUserRecord on User (after insert,after update) {
    
    List<ShareProjectLabelRecordCTRL.UserWrapper> listUserWrapper = new List<ShareProjectLabelRecordCTRL.UserWrapper>();
    Map<Id,ShareProjectLabelRecordCTRL.UserWrapper> mapuserIdVSOldUserWrapper = new Map<Id,ShareProjectLabelRecordCTRL.UserWrapper>();
    // @Reminder: Uncomment below line
     ShareProjectLabelRecordCTRL.ShareRecordUserAfterInsert(trigger.newMap.keySet());
     // T-00705 - 270919 - VennScience_BFL_Amruta - Call handler method
     if(Trigger.isInsert && Trigger.isAfter) {
         listUserWrapper = new List<ShareProjectLabelRecordCTRL.UserWrapper>();
         mapuserIdVSOldUserWrapper = new Map<Id,ShareProjectLabelRecordCTRL.UserWrapper>();
         
         // T-00705 - 270919 - VennScience_BFL_Amruta - Iterate over Trigger.new list
         for(User userRec : Trigger.new) {
             listUserWrapper.add(new ShareProjectLabelRecordCTRL.UserWrapper(userRec.Id,userRec.ContactId,userRec.ProfileId,userRec.IsActive));
         }
         
         // T-00705 - 270919 - VennScience_BFL_Amruta Merge data into Container Wrapper object
         ShareProjectLabelRecordCTRL.ContainerWrapper containerWrapperObj = new ShareProjectLabelRecordCTRL.ContainerWrapper(
             listUserWrapper , mapuserIdVSOldUserWrapper );
         // // T-00705 - 270919 - VennScience_BFL_Amruta - Call Handler method
         ShareProjectLabelRecordCTRL.filterUsers(JSON.serialize(containerWrapperObj));
     }
     // T-00705 - 270919 - VennScience_BFL_Amruta - Check if it is an after update event
     if(Trigger.isUpdate && Trigger.isAfter) {
         listUserWrapper = new List<ShareProjectLabelRecordCTRL.UserWrapper>();
         mapuserIdVSOldUserWrapper = new Map<Id,ShareProjectLabelRecordCTRL.UserWrapper>();
         // T-00705 - 270919 - VennScience_BFL_Amruta Iterate over Trigger.new list
         for(User userRec : Trigger.new) {
             listUserWrapper.add(new ShareProjectLabelRecordCTRL.UserWrapper(userRec.Id,userRec.ContactId,userRec.ProfileId,userRec.IsActive));
         }
         // T-00705 - 270919 - VennScience_BFL_Amruta Iterate over Trigger.old list
         for(User userRec : Trigger.old) {
             mapuserIdVSOldUserWrapper.put(userRec.Id, new ShareProjectLabelRecordCTRL.UserWrapper(userRec.Id,userRec.ContactId,userRec.ProfileId,userRec.IsActive));
         }
         // T-00705 - 270919 - VennScience_BFL_Amruta Merge data into Container Wrapper object
         ShareProjectLabelRecordCTRL.ContainerWrapper containerWrapperObj = new ShareProjectLabelRecordCTRL.ContainerWrapper(
             listUserWrapper , mapuserIdVSOldUserWrapper );
         // T-00705 - 270919 - VennScience_BFL_Amruta - Call Handler method
         ShareProjectLabelRecordCTRL.filterUsers(JSON.serialize(containerWrapperObj ));
     }
}