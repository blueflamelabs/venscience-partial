trigger DeliverableRecordShare on Deliverable__c (after insert,after update, before update) {
    ShareProjectLabelRecordCTRL.DeliverableRecordShare(trigger.new);
    
    // 141019 - T-000669 - VennScience_BFL_Monali - Check if this is an before insert event.
    if(Trigger.isAfter && Trigger.isInsert) {
        // 141019 - T-000669 - VennScience_BFL_Monali - Call Handler
        DeliverableTriggerHandler.filterDeliverableRecords(Trigger.New);
    }// End of after insert if
    
    // 141019 - T-000669 - VennScience_BFL_Monali - Check if this is an before update event.
    if(Trigger.isBefore && Trigger.isUpdate) {
        // 141019 - T-000669 - VennScience_BFL_Monali - Call Handler
        DeliverableTriggerHandler.filterUpdatedDeliverableRecords(Trigger.New, Trigger.oldMap);
    }// End of before update if
}