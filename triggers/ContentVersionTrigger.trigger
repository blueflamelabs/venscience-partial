trigger ContentVersionTrigger on ContentVersion (after insert) {

    // T- 00284 - VennScience_BFL_Amruta - Check if it is an After Insert call
    if(Trigger.isInsert && Trigger.isAfter) {
        // T- 00284 - VennScience_BFL_Amruta - Call trigger handler
        ContentVersionTriggerHandler.updateFileVisibilityForTasks(Trigger.New); 
    }
}