trigger ShareProjectLabelRecord on Project_Contact_Role__c (after insert, after update, after delete) {
    // T-00281 - 050819 - VennScience_BFL_Amruta - Check if Trigger.New is not null
    if(Trigger.New != null) {
        ShareProjectLabelRecordCTRL.ShareRecordAfterInsert(trigger.new);
    }    
    // T-00281 - 050819 - VennScience_BFL_Amruta - Call handler method to create Share rows
    if(Trigger.isInsert && Trigger.isAfter) {
        // 300919 - VennScience_BFL_Amruta - Commented below code
        ShareProjectLabelRecordCTRL.addToChatterGroup(Trigger.New);
        // T-00705 - 260919 - VennScience_BFL_Amruta - Call handler method
        ShareProjectLabelRecordCTRL.shareRecordsWithProjConRole(Trigger.New);
    }
    // T-00281 - 050819 - VennScience_BFL_Amruta - Call handler method
    if(Trigger.isUpdate && Trigger.isAfter) {
        ShareProjectLabelRecordCTRL.filterProjectContactRoleRecords(Trigger.New,Trigger.oldMap);
        // T-00705 - 260919 - VennScience_BFL_Amruta - Call handler method to add/remove Share rows
        //ShareProjectLabelRecordCTRL.filterProjConRoleForSharing(Trigger.New,Trigger.oldMap);
    }
    // T-00281 - 260819 - VennScience_BFL_Amruta - Call handler method
    if(Trigger.isDelete && Trigger.isAfter) {
        // 300919 - VennScience_BFL_Amruta - Commented below code
        ShareProjectLabelRecordCTRL.removeFromChatterGroup(Trigger.Old);
        // T-00705 - 260919 - VennScience_BFL_Amruta - Call handler method to remove Share rows
        ShareProjectLabelRecordCTRL.removeShareRowsForProjConRole(Trigger.Old,false);
    }
}