/****
    
****/

trigger CreateProject on Opportunity (After Update,after insert,before update) {
    List<Opportunity> Opportunitylist =new List<Opportunity>();
    List<Opportunity> OpportunitylistUpdate =new List<Opportunity>();
    List<Project__c> ProjectList =new List<Project__c>();
    List<Deliverable__c> DeliverableList =new List<Deliverable__c>();
    List<ContentDocumentLink> FileList = new List<ContentDocumentLink>();
    List<Quota__c> QuotaList =new List<Quota__c>();
    map<string,list<opportunity>> mapOfStrQuota = new map<string,list<opportunity>>();
    map<string,string> mapOppIds = new map<string,string>();
    set<string> setOfQuotaIds = new  set<string>();
    /* Create new project when opportunity make close won */

    // 090719 - T-00402 - VennScience_BFL_Amruta - Call handler class in order to create Project for Opportunity on update
    if(trigger.isUpdate && trigger.isAfter) {
        // Call handler method
        CreateProjectHandler objHandler = new CreateProjectHandler();
        objHandler.insertProjectForOpp(trigger.new, trigger.oldMap);
    }
    // 100719 - T-00402 - VennScience_BFL_Amruta - Call handler class in order to create Project for Opportunity on insert
    if(trigger.isInsert && trigger.isAfter) {
        // Call handler method
        CreateProjectHandler objHandler = new CreateProjectHandler();
        objHandler.insertProjectForOpp(trigger.new, new Map<Id,Opportunity>());
    }
    
    /* 090719 - T-00402 - VennScience_BFL_Amruta - commented code
      if(trigger.isInsert && trigger.isAfter)
    for(Opportunity opp :trigger.new){
        string qutrStr = '';
            if(1<=opp.closedate.month() && 3>=opp.closedate.month())
                qutrStr = 'Q1 '+string.valueof(opp.closedate.Year());
            if(4<=opp.closedate.month() && 6>=opp.closedate.month())
                qutrStr = 'Q2 '+string.valueof(opp.closedate.Year());
            if(7<=opp.closedate.month() && 9>=opp.closedate.month())
                qutrStr = 'Q3 '+string.valueof(opp.closedate.Year());
            if(10<=opp.closedate.month() && 12>=opp.closedate.month())
                qutrStr = 'Q4 '+string.valueof(opp.closedate.Year());
            if(qutrStr!='' && opp.stagename != 'Closed Lost'){
                if(!mapOfStrQuota.keyset().contains(qutrStr))
                    mapOfStrQuota.put(qutrStr,new list<opportunity>());
                mapOfStrQuota.get(qutrStr).add(opp);
            }
            
    }
    commented upto here */

    /* Link Quota to opportunity */
               
    /*for(Quota__c quota : [select id,Quarter__c,ownerid from Quota__c where Quarter__c in:mapOfStrQuota.keyset()]){
        for(opportunity opp : mapOfStrQuota.get(quota.Quarter__c)){
            if(opp.ownerid == quota.ownerid){
                setOfQuotaIds.add(quota.id);
                opportunity opp1 = new opportunity(id = opp.id);
                opp1.Quota__c = quota.id;
                setOfQuotaIds.add(quota.id);
                OpportunitylistUpdate.add(opp1);
            }            
        }
    }*/
    /* Update booked amount on Quota */
   /* if(OpportunitylistUpdate.size()>0){
        update OpportunitylistUpdate;        
    }
    set<string> oppids = new set<string>();
    if(trigger.isUpdate && trigger.isbefore){
        for(Opportunity opp :trigger.new){
            if(opp.stagename  != trigger.oldmap.get(opp.id).StageName && opp.stagename=='Closed Lost' ){
                    setOfQuotaIds.add(opp.quota__c);
                    opp.quota__c = null;
                    oppids.add(opp.id);
            }
        }
    }
    for(Quota__c quota : [select id,Booked__c,(select id,amount from Opportunities__r where stagename = 'Closed Won' and id not in:oppids ) from Quota__c where id in:setOfQuotaIds]){
        decimal amount = 0;
        for(opportunity opp : quota.Opportunities__r){
            if(opp.amount>0)
                amount +=opp.amount;
        }
        quota.Booked__c = amount;
        QuotaList.add(quota);
    }    
    if(QuotaList.size()>0)
        update QuotaList;  */
    
    
}