/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        090819         VennScience_BFL_Amruta     
**********************************************************************************************************************************************************/     
trigger TeamMemberTrigger on agf__ADM_Scrum_Team_Member__c (after insert, after update, after delete) {
   
   // Check if this is an after insert event
   if(Trigger.isAfter && Trigger.isInsert) {
       TeamMemberTriggerHandler.addToChatterGroup(Trigger.New);
   }
   // Check if this is an after update event
   if(Trigger.isAfter && Trigger.isUpdate) {
       TeamMemberTriggerHandler.filterTeamMemberRecords(Trigger.New,Trigger.oldMap);
   }
   // Check if this is an after delete event
   if(Trigger.isAfter && Trigger.isDelete) {
       TeamMemberTriggerHandler.removeFromChatterGroup(Trigger.Old);
   }
}