trigger ProjectTrigger on Project__c (after insert, after update, before delete) {
    // Check if it is an after insert event
    if(Trigger.isInsert && Trigger.isAfter) {
        ProjectTriggerHandler.filterProjectRecords(Trigger.New, new Map<Id,Project__c>());
        //System.debug('==AFTER INSERT ==');
        ProjectTriggerHandler.shareProjectRecords(Trigger.New, new Map<Id,Project__c>());
    }
    // Check if it is an after update event
    if(Trigger.isUpdate && Trigger.isAfter) {
        // Call handler method only if it is not a recursive call
        if(!ProjectTriggerHandler.isRecursiveCall) {
            //System.debug('After update called');
            ProjectTriggerHandler.filterProjectRecords(Trigger.New, Trigger.oldMap);
        } // End of if
        //System.debug('==AFTER UPDATE ==');
        ProjectTriggerHandler.shareProjectRecords(Trigger.New, Trigger.oldMap);
    }
}