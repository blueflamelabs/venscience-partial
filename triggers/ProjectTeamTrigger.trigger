trigger ProjectTeamTrigger on Project_Team__c (after insert, after update) {
    // 071019 - T-000669 - VennScience_BFL_Amruta - Check if this is an after insert event
    if(Trigger.isInsert && Trigger.isAfter) {
        // 071019 - T-000669 - VennScience_BFL_Amruta - Call Handler
        ProjectTeamTriggerHandler.filterProjectTeamRecords(Trigger.New, new Map<Id,Project_Team__c>());
    } // End of after insert if
    // 091019 - T-000669 - VennScience_BFL_Amruta - Check if this is an after update event
    if(Trigger.isUpdate && Trigger.isAfter) {
        // 091019 - T-000669 - VennScience_BFL_Amruta - Call Handler
        ProjectTeamTriggerHandler.filterProjectTeamRecords(Trigger.New, Trigger.oldMap);
    } // End of after update if
}