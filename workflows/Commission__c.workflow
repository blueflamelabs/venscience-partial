<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Spiff</fullName>
        <field>Spiff__c</field>
        <formula>Quota__r.Spiff__c</formula>
        <name>Set Spiff %</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Commission %26 Spiff %25</fullName>
        <actions>
            <name>Set_Spiff</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Commission__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets these values behind the scenes to calculate the Commission Amount, required in order to have Rollup Summary of Commission Amounts on the Quota.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
