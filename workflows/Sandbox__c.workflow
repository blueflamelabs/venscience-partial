<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Project_Default_Sandbox</fullName>
        <field>Sandbox_Username__c</field>
        <formula>Username__c</formula>
        <name>Update Project Default Sandbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Project__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Default_Sandbox_Password</fullName>
        <field>Sandbox_Password__c</field>
        <formula>Password__c</formula>
        <name>Update Project Default Sandbox Password</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Project__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Default_Sandbox_Token</fullName>
        <field>Sandbox_Security_Token__c</field>
        <formula>Security_Token__c</formula>
        <name>Update Project Default Sandbox Token</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Project__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sandbox_ID</fullName>
        <field>Sandbox_Org_ID__c</field>
        <formula>Org_ID__c</formula>
        <name>Update Sandbox ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Project__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Set_as_Project_Default</fullName>
        <field>Set_as_Project_Default_Sandbox__c</field>
        <literalValue>0</literalValue>
        <name>Update Set as Project Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Project Default Sandbox</fullName>
        <actions>
            <name>Update_Project_Default_Sandbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Default_Sandbox_Password</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Default_Sandbox_Token</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Sandbox_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Set_as_Project_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sandbox__c.Set_as_Project_Default_Sandbox__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
