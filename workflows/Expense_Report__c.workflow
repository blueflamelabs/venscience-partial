<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reminder_Expense_Report_Due_Today</fullName>
        <description>Reminder: Expense Report Due Today</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_Submit_Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Submit_Expense_Report</fullName>
        <description>Reminder: Submit Expense Report</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_Submit_Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Send_Manager_Action_Email</fullName>
        <description>Send Manager Action Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Expense_Report_Manager_Reaction</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Expense_Report_Name</fullName>
        <field>Name</field>
        <formula>IF(AND(TEXT(Month(Start_Date__c))!=&apos;10&apos;, TEXT(Month(Start_Date__c))!=&apos;11&apos;, TEXT(Month(Start_Date__c))!=&apos;12&apos;),

Owner:User.Alias &amp; &apos; - &apos; &amp; TEXT(Year(Start_Date__c))&amp;&apos;/&apos;&amp;&apos;0&apos; &amp; TEXT(Month(Start_Date__c)),
Owner:User.Alias &amp; &apos; - &apos; &amp; TEXT(Year(Start_Date__c))&amp;&apos;/&apos;&amp; TEXT(Month(Start_Date__c))
)</formula>
        <name>Update Expense Report Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Open</fullName>
        <field>Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Status - Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Expense Report Submission Reminders</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Expense_Report__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Submit_Expense_Report</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Expense_Report__c.Submission_Allowed_Date__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Expense_Report_Due_Today</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Expense_Report__c.Submission_Allowed_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Expense Report Name</fullName>
        <actions>
            <name>Update_Expense_Report_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Expense_Report__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
