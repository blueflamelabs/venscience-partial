<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Quota_Name</fullName>
        <field>Name</field>
        <formula>Quarter__c &amp;&quot; - &quot;&amp; Employee__r.FirstName &amp;&quot; &quot;&amp;Employee__r.LastName</formula>
        <name>Set Quota Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Spiff</fullName>
        <field>Spiff__c</field>
        <formula>Employee__r.Spiff__c</formula>
        <name>Set Spiff %</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Commission %26 Spiff %25</fullName>
        <actions>
            <name>Set_Spiff</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Quota Name</fullName>
        <actions>
            <name>Set_Quota_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quota__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
