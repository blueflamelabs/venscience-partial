<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Invitation_to_Screening_Associate</fullName>
        <description>Send Invitation to Screening - Associate</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mike.mccann@vennsci.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HR_Emails/Invitation_to_Screening_Call_Associate</template>
    </alerts>
    <alerts>
        <fullName>Send_Invitation_to_Screening_Call_1</fullName>
        <description>Send Invitation to Screening -Consultant</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mike.mccann@vennsci.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HR_Emails/Invitation_to_Screening_Call_1SC</template>
    </alerts>
    <alerts>
        <fullName>Send_Invitation_to_Screening_PM</fullName>
        <description>Send Invitation to Screening - PM</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mike.mccann@vennsci.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HR_Emails/Invitation_to_Screening_Call_1PM</template>
    </alerts>
</Workflow>
