<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_DiscoverOrg_ID</fullName>
        <description>Copy​​ DiscoverOrg ​​ID​​ value​​ to ​​the ​​DiscoverOrg ​​ID ​​field</description>
        <field>DSCORGPKG__DiscoverOrg_ID__c</field>
        <formula>DSCORGPKG__External_DiscoverOrg_Id__c</formula>
        <name>Copy ​​DiscoverOrg ​​ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DiscoverOrg%E2%80%8B%E2%80%8B ID %E2%80%8B%E2%80%8Bnot %E2%80%8B%E2%80%8Bblank%E2%80%8B%E2%80%8B on %E2%80%8B%E2%80%8Bcreation</fullName>
        <actions>
            <name>Copy_DiscoverOrg_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.DSCORGPKG__DiscoverOrg_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Evaluates​​ whether ​​DiscoverOrg ​​ID ​​is ​​not ​​null ​​when ​​an Account ​​is ​​created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
