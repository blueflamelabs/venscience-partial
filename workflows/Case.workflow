<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Response_to_Email</fullName>
        <description>New Response to Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/Case_New_Response</template>
    </alerts>
    <alerts>
        <fullName>Notification_New_Case</fullName>
        <description>Notification: New Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Templates/Notification_New_Case</template>
    </alerts>
    <rules>
        <fullName>Notification Email on New Case</fullName>
        <actions>
            <name>Notification_New_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification Email on New Response</fullName>
        <actions>
            <name>New_Response_to_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New Response</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
