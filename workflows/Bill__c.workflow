<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Bill_Due_Date_Approaching</fullName>
        <description>Bill Due Date Approaching</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Bill_Due_Date_Approaching</template>
    </alerts>
    <alerts>
        <fullName>Bill_Overdue</fullName>
        <description>Bill Overdue</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Bill_Over_Due</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Overdue_Days</fullName>
        <field>Days_Overdue__c</field>
        <formula>Days_Overdue__c +1</formula>
        <name>Update Overdue Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Paid_Date</fullName>
        <field>Paid_Date__c</field>
        <formula>Today()</formula>
        <name>Update Paid Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Bill Due Date Approaching</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Bill__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bill_Due_Date_Approaching</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Bill__c.Due_Date__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Bill Over Due</fullName>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(Due_Date__c)),  TEXT(Status__c)= &quot;Open&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bill_Overdue</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Overdue_Days</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Bill__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Paid Date</fullName>
        <actions>
            <name>Update_Paid_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Bill__c.Status__c</field>
            <operation>equals</operation>
            <value>Paid</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
