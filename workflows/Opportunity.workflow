<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Opportunity_Closed_Won</fullName>
        <description>Alert: Opportunity Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/New_Project_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Close_Date_is_Today</fullName>
        <description>Sets Opportunity Close Date to equal Today&apos;s Date.</description>
        <field>CloseDate</field>
        <formula>Today()</formula>
        <name>Close Date is Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Project</fullName>
        <field>Create_Project__c</field>
        <literalValue>1</literalValue>
        <name>Create Project</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Acct_Legal_Name</fullName>
        <field>Legal_Name__c</field>
        <formula>Legal_Name__c</formula>
        <name>Populate Acct Legal Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Opp_Corporation_Location</fullName>
        <field>Corporation_Location__c</field>
        <formula>Account.Entity_Location__c</formula>
        <name>Populate Opp Corporation Location</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Opp_Legal_Name</fullName>
        <field>Legal_Name__c</field>
        <formula>Account.Legal_Name__c</formula>
        <name>Populate Opp Legal Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Type</fullName>
        <field>Type</field>
        <literalValue>Client</literalValue>
        <name>Update Account Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SOW_Expiration_Date</fullName>
        <field>SOW_Expiration_Date__c</field>
        <formula>IF(AND(Month(Today())&gt;=8,Month(Today())&lt;=12),DATE(Year(Today())+1,8,31),
IF(Month(Today())=1,DATE(Year(Today()),8,31),
DATE(Year(Today())+1,3,1)-1))</formula>
        <name>Update SOW Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert%3A Opportunity Closed Won</fullName>
        <actions>
            <name>Alert_Opportunity_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Account_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Project Checkbox</fullName>
        <actions>
            <name>Create_Project</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Acct Legal Name</fullName>
        <actions>
            <name>Populate_Acct_Legal_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Legal_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Opp Corporation Location</fullName>
        <actions>
            <name>Populate_Opp_Corporation_Location</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Entity_Location__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Opp Legal Name</fullName>
        <actions>
            <name>Populate_Opp_Legal_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Legal_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Close Date</fullName>
        <actions>
            <name>Close_Date_is_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>This updates the Opportunity Close Date to today&apos;s date when an Opportunity is marked Closed-Won or Closed-Lost.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SOW Expiration Date</fullName>
        <actions>
            <name>Update_SOW_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Service__c</field>
            <operation>equals</operation>
            <value>Retained Consulting,Hourly Consulting</value>
        </criteriaItems>
        <description>For Hourly and Retained Consulting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
