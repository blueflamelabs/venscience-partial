<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Expense_Line_Total</fullName>
        <field>Total__c</field>
        <formula>Distance_mi__c *0.58</formula>
        <name>Update Expense Line Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Merchant_for_Mileage_Expenses</fullName>
        <field>Merchant__c</field>
        <formula>TEXT(Distance_mi__c) &amp; &quot; Mi @ $0.58/Mi&quot;</formula>
        <name>Update Merchant for Mileage Expenses</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Merchant for Mileage Expenses</fullName>
        <actions>
            <name>Update_Expense_Line_Total</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Merchant_for_Mileage_Expenses</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Expense_Line__c.Mileage__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
