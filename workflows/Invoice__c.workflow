<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Invoice_Sent</fullName>
        <description>Alert: Invoice Sent</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Invoice_Sent</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Description_on_Fixed_Price</fullName>
        <field>Description__c</field>
        <formula>&quot;Salesforce &quot;  &amp; TEXT(Product_Service__c) &amp; &quot; Milestone Invoice&quot;</formula>
        <name>Set Description on Fixed Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payment_Terms_Due_on_Receipt</fullName>
        <field>Payment_Terms_for_Invoice__c</field>
        <literalValue>Due on Receipt</literalValue>
        <name>Set Payment Terms = Due on Receipt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BP_End_Date</fullName>
        <field>BP_End_Date__c</field>
        <formula>TEXT(DATE(YEAR(Invoice_Date__c),MONTH(Invoice_Date__c),DAY(Invoice_Date__c)))</formula>
        <name>Update BP End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Sent</fullName>
        <field>Date_Sent__c</field>
        <formula>Today()</formula>
        <name>Update Date Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Due_Date</fullName>
        <field>Due_Date__c</field>
        <formula>IF( TEXT(Payment_Terms_for_Invoice__c)=&quot;Due on Receipt&quot;,Invoice_Date__c,
IF( TEXT(Project__r.Payment_Term_Period__c)=&quot;Calendar Days&quot;, 
Invoice_Date__c + Project__r.Payment_Term_Days__c,

CASE(
MOD(Invoice_Date__c - DATE(1900, 1, 7), 7),
0, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + FLOOR((Project__r.Payment_Term_Days__c-1)/5)*2,
1, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + FLOOR((Project__r.Payment_Term_Days__c)/5)*2,
2, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + FLOOR((Project__r.Payment_Term_Days__c+1)/5)*2,
3, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + FLOOR((Project__r.Payment_Term_Days__c+2)/5)*2,
4, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + FLOOR((Project__r.Payment_Term_Days__c+3)/5)*2,
5, (Invoice_Date__c) + Project__r.Payment_Term_Days__c + CEILING((Project__r.Payment_Term_Days__c)/5)*2,
6, (Invoice_Date__c) - IF(Project__r.Payment_Term_Days__c&gt;0,1,0) + Project__r.Payment_Term_Days__c + CEILING((Project__r.Payment_Term_Days__c)/5)*2,
null)
))</formula>
        <name>Update Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Due_Date_Net_15</fullName>
        <field>Due_Date__c</field>
        <formula>Invoice_Date__c + 15</formula>
        <name>Update Due Date - Net 15</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Due_Date_Net_45</fullName>
        <field>Due_Date__c</field>
        <formula>Invoice_Date__c+45</formula>
        <name>Update Due Date - Net 45</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Due_Date_On_Receipt</fullName>
        <field>Due_Date__c</field>
        <formula>Invoice_Date__c</formula>
        <name>Update Due Date - On Receipt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Edit_Invoice_1</fullName>
        <description>This workflow rule updates the field Edit_Invoice__c every 24 hours, as long as the Invoice Status =! Placeholder, Payment Received, On Hold, or Cancelled. The field update allows other workflow rules to fire on a daily basis (e.g. Update Weeks Late).</description>
        <field>Edit_Invoice__c</field>
        <literalValue>2</literalValue>
        <name>Update Edit Invoice 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Edit_Invoice_2</fullName>
        <description>This workflow rule updates the field Edit_Invoice__c every 24 hours, as long as the Invoice Status =! Placeholder, Payment Received, On Hold, or Cancelled. The field update allows other workflow rules to fire on a daily basis (e.g. Update Weeks Late).</description>
        <field>Edit_Invoice__c</field>
        <literalValue>1</literalValue>
        <name>Update Edit Invoice 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Payment_Received_Date</fullName>
        <field>Payment_Recieved_Date__c</field>
        <formula>Today()</formula>
        <name>Update Payment Received Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Weeks_Late</fullName>
        <field>Weeks_Late__c</field>
        <formula>IF( 
AND(
Due_Date__c &lt; TODAY(), ISBLANK(Payment_Recieved_Date__c)),
FLOOR(( TODAY() - Due_Date__c ) / 30), 
FLOOR(( Payment_Recieved_Date__c - Due_Date__c) / 30))</formula>
        <name>Update Weeks Late</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert%3A Invoice has been sent</fullName>
        <actions>
            <name>Alert_Invoice_Sent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(TEXT(Status__c)=&quot;Sent&quot;,  &quot;005i0000000Nmyb&quot; != $User.Id)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Edit Invoice 1</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Edit_Invoice__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Placeholder,Payment Received,On Hold,Cancelled</value>
        </criteriaItems>
        <description>This workflow rule updates the field Edit_Invoice__c every 24 hours, as long as the Invoice Status =! Placeholder, Payment Received, On Hold, or Cancelled. The field update allows other workflow rules to fire on a daily basis (e.g. Update Weeks Late).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Edit_Invoice_1</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Edit Invoice 2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Edit_Invoice__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Placeholder,Payment Received,On Hold,Cancelled</value>
        </criteriaItems>
        <description>This workflow rule updates the field Edit_Invoice__c every 24 hours, as long as the Invoice Status =! Placeholder, Payment Received, On Hold, or Cancelled. The field update allows other workflow rules to fire on a daily basis (e.g. Update Weeks Late).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Edit_Invoice_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>On Invoice Insert for Fixed Price</fullName>
        <actions>
            <name>Set_Description_on_Fixed_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Product_Service__c</field>
            <operation>notEqual</operation>
            <value>Hourly Consulting Payment</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>On Kickoff Invoice Insert</fullName>
        <actions>
            <name>Set_Payment_Terms_Due_on_Receipt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Product_Service__c</field>
            <operation>equals</operation>
            <value>Kickoff</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update BP End Date</fullName>
        <actions>
            <name>Update_BP_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Invoice_Date__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Due Date Based on Payment Terms</fullName>
        <actions>
            <name>Update_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(Isnew(),
Ischanged(Invoice_Date__c),
Ischanged(Payment_Terms__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Payment Received Date</fullName>
        <actions>
            <name>Update_Payment_Received_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISNull( Payment_Recieved_Date__c ),Ispickval(Status__c,&quot;Payment Received&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sent Date</fullName>
        <actions>
            <name>Update_Date_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISNull(Date_Sent__c),Ispickval(Status__c,&quot;Sent&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Weeks Late</fullName>
        <actions>
            <name>Update_Weeks_Late</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Payment Received,On Hold,Cancelled,Placeholder</value>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice__c.Override_Weeks_Late__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow rule calculates the number of weeks an invoice is overdue. It continues to update until the Invoice has been marked Payment Received or Cancelled.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
