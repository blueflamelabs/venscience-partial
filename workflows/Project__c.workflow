<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_New_Project_Created</fullName>
        <description>Email:  New Project Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/New_Project_Created</template>
    </alerts>
    <alerts>
        <fullName>Error</fullName>
        <description>Error</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Invoice_Templates/Follow_up_on_Invoice</template>
    </alerts>
    <alerts>
        <fullName>New_Project_Assigned_to_PM</fullName>
        <description>New Project Assigned to PM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/New_Project_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Notification_Project_Stage_is_Changed</fullName>
        <description>Notification: Project Stage is Changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Notification_Project_Stage_is_Changed</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Training_Approaching</fullName>
        <description>Reminder: Training Approaching</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_Training_Approaching</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Training_Complete</fullName>
        <description>Reminder: Training Complete</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_Update_Stage_Training_Completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Status_Active</fullName>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>Update Status Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Project Assigned to PM</fullName>
        <actions>
            <name>New_Project_Assigned_to_PM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISChanged( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Project Created</fullName>
        <actions>
            <name>Email_New_Project_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Manager of Project Stage Update</fullName>
        <actions>
            <name>Notification_Project_Stage_is_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Ischanged(Stage__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Training Date Approaching</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Training_Go_Live_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Stage__c</field>
            <operation>notEqual</operation>
            <value>Training/Go-Live Complete,Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Training_Approaching</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Project__c.Training_Go_Live_Date__c</offsetFromField>
            <timeLength>-9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Training_Complete</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Project__c.Training_Go_Live_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Status Active</fullName>
        <actions>
            <name>Update_Status_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Stage__c</field>
            <operation>equals</operation>
            <value>Pre-Kickoff,Kickoff,Alpha Testing Complete,Beta Testing Complete,Pilot Testing Complete,Training/Go-Live Complete,Ongoing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status Closed</fullName>
        <actions>
            <name>Update_Status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Stage__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
