<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_15_5_Report_Completed</fullName>
        <description>Notification: 15/5 Report Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Notification_15_5_Report_Completed</template>
    </alerts>
    <alerts>
        <fullName>Notification_15_5_Report_Submitted_to_Employee</fullName>
        <description>Notification: 15/5 Report Submitted to Employee</description>
        <protected>false</protected>
        <recipients>
            <field>Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Notification_15_5_Report_Submitted_to_Employee</template>
    </alerts>
    <alerts>
        <fullName>Notification_15_5_Report_Submitted_to_Manager</fullName>
        <description>Notification: 15/5 Report Submitted to Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Project_Emails/Notification_15_5_Report_Submitted_to_Manager</template>
    </alerts>
    <alerts>
        <fullName>Reminder_15_5_Report_Due_Date_Approaching_Employee</fullName>
        <description>Reminder: 15/5 Report Due Date Approaching (Employee)</description>
        <protected>false</protected>
        <recipients>
            <field>Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_15_5_Report_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Reminder_15_5_Report_Overdue</fullName>
        <description>Reminder: 15/5 Report Overdue</description>
        <protected>false</protected>
        <recipients>
            <recipient>grace.costa@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.mccann@vennsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Email_Template/Reminder_15_5_Report_Overdue</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_15_5_Report_Name</fullName>
        <field>Name</field>
        <formula>Employee__r.Alias &amp; &quot; - &quot; &amp; TEXT(Month(Date__c))&amp;&quot;/&quot;&amp;Text(Year(Date__c))</formula>
        <name>Update 15/5 Report Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Hidden_Rating</fullName>
        <field>Rating_Hidden__c</field>
        <formula>Value(Text(Manager_Rating__c))</formula>
        <name>Update Hidden Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Employee Completed 15%2F5 Report</fullName>
        <actions>
            <name>Notification_15_5_Report_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>X15_5_Report__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alert Employee To Enter 15%2F5 Report</fullName>
        <actions>
            <name>Notification_15_5_Report_Submitted_to_Employee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>X15_5_Report__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alert Manager 15%2F5 Report Submission</fullName>
        <actions>
            <name>Notification_15_5_Report_Submitted_to_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>X15_5_Report__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to Manager</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reminder%3A 15%2F5 Report Due Date Approaching or Overdue</fullName>
        <active>true</active>
        <criteriaItems>
            <field>X15_5_Report__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_15_5_Report_Due_Date_Approaching_Employee</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>X15_5_Report__c.Due_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_15_5_Report_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>X15_5_Report__c.Due_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update 15%2F5 Report Name</fullName>
        <actions>
            <name>Update_15_5_Report_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Or(isnew(), ischanged(Employee__c), ischanged(Date__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Hidden Rating</fullName>
        <actions>
            <name>Update_Hidden_Rating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>X15_5_Report__c.Manager_Rating__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
