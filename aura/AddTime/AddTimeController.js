({
	doInit : function(component, event, helper) {
        helper.init(component, event, helper);
        helper.getUser(component, event, helper);
        helper.fetchTypePicklist(component, event, helper); // fetches PickList Values of Type Field
        
        helper.fetchCategoryPicklist(component, event, helper); // fetches PickList Values of Category Field
        //helper.getselectOptions(component, 'Category__c', 'opt');
    },
    
    saveTimeRecord : function(component, event, helper){
        if(helper.validateFields(component,event)){
            
            helper.SaveTimeRecord(component, event, helper);
        }
    },
    
    closeModel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
   /* dateChange: function(component, event, helper) {
        var selected = event.getSource().get("v.value");
        console.log('==selected==',selected);
        console.log("dateChange");
        var date = component.get("v.sheetdate");
        console.log("date is: ", date);
    },*/
    
})