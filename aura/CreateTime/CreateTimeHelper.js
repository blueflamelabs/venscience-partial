({
	//get the Task record page id on page load. 
    init : function(component, event, helper){
        var recId = component.get("v.recordId");
        var action = component.get("c.getTaskRecord");
        action.setParams({
            recordId : recId
        });        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('--resValue--',resValue);
                component.set("v.taskRecord",resValue);
            }
            var projectId = resValue.agf__Work__r.agf__Epic__r.Project__r.Id;
            console.log('--proid--',projectId);
            component.set("v.projectId",projectId);
            console.log('--proid-get-',component.get("v.projectId"));
            
            var objChild = component.find('compB');
            console.log("Method Called from Child " , objChild.get('v.selectedRecord.value'));
            
            //var userId = $A.get("$SObjectType.CurrentUser.Id");
            //console.log('--userid--',userId);  
            //component.set('v.currentUserId',userId);
            var today = new Date();
            component.set('v.today', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
            
        });
        $A.enqueueAction(action);
    },
    
    getUser : function(component, event, helper) {
	var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.selectedLookUpRecord.Name", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
    
    fetchTypePicklist : function(component, event, hel){
        console.log('--test--');
        var action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': component.get("v.ObjectName"),
            'field_apiname': component.get("v.Type")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                var picklistvalue = a.getReturnValue();
                console.log('picklistvalue--',picklistvalue);
                component.set("v.TypePicklist", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchCategoryPicklist : function(component){
        console.log('--category--');
        var action = component.get("c.getPicklistvalues");
        action.setParams({
            'objectName': component.get("v.ObjectName"),
            'field_apiname': component.get("v.Category")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.CategoryPicklist", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    SaveTimeRecord : function(cmp, event, helper){
        console.log('--currentUserIdsave--',cmp.get("v.selectedLookUpRecord1.value"));
        var expdate = cmp.find("expdate").get("v.value");
        console.log('--expdate--',expdate);
        var deliverable  = cmp.get("v.selectedLookUpRecord.Id");
               
        var projectId = cmp.get("v.taskRecord.agf__Work__r.agf__Epic__r.Project__r.Id");
        console.log('--project--',projectId);
        
        var action = cmp.get("c.saveTimeSheet");
        action.setParams({
            'userId': cmp.get("v.selectedLookUpRecord1.value"),
            'Category' : cmp.get("v.categoryValue"),
            'sheetdate' : expdate,
            'hours' : cmp.get("v.hours"),
            'taskRecord': cmp.get("v.recordId"),
            'Description': cmp.get("v.Description"),
            'type' : cmp.get("v.typeValue"),
            'Deliverable' : cmp.get("v.selectedLookUpRecord.Id"),
            'project' : projectId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('--before success--',state);
            if (state === "SUCCESS"){
                console.log('--inside success--');
                var response = response.getReturnValue();
                console.log('--94-response--',response);
                if(response.succMsg){
                    helper.showSuccessToastfire(response.succMsg);
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                }else{
                    helper.showErrorToastfire(response.errorMsg);
                }
                
            }else{
                let errors = response.getError(); 
                let message = 'Unknown error'; 
                if (errors && Array.isArray(errors) && errors.length > 0) { 
                    message = errors[0].message; 
                } // Display the message 
                console.log('--error-',message); 
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    validateFields: function(component,event){
        var isValid = true;
        // get lookup value 
        var lVal = component.get("v.selectedLookUpRecord"); 
        var hr = component.get("v.hours");
        console.log('--hr--',hr);
        var category = component.get("v.categoryValue");
        console.log('--category--',category);
        var type = component.get("v.typeValue");
        console.log('--type--',type);
        var description = component.get("v.Description");
        console.log('--description--',description);
        
        if(lVal.Id == undefined || hr == undefined || category == '' || type == '' || lVal.Id == '' || description == '' || description== undefined){
            isValid = false ;
            component.find('errorMsg').set('v.value', 'Please Enter the required fields value');
            //var msg = 'Lookup field is required...Please select contact';
            //helper.showErrorToastfire(msg);
        }
        return isValid;
    },
    
    showSuccessToastfire : function(msg) {
        console.log('error-');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success!',
            message: msg,
            duration:'5000',
            type: 'success'
        });
        toastEvent.fire();
        },
    
    showErrorToastfire : function(msg) {
        console.log('error-');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error!',
            message: msg,
            duration:'8000',
            type: 'error'
        });
        toastEvent.fire();
        },
    
    // get the picklist value of selected field from the apex controller. 
    /*getselectOptions : function(component, fieldName, elementId){
        var action = component.get("c.getselectOptions");
        action.setParams({
            fld : fieldName
        });
        var inputSub = component.find("opt");
        console.log('--inputsub--',inputSub);
        var opts=[];
        action.setCallback(this,function(response){
            var loadResponse = response.getReturnValue();
            for(var i=0;i< loadResponse.length;i++){
                opts.push({
                    value : loadResponse[i],
                    label : loadResponse[i]
                });
            }
            inputSub.set("v.options", opts);
            
        });
        $A.enqueueAction(action);
    },
    
    createRecord : function (component, event, helper) {
        var createAcountContactEvent = $A.get("e.force:createRecord");
        createAcountContactEvent.setParams({
            "entityApiName": "Timesheet__c",
            "defaultFieldValues": {
                'Category__c': '',
                 'Date__c' : '',
                  'Hours__c': '',
                  'Consultant__c': '',
                   'Deliverable__c': '',
                    'Type__c': '',
                    'Description__c': '',	
                    'Task__c': component.get("v.recordId")
            }
        });
        createAcountContactEvent.fire();
    }*/
})