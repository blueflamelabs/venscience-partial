({
    /*searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchUserLookUpValues");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                var userRecId = component.get('v.userId');
                var listofUserRec = [];
                storeResponse = storeResponse.concat(userRecId);
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
    },
    getUser : function(component,event,helper) {
        // call the apex class method 
        var action = component.get("c.getDefaultUserLookUpValues");
        // set param to method  
        action.setParams({
            'ObjectName' : component.get("v.objectAPIName")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('storeResponse ',storeResponse);
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
    },
    searchHelper1 : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchUserLookUpValues1");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
    },*/
    
    
    showDefaultUser : function(component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('--userid--',userId); 
         
        var action = component.get('c.fetchCurrentUserRecord');
        action.setParams({
            'userIds' : userId
        });
        action.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS') {
                var result = response.getReturnValue(); 
                console.log('Result is ',result);
                console.log('Result is ',result.label);
                // document.getElementsByClassName('slds-button').style.display = block;
                // $A.util.removeClass(component.find("slds-button"), "slds-button");
                //$A.util.addClass(component.find("slds-button"), "newblock");
                if(result.length > 0)
                {
                    
                    
                    //  $A.util.addClass(component.find("slds-button"), "newblock");
                    component.set('v.selectedRecord',result[0]);
                    component.set('v.value',result[0].value);
                    console.log('selected Record is ', component.get('v.value'));
                }
            }
        });
        $A.enqueueAction(action);
    }, 
    searchRecordsHelper : function(component, event, helper, value) {
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        component.set('v.message','');
        component.set('v.recordsList',null);
        // Calling Apex Method
        var action = component.get('c.fetchRecords');
        action.setStorable();
        //alert(JSON.stringify(component.get('v.fieldName')));
        action.setParams({
            'objectName' : component.get('v.objectName'),
            'filterField' : component.get('v.fieldName'),
            'searchString' : component.get('v.searchString')
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            if(response.getState() === 'SUCCESS') {
                // To check if any records are found for searched keyword
                if(result.length > 0) {
                    // To check if value attribute is prepopulated or not
                    if( $A.util.isEmpty(value) ) {
                        component.set('v.recordsList',result);   
                        component.set("v.recordCount",result.length);
                    } else {
                        var index = result.findIndex(x => x.value === value)
                        if(index != -1) {
                            var selectedRecord = result[index];
                        }
                        component.set('v.selectedRecord',selectedRecord);
                    }
                } else {
                    component.set('v.message','No Records Found');
                }
            } else if(response.getState() === 'INCOMPLETE') {
                component.set('v.message','No Server Response or client is offline');
            } else if(response.getState() === 'ERROR') {
                // If server throws any error
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set('v.message', errors[0].message);
                }
            }
            // To open the drop down list of records
            if( $A.util.isEmpty(value) )
                $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
            $A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(action);
    }
})