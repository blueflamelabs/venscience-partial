({
	doInit : function(component, event, helper) { 
        var todaydate = new Date(); // get today date
		var year = todaydate.getFullYear(); // get current year
        
        if(component.get("v.fontfamily") == 'None'){
            component.set("v.fontfamily", 'Open Sans');}
        else{
            component.set("v.fontfamily", component.get("v.fontfamily"));
        }
        // set value in component variable from builder attribute		
       
        
        if(component.get("v.email") != '') 
        component.set("v.email", component.get("v.email"));
        component.set("v.pcid", "https://vennsci--c.na56.content.force.com/servlet/servlet.FileDownload?file="+component.get("v.logoid"));
        component.set("v.phone", component.get("v.phone"));
            //component.set("v.phone", "(857) 244-6200");

        component.set("v.currentyear", year);
        component.set("v.backgroundcolor", component.get("v.backgroundcolor"));
        component.set("v.fontcolor", component.get("v.fontcolor"));
	}
})