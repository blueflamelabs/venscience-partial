({
    
   /**Set all the dynamic values from attributes properties */

	doInit : function(component, event, helper) {
        component.set("v.fontcolor", component.get("v.fontcolor"));
        if(component.get("v.fontsize") != '') {
            component.set("v.fontsize", component.get("v.fontsize"));
        }else {
            component.set("v.fontsize", "12");
        }
        
        component.set("v.bgcolor", component.get("v.bgcolor"));
        
        if(component.get("v.fontfamily") == 'None'){
            component.set("v.fontfamily", 'Open Sans');
        }
        else{ 
                    component.set("v.fontfamily", component.get("v.fontfamily"));

        }
        
        if(component.get("v.label1") != '') {
            component.set("v.label1", component.get("v.label1"));
        }else {
            component.set("v.label1","");
        }
        if(component.get("v.label2") != '') {
            component.set("v.label2", component.get("v.label2"));
        }else {
            component.set("v.label2", "");
        }
        if(component.get("v.label3") != '') {
            component.set("v.label3", component.get("v.label3"));
        }else {
            component.set("v.label3", "");
        }
        if(component.get("v.label4") != '') {
            component.set("v.label4", component.get("v.label4"));
        }else {
            component.set("v.label4", "");
        }
        if(component.get("v.label5") != '') {
            component.set("v.label5", component.get("v.label5"));
        }else {
            component.set("v.label5", "");
        }
        if(component.get("v.label6") != '') {
            component.set("v.label6", component.get("v.label6"));
        }else {
            component.set("v.label6", "");
        }
        if(component.get("v.label7") != '') {
            component.set("v.label7", component.get("v.label7"));
        }else {
            component.set("v.label7", "");
        }
        if(component.get("v.label8") != '') {
        	component.set("v.label8", component.get("v.label8"));
        }else {
            component.set("v.label8", "");
        }
        if(component.get("v.label9") != '') {
            component.set("v.label9", component.get("v.label9"));
        }else {
            component.set("v.label9", "");
        }
        component.set("v.urllabel1", component.get("v.urllabel1"));
        component.set("v.urllabel2", component.get("v.urllabel2"));
        component.set("v.urllabel3", component.get("v.urllabel3"));
        component.set("v.urllabel4", component.get("v.urllabel4"));
        component.set("v.urllabel5", component.get("v.urllabel5"));
        component.set("v.urllabel6", component.get("v.urllabel6"));
        component.set("v.urllabel7", component.get("v.urllabel7"));
        component.set("v.urllabel8", component.get("v.urllabel8"));
        component.set("v.urllabel9", component.get("v.urllabel9"));
        

	}
})