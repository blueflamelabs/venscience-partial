({
	myprofileurl : function() {
		var myprofileurl = $A.get("$SObjectType.CurrentUser.Id");
        myprofileurl= '/s/profile/'+myprofileurl;
        window.open(myprofileurl,'_self');        
	},
    caseurl : function() {
        window.open('https://vscidev-communitydataxu.cs19.force.com/s/case/Case/00B800000067SlpEAE','_self');        
	},
    mysettingurl : function() {
		var mysettingurl = $A.get("$SObjectType.CurrentUser.Id");
        mysettingurl= 'https://vscidev-communitydataxu.cs19.force.com/s/settings/'+mysettingurl;
        window.open(mysettingurl,'_self');
        
	},
    homeurl : function() {
        window.open('https://vscidev-communitydataxu.cs19.force.com/s/','_self');        
	},
    logouturl : function() {
        window.location.replace("https://vscidev-communitydataxu.cs19.force.com/s//servlet/networks/switch?startURL=%2Fsecur%2Flogout.jsp");        
	} ,
    
    /* handleClick : function(component, event, helper) {
      var myAttribute = component.get("v.myAttribute");
      var action = component.get("c.setAttribute");
      var searchText = component.get('v.searchText');
      var action = component.get('c.searchForIds');
      action.setParams({searchText: searchText,
                       "myString" : myAttribute
                       });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') { 
          var ids = response.getReturnValue();
          sessionStorage.setItem('customSearch--recordIds', JSON.stringify(ids));
          var navEvt = $A.get('e.force:navigateToURL');
          navEvt.setParams({url: '/'});
         navEvt.fire();
        }
      });

      $A.enqueueAction(action);
    },
   */
    
    
})