({
     /*
    set all the values dynamically which coming from atrributes properties
    */
    doInit : function(component, event, helper) {
        //component.set("v.myAttribute", component.get("v.myAttribute"));
        
        component.set("v.imageURL", "https://vscidev-communitydataxu.cs19.force.com/servlet/servlet.FileDownload?file="+component.get("v.logoid"));   
        component.set("v.buttonname", component.get("v.buttonname"));            
        
        if(component.get("v.buttonname") != ''){
            component.set("v.buttoncolor", component.get("v.buttoncolor"));
        }
        else {
            component.set("v.buttoncolor", "");
        }
        
        if(component.get("v.buttonname2") != ''){
            component.set("v.buttoncolor2", component.get("v.buttoncolor2"));
        }
        else {
            component.set("v.buttoncolor2", "");
        }
        
        if(component.get("v.buttonname3") != ''){
            component.set("v.buttoncolor3", component.get("v.buttoncolor3"));
        }
        else {
            component.set("v.buttoncolor3", "");
        } 
        
        component.set("v.showsearchbar", component.get("v.showsearchbar"));
        document.getElementsByClassName("lightningPrimitiveIcon").innerHTML = 'MENU JI'        
    },
    
    buttonredirect: function(component, event, helper) {
        window.open(component.get("v.buttonurl"),'_self');
    },
    buttonredirectsecond: function(component, event, helper) {
        window.open(component.get("v.buttonurl2"),'_self');
    },
    buttonredirectThird: function(component, event, helper) {
        window.open(component.get("v.buttonurl3"),'_self');
    },
    
    myaccount: function(component, event, helper) { 
        console.log('hi>>'+event);       
        console.log(event.target.tabIndex);
        var menuValue = event.target.title;
        
        
        switch(menuValue) {
            case "home": helper.homeurl(); break;
            case "mycases": helper.caseurl(); break;
            case "mysetting": helper.mysettingurl(); break;
            case "myprofile": helper.myprofileurl(); break;
            case "logout": helper.logouturl(); break;
        }
        
    },
    
    jsLoaded: function(component, event, helper) {
    },
    
    handleWindowOpenUrl : function(component, event, helper) {
        window.open("searchresult?searchString="+component.get("v.searchText") , '_self');
    },    
    
    formPress : function(component, event, helper) {
        if (event.keyCode === 13) {
            window.open("searchresult?searchString="+component.get("v.searchText") , '_self');
        }
    },
    
    callServer : function(component, event, helper) {
        /*var myAttribute = component.get("v.myAttribute");
        
        var action = component.get("c.setAttribute");
        action.setParams({ "myString" : myAttribute });
        
        $A.enqueueAction(action);*/
    },
    
    toggalMenu : function(component, event, helper){
        var toggleText = component.find("text");
        $A.util.toggleClass(toggleText, "toggle");
    }
    
})