<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>VSCI_Mark</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <formFactors>Large</formFactors>
    <label>VDLC Console</label>
    <navType>Console</navType>
    <tab>Project__c</tab>
    <tab>Timesheet__c</tab>
    <tab>Solution__c</tab>
    <tab>standard-Feed</tab>
    <tab>Project_Team__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>VDLC_UtilityBar</utilityBar>
</CustomApplication>
