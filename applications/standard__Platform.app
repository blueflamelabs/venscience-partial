<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Book__c</tab>
    <tab>Bill__c</tab>
    <tab>Create_Invoices</tab>
    <tab>Partner_Lead__c</tab>
    <tab>Project_Team__c</tab>
</CustomApplication>
