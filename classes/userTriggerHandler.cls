public with sharing class userTriggerHandler {
    // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
    // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    //090719 T - 00400 Initialise List for the new expense reports record VennScience_BFL_Monali
    //public List<Expense_Report__c> expReportsList = new List<Expense_Report__c>();
    
    //090719 T - 00400 Initialize set of Owner Ids VennScience_BFL_Monali
    public Set<Id> setOfIds = new Set<Id>();
    
    
    public userTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    
    public void OnAfterInsert(User[] newRecords){
        for(User newRecord : newRecords){
            if(newRecord.isActive == TRUE && newRecord.create_expense_reports__c == TRUE && newRecord.ContactId==null){
                //090719 T - 00400 commented Because DML in for loop VennScience_BFL_Monali
                //createExpenses(newRecord.Id);
                setOfIds.add(newRecord.Id);
            }
        }//End of Loop
        createNewExpenses(setOfIds);
        
    }
    /**
* Method Name : createNewExpenses
* Parameters  : pass Set of Owner Ids as a parameter 
* Description : Used to Create the Expence Report.
* Created By  : VennScience_BFL_Monali 
* Created On  : 230819
**/
    @future
    public static void createNewExpenses(Set<Id> ownerId) {
        
        Expense_Report__c[] expReportsList = new List<Expense_Report__c>();
        
        for(Id userId : ownerId) {
            
            String currentMonthYear = String.valueOf(System.Today().month())+'/'+1+'/'+String.valueOf(System.Today().year());
            
            Integer thisMonth = Integer.valueOf(System.Today().month());
            
            String thisYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().year());
            
            String nextYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().addYears(1).year());
            
            Expense_Report__c nextMonthExp = new Expense_Report__c(
                Name='Temp Name', 
                Status__c = 'Open',
                OwnerId = userId);
            if(thisMonth!=12){
                nextMonthExp.Start_Date__c = date.parse(thisYear);
                nextMonthExp.Year__c = String.valueOf(System.Today().year());
            }
            else{
                nextMonthExp.Start_Date__c = date.parse(nextYear);
                nextMonthExp.Year__c = String.valueOf(System.Today().addYears(1).year());
                
            }
            expReportsList.add(nextMonthExp);
            
            //250719 T - 00400 add new  VennScience_BFL_Monali
            Expense_Report__c currentMonthExp = new Expense_Report__c(
                Name='Temp Name', 
                Status__c = 'Open',
                OwnerId = userId);
            if(thisMonth!=12){
                currentMonthExp.Start_Date__c = date.parse(currentMonthYear);
                currentMonthExp.Year__c = String.valueOf(System.Today().year());
            }
            else{
                currentMonthExp.Start_Date__c = date.parse(nextYear);
                currentMonthExp.Year__c = String.valueOf(System.Today().addYears(1).year());
                
            }
            
            expReportsList.add(currentMonthExp);
        }
        System.debug('trigger==before insert =='+expReportsList);
        insert expReportsList;
        System.debug('trigger==insert ===expReports=='+expReportsList);
    }
    
    public void OnBeforeInsert(User[] newRecords){
        /*
//Example usage
for(User newRecord : newRecords){
if(newRecord.AnnualRevenue == null){
newRecord.AnnualRevenue.addError('Missing annual revenue');
}
}
*/
    }
    
    @future public static void OnAfterInsertAsync(Set<ID> newRecordIDs){
        //Example usage
        //List<User> newRecords = [select Id, Name from User where Id IN :newRecordIDs];
    }
    
    public void OnBeforeUpdate(User[] oldRecords, User[] updatedRecords, Map<ID, User> recordMap){
        //Example Map usage
        //Map<ID, Contact> contacts = new Map<ID, Contact>( [select Id, FirstName, LastName, Email from Contact where UserId IN :recordMap.keySet()] );
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedRecordIDs){
        //List<User> updatedRecords = [select Id, Name from User where Id IN :updatedRecordIDs];
    }
    
    public void OnAfterUpdateUser(User[] oldRecords, User[] updatedRecords, Map<ID, User> recordMap, Map<Id, User> oldMap) {
        List<User> updatedUserList = new List<User>();
        for(User updatedRecord : updatedRecords){
            if(updatedRecord.isActive == TRUE) { 
                updatedUserList.add(updatedRecord);
            }
        }
        OnAfterUpdate(oldRecords, updatedUserList, recordMap, oldMap);
    }
    
    public void OnAfterUpdate(User[] oldRecords, User[] updatedRecords, Map<ID, User> recordMap, Map<Id, User> oldMap){
        
        Map<Id,List<Expense_Report__c>> mapOfExpenseReport = new Map<Id,List<Expense_Report__c>>();
        
        String yearString = String.valueOf(System.Today().year());
        System.debug('===yearString=='+yearString);
        String jsonMapOfExpReport = '';
        //090719 T - 00400 initialise the Set of userId VennScience_BFL_Monali
        Set<Id> setOfOwnerIds = new Set<Id>();
        //090719 T - 00400 initialise the Set of expense report ownerId VennScience_BFL_Monali
        Set<Id> setOfExpOwnerId = new Set<Id>();
        
        Set<Id> setOfIds = new Set<Id>();
        
        //try{
        System.debug('===updatedRecords=='+updatedRecords);
        for(User updatedRecord : updatedRecords){
            User oldUser = oldMap.get(updatedRecord.Id);
            System.debug('===updatedRecord=='+updatedRecord.Create_Expense_Reports__c);
            System.debug('===updatedRecord=='+updatedRecord.isActive+ '===old ==='+oldUser.isActive);
            System.debug('===updatedcontact id=='+updatedRecord.ContactId);
            if(updatedRecord.Create_Expense_Reports__c == TRUE && updatedRecord.ContactId==null){
                if(updatedRecord.isActive == TRUE || updatedRecord.isActive != oldUser.isActive) {
                //090719 T - 00400 Set to add the userId VennScience_BFL_Monali.
                setOfOwnerIds.add(updatedRecord.Id);
                //String yearString = string.valueOf(System.Today().year());
                //List<Expense_Report__c> currExp = [select Id, Quarter__c, Year__c FROM Expense_Report__c WHERE OwnerId = :updatedRecord.Id AND Year__c = :yearString ];
                //if(currExp.size()<1){
                //System.debug('onupdate ==='+currExp);
                //createExpenses(updatedRecord.Id);
                }
            }
        }
        System.debug('==setOfOwnerIds=='+setOfOwnerIds);
        //090719 T - 00400 query to get the Expense Record related to updated user VennScience_BFL_Monali
        List<Expense_Report__c> currExpList = [SELECT Id, 
                                               Quarter__c, 
                                               Year__c,
                                               OwnerId,
                                               Start_Date__c
                                               FROM Expense_Report__c 
                                               WHERE OwnerId In : setOfOwnerIds AND Year__c =: yearString];
        System.debug('===currExpList=='+currExpList);
        //230819 T - 00400 iteration on Expense Report to collect the User related Expense Report VennScience_BFL_Monali
        for(Expense_Report__c exp : currExpList) {
            if(exp.OwnerId != null) {
                if(!mapOfExpenseReport.containsKey(exp.OwnerId)) {
                    mapOfExpenseReport.put(exp.OwnerId,new List<Expense_Report__c>{exp});
                }else {
                    List<Expense_Report__c> listofexpReport = new List<Expense_Report__c>();
                    listofexpReport.addAll(mapOfExpenseReport.get(exp.OwnerId));
                    listofexpReport.add(exp);
                    System.debug('---listofexpReport--'+listofexpReport);
                    mapOfExpenseReport.put(exp.OwnerId,listofexpReport);
                }
            }
            
        }//End of loop
        for(User user : updatedRecords) {
            User oldUser = oldMap.get(user.Id);
            if(user.Create_Expense_Reports__c == TRUE && user.ContactId==null){
                if(user.isActive == TRUE || user.isActive != oldUser.isActive){
                    if(!mapOfExpenseReport.containsKey(user.Id)) {
                        setOfIds.add(user.Id);
                    }
                }
            }
        }
        jsonMapOfExpReport = JSON.serialize(mapOfExpenseReport);
        System.debug('====='+jsonMapOfExpReport);
        //}catch(Exception ex) {
            //System.debug('====='+ex.getMessage());
       // }
        /*for(User u : updatedRecords) {
//090719 T - 00400 check the ownerId contains or not VennScience_BFL_Monali
if(!setOfExpOwnerId.contains(u.Id)) {

setOfIds.add(u.Id);
}//End Of if

}//End Of For */
        
        //090719 T - 00400 call method to create Expense related to Updated owner and pass the SetOf OwnerIds VennScience_BFL_Monali
        System.debug('==jsonMapOfExpReport==='+jsonMapOfExpReport);
        System.debug('==setOfIds==='+setOfIds);
        createExpenses(jsonMapOfExpReport, setOfIds);
        
    }
    
    //@future
    public static void createExpenses(String jsonMapOfExpReport, Set<Id> setOfIds){ //Map<Id,List<Expense_Report__c>> mapOfExpenseReport
        
        String currentMonthYear = String.valueOf(System.Today().month())+'/'+1+'/'+String.valueOf(System.Today().year());
        System.debug('--currentMonthYear--'+currentMonthYear);
        
        Integer thisMonth = Integer.valueOf(System.Today().month());
        
        String thisYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().year());
        System.debug('--thisYear--'+thisYear);
        String nextYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().addYears(1).year());
        System.debug('--nextYear--'+nextYear);
        
        List<Expense_Report__c> newExpReportsList = new List<Expense_Report__c>();
        
        Map<Id,List<Expense_Report__c>> mapOfExpenseReport = (Map<Id,List<Expense_Report__c>>)JSON.deserialize(jsonMapOfExpReport, Map<Id,List<Expense_Report__c>>.class);
        Expense_Report__c[] expReportsList = new List<Expense_Report__c>();
        System.debug('==map values=='+mapOfExpenseReport);
        if(mapOfExpenseReport.size() >0) {
            System.debug('inside if part');
            for(Id userId : mapOfExpenseReport.keySet()) {
                Boolean isCurrentMonth = false;
                Boolean isNextMonth = false;
                System.debug('inside user keyset======'+userId);
                System.debug('mapOfExpenseReport.get(userId)======'+mapOfExpenseReport.get(userId));
                for(Expense_Report__c expReport : mapOfExpenseReport.get(userId)) {
                    System.debug('inside expense report======='+expReport);
                    if(expReport.Start_Date__c == date.parse(currentMonthYear)) {
                        System.debug('current month expense report');
                        isCurrentMonth = true;
                    }//End of if 
                    
                    if(expReport.Start_Date__c == date.parse(thisYear)) {
                        System.debug('next month expense report');
                        isNextMonth = true;
                    }//End of if
                }
                System.debug('isCurrentMonth=='+isCurrentMonth);
                System.debug('isNextMonth=='+isNextMonth);
                if(!isCurrentMonth && isNextMonth) {
                    System.debug('created current month expense report');
                    Expense_Report__c exp1 = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp1.Start_Date__c = date.parse(currentMonthYear);
                        exp1.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp1.Start_Date__c = date.parse(nextYear);
                        exp1.Year__c = String.valueOf(System.Today().addYears(1).year());
                    }
                    expReportsList.add(exp1);
                    System.debug('expReportsList==='+expReportsList);
                }
                
                if(isCurrentMonth && !isNextMonth) {
                    System.debug('created next month expense report');
                    Expense_Report__c exp = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp.Start_Date__c = date.parse(thisYear);
                        exp.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp.Start_Date__c = date.parse(nextYear);
                        exp.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }
                    expReportsList.add(exp);
                    System.debug('expReportsList==='+expReportsList);
                }
                
                if(!isCurrentMonth && !isNextMonth) {
                    System.debug('created both month expense report');
                    Expense_Report__c exp1 = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp1.Start_Date__c = date.parse(currentMonthYear);
                        exp1.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp1.Start_Date__c = date.parse(nextYear);
                        exp1.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }
                    expReportsList.add(exp1);
                    
                    Expense_Report__c exp = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp.Start_Date__c = date.parse(thisYear);
                        exp.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp.Start_Date__c = date.parse(nextYear);
                        exp.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }
                    expReportsList.add(exp);
                    System.debug('expReportsList==='+expReportsList); 
                }
            }
            insert expReportsList;
            System.debug('expReportsList=after insert=='+expReportsList); 
        }else {
            if(!setOfIds.isEmpty()) {
                System.debug('set is not empty'); 
                for(Id userId : setOfIds) {
                    
                    Expense_Report__c exp = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp.Start_Date__c = date.parse(thisYear);
                        exp.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp.Start_Date__c = date.parse(nextYear);
                        exp.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }
                    newExpReportsList.add(exp);
                    
                    //250719 T - 00400 add new  VennScience_BFL_Monali
                    Expense_Report__c exp1 = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp1.Start_Date__c = date.parse(currentMonthYear);
                        exp1.Year__c = String.valueOf(System.Today().year());
                    }//End of of
                    else{
                        exp1.Start_Date__c = date.parse(nextYear);
                        exp1.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }//End of else
                    newExpReportsList.add(exp1);
                    System.debug('exp1===exp1=='+exp1); 
                    
                }//End for loop
                insert newExpReportsList;
                System.debug('newExpReportsList===expReports=='+newExpReportsList); 
            }
        }       
    }
    
}