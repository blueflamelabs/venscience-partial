public with sharing class createContentDocumentLink {
	public void createContentDocumentLink(Map<String, String> mapOppIds) {

		 List<ContentDocumentLink> FileList = new List<ContentDocumentLink>();
		 List<String> oppIds = new List<String>(mapOppIds.keySet());


		 for(ContentDocumentLink file : [select Id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId in:OppIds]){
            system.debug(mapOppIds.get(file.LinkedEntityId)+mapOppIds);
            ContentDocumentLink cdLink = new ContentDocumentLink();
            cdLink.ContentDocumentId = file.ContentDocumentId;
            cdLink.LinkedEntityId = mapOppIds.get(file.LinkedEntityId);
            cdLink.ShareType = 'V';
            cdLink.Visibility = 'AllUsers';
            system.debug(file+'----'+cdLink);
            FileList.add(cdLink);            
    }
    system.debug(FileList.size()+'-----'+FileList);
        if(FileList.size()>0){
            insert FileList;
        }
	}


	public void createInvoices(Map<String, String> mapOppIds) {

		List<Invoice__c> InvoiceList = new List<Invoice__c>();
		List<String> oppIds = new List<String>(mapOppIds.keySet());
    	List<Milestone__c> MilestoneList = new List<Milestone__c>();


		 for(Milestone__c milestone : [select Id, Type__c, Payment_Perc__c, Amount__c, Opportunity__c from Milestone__c where Opportunity__c in:OppIds]){
            Invoice__c invoice = new Invoice__c();
            invoice.Project__c = mapOppIds.get(milestone.Opportunity__c);
            invoice.Opportunity__c = milestone.Opportunity__c;
            invoice.Status__c = 'Placeholder';
            invoice.Invoice_Date__c = system.today();
            invoice.Product_Service__c = milestone.Type__c;
            invoice.Description__c = 'Salesforce.com Fixed Price Plus';
            invoice.Invoice_Amount__c = milestone.Amount__c;
            invoice.Milestone_Id__c = milestone.Id;

            InvoiceList.add(invoice); 
    	}
		system.debug(InvoiceList.size()+'-----'+InvoiceList);
        if(InvoiceList.size()>0){
            insert InvoiceList; 


            for(Invoice__c inv : InvoiceList){
            Milestone__c newMilestone = new Milestone__c(id=inv.Milestone_Id__c);
            newMilestone.Invoice__c = inv.id;
            MilestoneList.add(newMilestone);

            
        	}

		        if(MilestoneList.size()>0){
		            update MilestoneList;
		        }
            	
        }
	}

public void createContactRoles(Map<String, String> mapOppIds) {

         List<Project_Contact_Role__c> roleList = new List<Project_Contact_Role__c>();
         List<String> oppIds = new List<String>(mapOppIds.keySet());


         for(OpportunityContactRole oppCont : [select Id, ContactId, OpportunityId, Role from OpportunityContactRole where OpportunityId in:OppIds]){
            Project_Contact_Role__c projCont = new Project_Contact_Role__c();
            projCont.Contact__c = oppCont.ContactId;
            projCont.Project__c = mapOppIds.get(oppCont.OpportunityId);
            projCont.Role__c = oppCont.Role;
            
            roleList.add(projCont);            
    }
    system.debug(roleList.size()+'-----'+roleList);
        if(roleList.size()>0){
            insert roleList;
        }
    }

public void createOngoingSupport(Map<String, String> mapOppIds) {

         List<Deliverable__c> delvList = new List<Deliverable__c>();
         List<String> oppIds = new List<String>(mapOppIds.keySet());


         for(Project__c proj : [select Id, RecordTypeId, Opportunity__c from Project__c where Id in:mapOppIds.values()]){
            if(proj.RecordTypeId == '012i0000000FBrp'){
                Deliverable__c delv = new Deliverable__c();
                delv.Name = 'Ongoing Support';
                delv.Type__c = 'Hourly';
                delv.Project__c = mapOppIds.get(proj.Opportunity__c);
                delv.Opportunity__c = proj.Opportunity__c;
                delv.Status__c = 'In Progress';
                delv.Deliverable_Overview__c = 'Ongoing Support';
                
                delvList.add(delv);   
            }
                     
    }
    system.debug(delvList.size()+'-----'+delvList);
        if(delvList.size()>0){
            insert delvList;
        }
    }
	
	
}