/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        141019         VennScience_BFL_Monali                              This is the handler class for DeliverableRecordShare. Used to filter the 
*                                                                                   Deliverable records as per the required criteria.
*                               
**********************************************************************************************************************************************************/
public class DeliverableTriggerHandler {
    
    // Variable Declarations
   // T-000669 - 050819 - VennScience_BFL_Monali - Used to store the Partner Community profile name
   public static final String PARTNERPROFILENAME = Label.Partner_Community; // Partner Profile Name
    
    /**
    * Method Name : filterDelievrableRecords
    * Parameters  : param1: List<Deliverable__c>
    * Description : Used to filter the Deliverable records as per the required criteria.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 141019
    **/
    public static void filterDeliverableRecords(List<Deliverable__c> deliverableRecordList) {
        // Variable Declarations
        List<Deliverable__c> listDeliverableRecord = new List<Deliverable__c>();
         // Iterate over Deliverable list(Trigger.New)
        for(Deliverable__c deliverableRecord : deliverableRecordList) {
            if(deliverableRecord.Project__c != null) { 
                listDeliverableRecord.add(deliverableRecord);
            }// End of if
        }// End of for
        
        // Call shareDeliverableRecord method and pass Deliverable Records list as a parameter.
        if(listDeliverableRecord.size() > 0)
            shareDeliverableRecord(listDeliverableRecord) ;
    }
    
    /**
    * Method Name : shareDeliverableRecord
    * Parameters  : param1: List<Deliverable__c>
    * Description : Used to share Deliverable record with all Project_Team's Team Member as per the required criteria.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 141019
    **/
    public static void shareDeliverableRecord(List<Deliverable__c> deliverableRecordList) {
        System.debug('deliverableRecordList => '+deliverableRecordList);
        // Variable Declarations
        Map<Id,Set<Id>> mapofProjectIdVsSetDeliverableId = new Map<Id,Set<Id>>();
        List<Deliverable__Share> shareDeliverableRecordList = new List<Deliverable__Share>();
        Set<Id> setOfDeliverable = new Set<Id>();
        // Iterate over Deliverable list
        for(Deliverable__c deliverableRecord : deliverableRecordList) {
            if(!mapofProjectIdVsSetDeliverableId.containsKey(deliverableRecord.Project__c)) {
                mapofProjectIdVsSetDeliverableId.put(deliverableRecord.Project__c, new set<Id>{deliverableRecord.Id});
            } else {
                setOfDeliverable = new Set<Id>();
                setOfDeliverable = mapofProjectIdVsSetDeliverableId.get(deliverableRecord.Project__c);
                setOfDeliverable.add(deliverableRecord.Id);
                mapofProjectIdVsSetDeliverableId.put(deliverableRecord.Project__c, setOfDeliverable);
            }// End of if else.
            
        }// End of for
         System.debug('mapofProjectIdVsSetDeliverableId => '+mapofProjectIdVsSetDeliverableId);
        if(!mapofProjectIdVsSetDeliverableId.isEmpty()) {
            // Fetch all Project Team that belongs to the same Project.
            List<Project_Team__c> projectTeamRecordList = [SELECT Id,
                                                           Project__c,
                                                           Team_Member__c
                                                           FROM Project_Team__c
                                                           WHERE Project__c IN : mapofProjectIdVsSetDeliverableId.keySet()
                                                           AND Team_Member__r.Profile.Name = :PARTNERPROFILENAME
                                                           AND Team_Member__r.isActive = true];
            System.debug('projectTeamRecordList => '+projectTeamRecordList);
            if(!projectTeamRecordList.isEmpty()) {
                // iterate over the Project Team list.
                for(Project_Team__c projectTeamRecord : projectTeamRecordList) {
                    for(Id delievableRecordId : mapofProjectIdVsSetDeliverableId.get(projectTeamRecord.Project__c)) {
                        Deliverable__Share shareDeliverableRecord = new Deliverable__Share();
                        shareDeliverableRecord.ParentId = delievableRecordId;
                        shareDeliverableRecord.UserOrGroupId = projectTeamRecord.Team_Member__c;
                        shareDeliverableRecord.AccessLevel = 'Edit';
                        shareDeliverableRecord.RowCause = Schema.Deliverable__Share.RowCause.Partner_Community__c;
                        shareDeliverableRecordList.add(shareDeliverableRecord);
                        
                    }//End of inner for
                }// End of outer for
                System.debug(' before insert shareDeliverableRecordList => '+shareDeliverableRecordList);
                if(!shareDeliverableRecordList.isEmpty()) {
                    insert shareDeliverableRecordList;
                    System.debug('after insert shareDeliverableRecordList => '+shareDeliverableRecordList);
                }// End of if
            }// End of inner if
        }// End of outer if
        
    }
    
    /**
    * Method Name : filterUpdatedDeliverableRecords
    * Parameters  : param1: List<Deliverable__c>,param2: Map<Id,Deliverable__c>
    * Description : Used to update Deliverable record with all Project_Team's Team Member as per the required criteria.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 151019
    **/
    public static void filterUpdatedDeliverableRecords(List<Deliverable__c> deliverableRecordList, Map<Id,Deliverable__c> deliverableOldMap) {
        
        // Variable Declarations
        List<Deliverable__c> listDeliverableRecord = new List<Deliverable__c>();
        Set<Id> setOfProjectIds = new Set<Id>();
        Set<Id> setOfDeliverableIds = new Set<Id>();
         // Iterate over Deliverable list(Trigger.New)
        for(Deliverable__c deliverableRecord : deliverableRecordList) {
            // if new project is null and old deliverable project is not null remove share records.
            if(deliverableRecord.Project__c == null && deliverableOldMap.get(deliverableRecord.Id).Project__c != null) {
                // Add old project to set to get team members
                    setOfProjectIds.add(deliverableOldMap.get(deliverableRecord.Id).Project__c);
                    // To fetch exiting share record
                    setOfDeliverableIds.add(deliverableRecord.Id);
            }//End of if
            /* if new deliverable project not null and old deliverable project not null create share record for new project 
             * and remove from old project
             */
            if(deliverableRecord.Project__c != null && deliverableOldMap.get(deliverableRecord.Id).Project__c != null) { 
                // If old project is not equal new project
                if(deliverableRecord.Project__c != deliverableOldMap.get(deliverableRecord.Id).Project__c ) {
                    // Add old project to set to get team members
                    setOfProjectIds.add(deliverableOldMap.get(deliverableRecord.Id).Project__c);
                    // To fetch exiting share record
                    setOfDeliverableIds.add(deliverableRecord.Id);
                    // To create new share records
                    listDeliverableRecord.add(deliverableRecord);
                }// End of inner if.
            }// End of if
            // if new deliverable project not null and old deliverable project null create share record.
            if(deliverableRecord.Project__c != null && deliverableOldMap.get(deliverableRecord.Id).Project__c == null) {
                listDeliverableRecord.add(deliverableRecord);
            }//End of if
        }// End of for
        
        // remove exiting share records if old project is not equal to new project
        if(!setOfProjectIds.isEmpty() && !setOfDeliverableIds.isEmpty()) {
            System.debug('inside remove update');
            removeSharedDeliverableRecords(setOfProjectIds, setOfDeliverableIds);
        }
        // Call shareDeliverableRecord method and pass Deliverable Records list as a parameter.
        if(listDeliverableRecord.size() > 0){
            System.debug('inside create share record');
            shareDeliverableRecord(listDeliverableRecord) ;
        }
    }
    
    /**
    * Method Name : removeSharedDeliverableRecords
    * Parameters  : param1: Set<Id> setOfProjectIds,param2: Set<Id> setOfDeliverableIds
    * Description : Used to remove Deliverable record with all Project_Team's Team Member as per the required criteria.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 151019
    **/
    public static void removeSharedDeliverableRecords(Set<Id> setOfProjectIds, Set<Id> setOfDeliverableIds) {
        System.debug('inside remove');
        // Variable Declarations
        Set<Id> setOfTeamMember = new Set<Id>();
         
        // Iterate over Project team list
        for(Project_Team__c projectTeamRecord : [SELECT Id,
                                                           Project__c,
                                                           Team_Member__c
                                                 FROM Project_Team__c
                                                 WHERE Project__c IN : setOfProjectIds
                                                 AND Team_Member__r.Profile.Name = :PARTNERPROFILENAME]) 
        {
            setOfTeamMember.add(projectTeamRecord.Team_Member__c);
        }//End of for.
        
        // fetch shared deliverable records with previous Project.
        List<Deliverable__Share> deliverableShareRecordList = [SELECT Id,
                                                               		  UserOrGroupId,
                                                                      ParentId
                                                               FROM Deliverable__Share
                                                               WHERE ParentId IN :setOfDeliverableIds
                                                               AND UserOrGroupId IN :setOfTeamMember];
        
       System.debug('deliverableShareRecordList for remove => '+deliverableShareRecordList);
        // get excetion if any, while deleting record.
        try{
            // delete shared records if list is not empty.
            if(!deliverableShareRecordList.isEmpty()) {
                // delete shared deliverable records.
                delete deliverableShareRecordList;
            }
        }catch(Exception ex) {
            System.debug('exception -->'+ex.getMessage());
        }
		        
    }

}