@isTest
private class userTriggerHandler_Test{

    @testSetup
    static void setupTestData() {
    
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        List<User> users = new List<user>{
            new User(Username = 'testuser@codecoverage.com', LastName = 'LastName804', Email = 'Email24@test.com', 
                Alias = 'Alias913', CommunityNickname = 'cNickName61741', IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', 
                EmailEncodingKey = 'UTF-8', ProfileId = p.ID, LanguageLocaleKey = 'en_US',Create_Expense_Reports__c = true, 
                Create_Performance_Reviews__c = false
            ),
            new User(Username = 'emailuser@codecoverage.com', LastName = 'Last user', Email = 'Email@test.com', 
                Alias = 'alias 20', CommunityNickname = 'last user', IsActive = false, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', 
                EmailEncodingKey = 'UTF-8', ProfileId = p.ID, LanguageLocaleKey = 'en_US',Create_Expense_Reports__c = true, 
                Create_Performance_Reviews__c = false
            ),
            new User(Username = 'testNewuser@codecoverage.com', LastName = 'LastNew user', Email = 'Email123@test.com', 
                Alias = 'alias 20', CommunityNickname = 'lastnew user', IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', 
                EmailEncodingKey = 'UTF-8', ProfileId = p.ID, LanguageLocaleKey = 'en_US',Create_Expense_Reports__c = true, 
                Create_Performance_Reviews__c = false
            ),
                new User(Username = 'testNewuserforExpReport@codecoverage.com', LastName = 'LastNew user', Email = 'Email123@test.com', 
                Alias = 'alias 20', CommunityNickname = 'lastnew_user01', IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', 
                EmailEncodingKey = 'UTF-8', ProfileId = p.ID, LanguageLocaleKey = 'en_US',Create_Expense_Reports__c = true, 
                Create_Performance_Reviews__c = true
            )
        };
        Insert users; 
        
        String currentMonthYear = String.valueOf(System.Today().addMonths(-2).month())+'/'+1+'/'+String.valueOf(System.Today().year());
        String nextMonth = String.valueOf(System.Today().addMonths(2).month())+'/'+1+'/'+String.valueOf(System.Today().year());
        
        List<Expense_Report__c> expList = new List<Expense_Report__c>{
            new Expense_Report__c(
                Name='Temp Name', 
                Status__c = 'Open',
                OwnerId = users[3].Id,
                //Start_Date__c = date.parse(currentMonthYear),
                Start_Date__c = System.today(),
                Year__c = String.valueOf(System.Today().year())
            ),
                new Expense_Report__c(
                    Name='Temp Name1', 
                    Status__c = 'Open',
                    OwnerId = users[3].Id,
                    //Start_Date__c = date.parse(nextMonth),
                    Start_Date__c = System.today(),
                    Year__c = String.valueOf(System.Today().year())
                )
                
        };
        insert expList;
        
        //String nextMonth = String.valueOf(System.Today().addMonths(2));
        
        test.stopTest();
    }
    
    
    static testMethod void test_UseCase1() {
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       
        System.runAs(currUser){
            
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User 
                                  WHERE Username = 'emailuser@codecoverage.com'];
            List<User> userList  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                     FROM User 
                                     WHERE Username = 'testNewuser@codecoverage.com'];
            
            users[0].isActive = true;
            test.starttest();
            update users;
            test.stoptest();
            System.assert(users.size() != null);
            List<Expense_Report__c> expList = [SELECT Id
                                               FROM Expense_Report__c
                                               WHERE OwnerId =:userList[0].Id];
            System.assertEquals(2, expList.size());
        }
    }
    
    static testMethod void test_UseCase2() {
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(currUser) {
            test.startTest();  
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testNewuser@codecoverage.com'];
            List<Expense_Report__c> userExpReportList = [SELECT Id,
                                                                Name
                                                         FROM Expense_Report__c
                                                         WHERE OwnerId IN : users];
            //System.debug('===expenseList= 66='+userExpReportList);
            users[0].isActive = false;
            update users;
            System.assertEquals(2, [SELECT Id 
                                    FROM Expense_Report__c 
                                    WHERE OwnerId =: users[0].Id].size());
            test.stoptest();
        }
    }  
    
    static testMethod void test_UseCase3() {
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(currUser){
            
            List<User> oldUser  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testNewuser@codecoverage.com'];
            

            oldUser[0].isActive = false;
            update oldUser;
            test.startTest(); 
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testNewuser@codecoverage.com'];
            List<Expense_Report__c> userExpReportList = [SELECT Id,
                                                                Name
                                                         FROM Expense_Report__c
                                                         WHERE OwnerId =: users[0].Id];
            users[0].isActive = true;
            update users;
            List<Expense_Report__c> expList = [SELECT Id
                                               FROM Expense_Report__c
                                               WHERE OwnerId =: users[0].Id];
            System.assert(expList.size() != null);
            test.stoptest();
        }
    }  
    
    static testMethod void test_UseCase4() {
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(currUser){
            test.startTest();  
            List<User> oldUsers  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testuser@codecoverage.com'];
            oldUsers[0].isActive = false;
            update oldUsers;
            
            List<Expense_Report__c> userExpReportList = [SELECT Id,
                                                                Name,
                                                                Start_Date__c
                                                         FROM Expense_Report__c
                                                         WHERE OwnerId = : oldUsers[0].Id];
            Delete userExpReportList[0];
            
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testuser@codecoverage.com'];
            users[0].isActive = true;
            update users;
            
            System.assert(users.size() != null);
            test.stoptest();
        }
    }  
    
    static testMethod void testUserExpenseReportForNextMonth() {
        
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(currUser){
            test.startTest();  
            List<User> oldUsers  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                     FROM User
                                     WHERE Username = 'testuser@codecoverage.com'];
            oldUsers[0].isActive = false;
            update oldUsers;
            
            List<Expense_Report__c> userExpReportList = [SELECT Id,
                                                         Name,
                                                         Start_Date__c
                                                         FROM Expense_Report__c
                                                         WHERE OwnerId = : oldUsers[0].Id];
            Delete userExpReportList[1];
            
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testuser@codecoverage.com'];
            users[0].isActive = true;
            update users;
            
            System.assert(users.size() != null);
            test.stoptest();
        }
    }
    
    static testMethod void testExpenseReport() {
        User currUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(currUser){
            String currentMonthYear = String.valueOf(System.Today().month())+'/'+1+'/'+String.valueOf(System.Today().year());
            String nextMonth = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().year());
            
            
            List<User> userList = [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                     FROM User
                                     WHERE Username = 'testNewuserforExpReport@codecoverage.com'];
            userList[0].isActive = false;
            update userList;
            
            List<Expense_Report__c> userExpReportList = [SELECT Id,
                                                         Name,
                                                         Start_Date__c
                                                         FROM Expense_Report__c
                                                         WHERE OwnerId = : userList[0].Id];
            List<Expense_Report__c> expList = new List<Expense_Report__c>();
            for(Expense_Report__c exp : userExpReportList) {
                if(exp.Start_Date__c == date.parse(currentMonthYear) || exp.Start_Date__c == date.parse(nextMonth)) {
                    expList.add(exp);
                }
            }
            delete expList;
            List<User> users  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                  FROM User
                                  WHERE Username = 'testNewuserforExpReport@codecoverage.com'];
            users[0].isActive = true;
            update users;
            System.assert(users.size() != null);
        }
    }
    
}