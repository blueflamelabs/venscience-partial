/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        041019         VennScience_BFL_Amruta                              This is the handler class for TaskTrigger. Used to filter the Task 
*                                                                                   records as per the required criteria.
*                               
**********************************************************************************************************************************************************/
public class TaskTriggerHandler {
    
    // Variable Declarations
   // T-000669 - 241019 - VennScience_BFL_Monali - Used to store the Partner Community profile name
   public static final String PARTNERPROFILENAME = Label.Partner_Community; // Partner Profile Name
    
	/**
    * Method Name : filterTaskRecords
    * Parameters  : param1: List<VDLC_Task__c>
    * Description : Used to filter the task records as per the required criteria.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 041019
    **/
    public static void filterTaskRecords(List<VDLC_Task__c> listTasks) {
        // Variable Declarations
        Set<Id> setFilteredTaskId = new Set<Id>();
        // T-000669 - 241019 - VennScience_BFL_Monali - Used to add the userid
        Set<Id> setFilteredUserId = new Set<Id>();
        // T-000669 - 241019 - VennScience_BFL_Monali - Used to fetch filter user records.
        List<User> userRecordList = [SELECT Id,
                                        Profile.Name
                                FROM User
                                WHERE Profile.Name = :PARTNERPROFILENAME];
        // Iterate over the User list
        for(User userRecord : userRecordList) {
            setFilteredUserId.add(userRecord.Id);
        }//End of for.
        // Fetch the task list.
        List<VDLC_Task__c> taskRecordList = [SELECT Id,
                                                    Assigned_To__c,
                                                    Work__c
                                             FROM VDLC_Task__c
                                             WHERE Id IN :listTasks 
                                             AND Assigned_To__c IN :setFilteredUserId];
        
        // Iterate over task list
        for(VDLC_Task__c taskRecord : taskRecordList) {
            // Filter Task records in order to share Work records with Users
            if(taskRecord.Work__c != null && taskRecord.Assigned_To__c != null) {
                setFilteredTaskId.add(taskRecord.Id);
            } // End of if
        } // End of for
        // Call Controller Class
        TaskTriggerController.shareTaskRelatedWorkWithUsers(setFilteredTaskId);
    }
}