/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        090819         VennScience_BFL_Amruta     This is the handler class for TeamMemberTrigger apex trigger.
**********************************************************************************************************************************************************/
public class TeamMemberTriggerHandler {

   // Variable Declarations
   // T-00281 - 120819 - VennScience_BFL_Amruta - Used to store the Customer Community profile name
   public static final String COMMUNITYPROFILENAME = Label.Customer_Community; // Community Profile Name
    /**
    * Method Name : addToChatterGroup
    * Parameters  : 
    * Description : Used to add the related Team Members to Project Chatter Group
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 090819
    **/
    
    public static void addToChatterGroup(List<agf__ADM_Scrum_Team_Member__c> listNewTeamMembers) {
        // Variable Declarations
        List<User> listCommUsers = new List<User>();
        List<Project__c> listProjects = new List<Project__c>();
        List<CollaborationGroupMember> listCollaborationGrpMem = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> listCollaborationGrpMemToBeInserted = new List<CollaborationGroupMember>();
        Set<Id> setProjectId = new Set<Id>();
        Set<Id> setMemberId = new Set<Id>();
        Set<Id> setGrpId = new Set<Id>(); 
        Map<Id,List<agf__ADM_Scrum_Team_Member__c>> mapProjectIdVSTeamMemberList = new Map<Id,List<agf__ADM_Scrum_Team_Member__c>>();
        Map<Id,Id> mapProjectIdVSGrpId = new Map<Id,Id>();
        Map<Id,List<Id>> mapGroupIdVSUsersList = new Map<Id,List<Id>>();
        
        // Iterate over Trigger.New list
        for(agf__ADM_Scrum_Team_Member__c teamMemberRecord : listNewTeamMembers) {
            if(teamMemberRecord.Add_to_Chatter_Group__c && teamMemberRecord.Project__c != null && teamMemberRecord.agf__Member_Name__c != null) {
                setProjectId.add(teamMemberRecord.Project__c);
                setMemberId.add(teamMemberRecord.agf__Member_Name__c);
                // Populate projectId vs Team Member list map
                if(!mapProjectIdVSTeamMemberList.containsKey(teamMemberRecord.Project__c)) {
                    mapProjectIdVSTeamMemberList.put(teamMemberRecord.Project__c,new List<agf__ADM_Scrum_Team_Member__c>{teamMemberRecord});
                } else {
                    List<agf__ADM_Scrum_Team_Member__c> listPreviousRecords = new List<agf__ADM_Scrum_Team_Member__c>();
                    listPreviousRecords.addAll(mapProjectIdVSTeamMemberList.get(teamMemberRecord.Project__c));
                    listPreviousRecords.add(teamMemberRecord);
                    mapProjectIdVSTeamMemberList.put(teamMemberRecord.Project__c, listPreviousRecords);                    
                } // End of else if block
            } // End of if
        } // End of for
        //System.debug('mapProjectIdVSTeamMemberList========'+mapProjectIdVSTeamMemberList);
        // Fetch users related to Team members
        listCommUsers = [SELECT Id,
                                Name
                          FROM  User
                         WHERE  Id IN :setMemberId
                           AND  ContactId != null
                           AND Profile.Name = :COMMUNITYPROFILENAME];
        // Fetch the related Project records
        listProjects = [SELECT Id,
                               Name,
                               Project_Chatter_Group__c
                          FROM Project__c
                         WHERE Id IN :setProjectId];
        for(Project__c objProject : listProjects) {
            mapProjectIdVSGrpId.put(objProject.Id,objProject.Project_Chatter_Group__c);
            setGrpId.add(objProject.Project_Chatter_Group__c);
        } // End of for
        //System.debug('mapProjectIdVSGrpId========'+mapProjectIdVSGrpId);
        // Fetch Collaboration Group Members
        listCollaborationGrpMem = [SELECT Id,
                                          MemberId,
                                          CollaborationGroupId
                                     FROM CollaborationGroupMember
                                    WHERE CollaborationGroupId IN :setGrpId];
        //System.debug('listCollaborationGrpMem==========='+listCollaborationGrpMem);
        // Iterate over CollaborationMembersList
        for(CollaborationGroupMember objCollGrpMem : listCollaborationGrpMem) {
            // Populate mapGroupIdVSUsersList map
            if(!mapGroupIdVSUsersList.containsKey(objCollGrpMem.CollaborationGroupId)) {
                mapGroupIdVSUsersList.put(objCollGrpMem.CollaborationGroupId,new List<Id>{objCollGrpMem.MemberId});
            } else {
                List<Id> listPreviousUserId = new List<Id>();
                listPreviousUserId.addAll(mapGroupIdVSUsersList.get(objCollGrpMem.CollaborationGroupId));
                listPreviousUserId.add(objCollGrpMem.MemberId);
                mapGroupIdVSUsersList.put(objCollGrpMem.CollaborationGroupId, listPreviousUserId);                    
            } // End of else if block
        } // End of for
        //System.debug('mapGroupIdVSUsersList========'+mapGroupIdVSUsersList);
        // Insert Group Members
        for(Id projId : mapProjectIdVSTeamMemberList.keySet()) {
            for(agf__ADM_Scrum_Team_Member__c teamMemberRec : mapProjectIdVSTeamMemberList.get(projId)) {
                CollaborationGroupMember objGroupMember = new CollaborationGroupMember();
                // Check if current user is not already a part of current group
                if(!mapProjectIdVSGrpId.isEmpty() && mapProjectIdVSGrpId.containsKey(projId)
                   && !mapGroupIdVSUsersList.isEmpty() && mapGroupIdVSUsersList.containsKey(mapProjectIdVSGrpId.get(projId))
                   && !mapGroupIdVSUsersList.get(mapProjectIdVSGrpId.get(projId)).contains(teamMemberRec.agf__Member_Name__c)) {
                   // Add current user to current Chatter Group
                   objGroupMember.CollaborationGroupId  = mapProjectIdVSGrpId.get(projId);
                   objGroupMember.memberid = teamMemberRec.agf__Member_Name__c; 
                   listCollaborationGrpMemToBeInserted.add(objGroupMember);
                } else if(!mapProjectIdVSGrpId.isEmpty() && mapProjectIdVSGrpId.containsKey(projId)) {
                    // Add current user to current Chatter Group
                   objGroupMember.CollaborationGroupId  = mapProjectIdVSGrpId.get(projId);
                   objGroupMember.memberid = teamMemberRec.agf__Member_Name__c; 
                } // End of if
            } // End of outer for
        } // End of outer for
        
        //System.debug('listCollaborationGrpMemToBeInserted========'+listCollaborationGrpMemToBeInserted);
        // Insert Group Members
        if(!listCollaborationGrpMemToBeInserted.isEmpty()) {
            try {
                insert listCollaborationGrpMemToBeInserted;    
            } catch(Exception e) {
                System.debug('Error occurred while inserting chatter group members:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    } 
    /**
    * Method Name : filterTeamMemberRecords
    * Parameters  : 
    * Description : Used to filter the Team Member records
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 120819
    **/
    public static void filterTeamMemberRecords(List<agf__ADM_Scrum_Team_Member__c> listNewTeamMembers,map<Id,agf__ADM_Scrum_Team_Member__c> teamMemberOldMap) {
        // Variable Declarations
        List<agf__ADM_Scrum_Team_Member__c> listUpdatedRecords = new List<agf__ADM_Scrum_Team_Member__c>();
        List<agf__ADM_Scrum_Team_Member__c> listRemovedMemberRecords = new List<agf__ADM_Scrum_Team_Member__c>();
        // 270819 - T-00281 - VennSience_BFL_Amruta - Added variable
        List<agf__ADM_Scrum_Team_Member__c> listUpdatedTeamMembers = new List<agf__ADM_Scrum_Team_Member__c>();
        
        // Iterate over Team Members list
        for(agf__ADM_Scrum_Team_Member__c teamMemberRecord : listNewTeamMembers) {
            if(!teamMemberOldMap.isEmpty() && teamMemberRecord.Add_to_Chatter_Group__c && teamMemberRecord.agf__Member_Name__c != null && 
               teamMemberRecord.agf__Member_Name__c != teamMemberOldMap.get(teamMemberRecord.Id).agf__Member_Name__c) {
                listUpdatedRecords.add(teamMemberRecord);
                // 270819 - T-00281 - VennSience_BFL_Amruta - Populate List
                listUpdatedTeamMembers.add(teamMemberOldMap.get(teamMemberRecord.Id));
            } // End of if
            if(!teamMemberOldMap.isEmpty() && teamMemberRecord.Add_to_Chatter_Group__c && teamMemberRecord.agf__Member_Name__c == null && 
               teamMemberRecord.agf__Member_Name__c != teamMemberOldMap.get(teamMemberRecord.Id).agf__Member_Name__c) {
                listRemovedMemberRecords.add(teamMemberOldMap.get(teamMemberRecord.Id));
            } // End of if
        } // End of for
        //System.debug('listUpdatedRecords======='+listUpdatedRecords);
        // Call method to add members to Chatter group
        if(!listUpdatedRecords.isEmpty()) {
            addToChatterGroup(listUpdatedRecords);  
        } // End of if  
        // Call method to remove members from Chatter group
        if(!listRemovedMemberRecords.isEmpty()) {
            removeFromChatterGroup(listRemovedMemberRecords);   
        } // End of if  
        // Call method to remove previous members from Chatter group
        if(!listUpdatedTeamMembers.isEmpty()) {
            removeFromChatterGroup(listUpdatedTeamMembers);    
        } // End of if
    }
    /**
    * Method Name : removeFromChatterGroup
    * Parameters  : 
    * Description : Used to remove the deleted Team Members from associated Project Chatter Group
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 120819
    **/
    
    public static void removeFromChatterGroup(List<agf__ADM_Scrum_Team_Member__c> listDeletedTeamMembers) {
        
        // Variable Declarations
        Set<Id> setProjectId = new Set<Id>();
        Set<Id> setMemberId = new Set<Id>();
        Set<Id> setGroupId = new Set<Id>();
        List<Project__c> listProjects = new List<Project__c>();
        List<CollaborationGroupMember> listCollaborationGrpMem = new List<CollaborationGroupMember>();
        
        // Iterate over deleted records
        for(agf__ADM_Scrum_Team_Member__c teamMemRec : listDeletedTeamMembers) {
            if(teamMemRec.Project__c != null && teamMemRec.agf__Member_Name__c != null) {
                setProjectId.add(teamMemRec.Project__c);
                setMemberId.add(teamMemRec.agf__Member_Name__c);
            } // End of if           
        } // End of for
        // Fetch Projects
        listProjects = [SELECT Id,
                               Project_Chatter_Group__c
                          FROM Project__c
                         WHERE Id IN :setProjectId];
        //System.debug('listProjects======='+listProjects);
        for(Project__c projectRecord : listProjects) {
            setGroupId.add(projectRecord.Project_Chatter_Group__c);
        } // End of for
        // Fetch Collaboration Group Members
        listCollaborationGrpMem = [SELECT Id,
                                          MemberId,
                                          CollaborationGroupId
                                     FROM CollaborationGroupMember
                                    WHERE CollaborationGroupId IN :setGroupId
                                      AND MemberId IN :setMemberId];
        //System.debug('listCollaborationGrpMem======'+listCollaborationGrpMem);
        // Remove Group Members
        if(!listCollaborationGrpMem.isEmpty()) {
            try {
                delete listCollaborationGrpMem;    
            } catch(Exception e) {
                System.debug('Error occurred while deleting collaboration group member records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
}