/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        071019         VennScience_BFL_Monali      This is the test data to test the share records with different scenario. 
*                               
**********************************************************************************************************************************************************/
@isTest
public class TaskTriggerControllerTest {
    
    /**
* Method Name : filterTaskRecordsPositiveTest
* Parameters  : 
* Description : 
* Created By  : VennScience_BFL_Monali 
* Created On  : 071019
**/
    @isTest
    public static void filterTaskRecordsPositiveTest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
        
            Project__c projectRec = TestDataFactory.createNewProject();
            
            insert projectRec;
            
            VDLC_Work__c workRecord = TestDataFactory.createVDLC_WorkTest(projectRec.Id);
            insert workRecord;
            
            VDLC_Task__c taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.communityUser.Id, workRecord.Id);
            
            Test.startTest();
            
            insert taskRecord;
            System.debug('taskRecord ->'+taskRecord);
            Test.stopTest();
            
            List<VDLC_Work__Share> workShareList = [SELECT Id,
                                                     ParentId,
                                                     RowCause,
                                                     AccessLevel,
                                                     UserOrGroupId
                                                     FROM VDLC_Work__Share
                                                     WHERE UserOrGroupId = :uWrap.communityUser.Id];

            System.assertEquals(1,workShareList.size());
        }
    }   
    
    /**
    * Method Name : filterTaskRecordsforBulkPositiveTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 091019
    **/
    @isTest
    public static void filterTaskRecordsforBulkPositiveTest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
        
            List<Project__c> projectRecList = TestDataFactory.createProjectList();
            
            insert projectRecList;
            
            List<VDLC_Work__c> workRecordList = TestDataFactory.createVDLC_WorkListWithProject();
            insert workRecordList;
            
            List<VDLC_Task__c> taskRecordList = TestDataFactory.createVDLC_TaskList(uWrap.communityUser.Id, workRecordList[0].Id);
            
            Test.startTest();
            
            insert taskRecordList;
            System.debug('taskRecordList ->'+taskRecordList);
            Test.stopTest();
            
            List<VDLC_Work__Share> workShareList = [SELECT Id,
                                                     ParentId,
                                                     RowCause,
                                                     AccessLevel,
                                                     UserOrGroupId
                                                     FROM VDLC_Work__Share
                                                     WHERE UserOrGroupId = :uWrap.communityUser.Id];

            System.assertEquals(1,workShareList.size());
        }
    } 
    
    /**
    * Method Name : taskRecordMultiplePartnerCommunityUserTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 091019
    **/
    @isTest
    public static void taskRecordMultiplePartnerCommunityUserTest() {
        Set<Id> setOfUserIds = new Set<Id>();
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        List<User> userlist = uWrap.listofCommunityUser;
        for(User u : userlist) {
           setOfUserIds.add(u.Id); 
        }
        System.debug('setOfUserIds ->'+setOfUserIds);
        System.runAs(uWrap.adminUser) {
        
            
            List<VDLC_Task__c> taskRecordList = TestDataFactory.createBulkVDLC_TaskList(setOfUserIds);
            
            Test.startTest();
            
            insert taskRecordList;
            System.debug('taskRecordList ->'+taskRecordList);
            Test.stopTest();
            
            List<VDLC_Work__Share> workShareList = [SELECT Id,
                                                     ParentId,
                                                     RowCause,
                                                     AccessLevel,
                                                     UserOrGroupId
                                                     FROM VDLC_Work__Share
                                                     WHERE UserOrGroupId IN :setOfUserIds];
			System.debug('workShareList ->'+workShareList);
            System.assertEquals(4,workShareList.size());
        }
    } 
    
    
}