/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        091019         VennScience_BFL_Amruta                              This is the test class for ProjectTeamTriggerController apex class. 
*                               
**********************************************************************************************************************************************************/
@isTest
public class ProjectTeamTriggerControllerTest {
	/**
    * Method Name : createShareRecordsOnProjectTeamCreationTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 091019
    **/
    @isTest
    public static void createShareRecordsOnProjectTeamCreationTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Project record
            Project__c proRecord = new Project__c();
            proRecord = TestDataFactory.createProject();
            insert proRecord;
            
            // Insert Work record
            VDLC_Work__c workRecord = new VDLC_Work__c();
            workRecord = TestDataFactory.createVDLC_WorkTest(proRecord.Id);
            insert workRecord;
            
            // Insert child Task record for Work
            VDLC_Task__c taskRecord = new VDLC_Task__c();
            taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.listofCommunityUser[0].Id, workRecord.Id);
            insert taskRecord;
            
            // Insert child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = proRecord.Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert child Project Team record for Project
            Project_Team__c proTeamRecord = new Project_Team__c();
            proTeamRecord.Project__c = proRecord.Id;
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[1].Id;
            Test.startTest();
            insert proTeamRecord;
            Test.stopTest();
            // Assert: Check if Project share records has been created
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId = :proRecord.Id
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listProShare.size() > 0,'Project share records are not created for Partner Community User');
            // Assert: Check if Work share records has been created
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId = :workRecord.Id
                                AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listWorkShare.size() > 0,'Work share records are not created for Partner Community User');
            // Assert: Check if Deliverable share records has been created
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listDelShare.size() > 0,'Deliverable share records are not created for Partner Community User');
        } // End of System.runAs block
    }
    /**
    * Method Name : createShareRecordsOnProjectTeamMemberUpdateTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 091019
    **/
    @isTest
    public static void createShareRecordsOnProjectTeamMemberUpdateTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Project record
            Project__c proRecord = new Project__c();
            proRecord = TestDataFactory.createProject();
            insert proRecord;
            
            // Insert Work record
            VDLC_Work__c workRecord = new VDLC_Work__c();
            workRecord = TestDataFactory.createVDLC_WorkTest(proRecord.Id);
            insert workRecord;
            
            // Insert child Task record for Work
            VDLC_Task__c taskRecord = new VDLC_Task__c();
            taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.listofCommunityUser[0].Id, workRecord.Id);
            insert taskRecord;
            
            // Insert child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = proRecord.Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert child Project Team record for Project
            Project_Team__c proTeamRecord = new Project_Team__c();
            proTeamRecord.Project__c = proRecord.Id;
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[1].Id;
            insert proTeamRecord;
            
            Project_Team__c projectTeamRecord = new Project_Team__c();
            projectTeamRecord.Project__c = proRecord.Id;
            projectTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            insert projectTeamRecord;
            // Update Project Team record
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            Test.startTest();
            update proTeamRecord;
            Test.stopTest();
            // Assert: Check if Project share records has been created
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId = :proRecord.Id
                               AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listProShare.size() > 0,'Project share records are not created for Partner Community User');
            // Assert: Check if Work share records has been created
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId = :workRecord.Id
                                AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listWorkShare.size() > 0,'Work share records are not created for Partner Community User');
            // Assert: Check if Deliverable share records has been created
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listDelShare.size() > 0,'Deliverable share records are not created for Partner Community User');
        } // End of System.runAs block
    }
    /**
    * Method Name : createShareRecordsOnMultipleTeamMemberUpdateTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 091019
    **/
    @isTest
    public static void createShareRecordsOnMultipleTeamMemberUpdateTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Project record
            Project__c proRecord = new Project__c();
            proRecord = TestDataFactory.createProject();
            insert proRecord;
            
            // Insert Work record
            VDLC_Work__c workRecord = new VDLC_Work__c();
            workRecord = TestDataFactory.createVDLC_WorkTest(proRecord.Id);
            insert workRecord;
            
            // Insert child Task record for Work
            VDLC_Task__c taskRecord = new VDLC_Task__c();
            taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.listofCommunityUser[0].Id, workRecord.Id);
            insert taskRecord;
            
            // Insert child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = proRecord.Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert child Project Team record for Project
            Project_Team__c proTeamRecord = new Project_Team__c();
            proTeamRecord.Project__c = proRecord.Id;
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            insert proTeamRecord;
            
            List<Project_Team__c> listProTeam = new List<Project_Team__c>();
            Project_Team__c projectTeamRecord = new Project_Team__c();
            projectTeamRecord.Project__c = proRecord.Id;
            projectTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            insert projectTeamRecord;
            // Update Project Team record
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[1].Id;
            projectTeamRecord.Team_Member__c = uWrap.listofCommunityUser[1].Id;
            listProTeam.add(proTeamRecord);
            listProTeam.add(projectTeamRecord);
            Test.startTest();
            update listProTeam;
            Test.stopTest();
            // Assert: Check if Project share records has been created
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId = :proRecord.Id
                               AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listProShare.size() > 0,'Project share records are not created for Partner Community User');
            // Assert: Check if Work share records has been created
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId = :workRecord.Id
                                AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listWorkShare.size() > 0,'Work share records are not created for Partner Community User');
            // Assert: Check if Deliverable share records has been created
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[0].Id];    
            System.assert(listDelShare.size() > 0,'Deliverable share records are not created for Partner Community User');
        } // End of System.runAs block
    }
    /**
    * Method Name : createShareRecordsOnTeamMemberRemoveTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 091019
    **/
    @isTest
    public static void createShareRecordsOnTeamMemberRemoveTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Project record
            Project__c proRecord = new Project__c();
            proRecord = TestDataFactory.createProject();
            insert proRecord;
            
            // Insert Work record
            VDLC_Work__c workRecord = new VDLC_Work__c();
            workRecord = TestDataFactory.createVDLC_WorkTest(proRecord.Id);
            insert workRecord;
            
            // Insert child Task record for Work
            VDLC_Task__c taskRecord = new VDLC_Task__c();
            taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.listofCommunityUser[0].Id, workRecord.Id);
            insert taskRecord;
            
            // Insert child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = proRecord.Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert child Project Team record for Project
            Project_Team__c proTeamRecord = new Project_Team__c();
            proTeamRecord.Project__c = proRecord.Id;
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[1].Id;
            insert proTeamRecord;
            
            Project_Team__c projectTeamRecord = new Project_Team__c();
            projectTeamRecord.Project__c = proRecord.Id;
            projectTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            //insert projectTeamRecord;
            // Update Project Team record
            proTeamRecord.Team_Member__c = null;
            Test.startTest();
            update proTeamRecord;
            Test.stopTest();
            // Assert: Check if Project share records has been deleted
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId = :proRecord.Id
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listProShare.size() == 0,'Project share records are not deleted for Partner Community User');
            // Assert: Check if Work share records has been deleted
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId = :workRecord.Id
                                AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listWorkShare.size() == 0,'Work share records are not deleted for Partner Community User');
            // Assert: Check if Deliverable share records has been deleted
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listDelShare.size() == 0,'Deliverable share records are not deleted for Partner Community User');
        } // End of System.runAs block
    }
    /**
    * Method Name : createShareRecordsOnMultipleTeamMemberRemoveTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 101019
    **/
    @isTest
    public static void createShareRecordsOnMultipleTeamMemberRemoveTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Project record
            Project__c proRecord = new Project__c();
            proRecord = TestDataFactory.createProject();
            insert proRecord;
            
            // Insert Work record
            VDLC_Work__c workRecord = new VDLC_Work__c();
            workRecord = TestDataFactory.createVDLC_WorkTest(proRecord.Id);
            insert workRecord;
            
            // Insert child Task record for Work
            VDLC_Task__c taskRecord = new VDLC_Task__c();
            taskRecord = TestDataFactory.createVDLC_TaskTest(uWrap.listofCommunityUser[0].Id, workRecord.Id);
            insert taskRecord;
            
            // Insert child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = proRecord.Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert child Project Team record for Project
            Project_Team__c proTeamRecord = new Project_Team__c();
            proTeamRecord.Project__c = proRecord.Id;
            proTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            insert proTeamRecord;
            
            List<Project_Team__c> listProjectTeam = new List<Project_Team__c>();
            Project_Team__c projectTeamRecord = new Project_Team__c();
            projectTeamRecord.Project__c = proRecord.Id;
            projectTeamRecord.Team_Member__c = uWrap.listofCommunityUser[0].Id;
            insert projectTeamRecord;
            // Update Project Team record
            proTeamRecord.Team_Member__c = null;
            projectTeamRecord.Team_Member__c = null;
            listProjectTeam.add(proTeamRecord);
            listProjectTeam.add(projectTeamRecord);
            Test.startTest();
            update listProjectTeam;
            Test.stopTest();
            // Assert: Check if Project share records has been deleted
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId = :proRecord.Id
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listProShare.size() == 0,'Project share records are not deleted for Partner Community User');
            // Assert: Check if Work share records has been deleted
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId = :workRecord.Id
                                AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listWorkShare.size() == 0,'Work share records are not deleted for Partner Community User');
            // Assert: Check if Deliverable share records has been deleted
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listDelShare.size() == 0,'Deliverable share records are not deleted for Partner Community User');
        } // End of System.runAs block
    }
    /**
    * Method Name : createShareRecordsOnMultipleTeamMemberRemoveTest2
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 101019
    **/
    @isTest
    public static void createShareRecordsBulkTest() {
        // Insert Partner community user
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        
        System.runAs(uWrap.adminUser) {
            // Insert Partner community user for Project Team
            TestDataFactory.UserWrapper userWrappper = new TestDataFactory.UserWrapper();
            userWrappper = TestDataFactory.createPartnerCommunityUserTest1();
            
            // Insert bulk Projects
            List<Project__c> listProjects = new List<Project__c>();
            listProjects = TestDataFactory.createProjectList();   
            insert listProjects;
                
            // Insert bulk child Deliverable record for Project
            List<Deliverable__c> listDel = new List<Deliverable__c>();
            List<Deliverable__c> listDelToInsert = new List<Deliverable__c>();
            listDel = TestDataFactory.createDeliverableListTest(2);
            for(Deliverable__c delRecord : listDel) {
                delRecord.Project__c = listProjects[0].Id;
                listDelToInsert.add(delRecord);
            }
            insert listDelToInsert;
            // Iterate over Del list
            Set<Id> setDelId = new Set<Id>();
            for(Deliverable__c deliverableRec : listDelToInsert) {
                setDelId.add(deliverableRec.Id);  
            }

            // Insert bulk Work records
            List<VDLC_Work__c> listWork = new List<VDLC_Work__c>();
            listWork = TestDataFactory.createVDLC_WorkList(listProjects.size());
            for(Integer i=0; i < listWork.size(); i++) {
                listWork[i].Project__c = listProjects[i].Id;
            }
            insert listWork;
            
            // Insert bulk child Task record for Work
            List<VDLC_Task__c> listTasks = new List<VDLC_Task__c>();
            Set<ID> setWorkId = new Set<Id>();
            for(Integer i=0; i < listWork.size(); i++) {
                VDLC_Task__c taskRec = new VDLC_Task__c();
                taskRec.Assigned_To__c = uWrap.listofCommunityUser[0].Id;
                taskRec.Due_By__c=System.today() + 2;
                taskRec.Environment__c='Test Environment'+i;
                taskRec.Work__c = listWork[i].Id;
                taskRec.Subject__c = 'Test Task Record'+i;
                setWorkId.add(listWork[i].Id);
                listTasks.add(taskRec);
            }
            insert listTasks;
            
            // Insert bulk Project Team records
            List<Project_Team__c> listProjectTeam = new List<Project_Team__c>();
            Set<Id> setProId = new Set<Id>();
            for(Integer i=0; i < listProjects.size(); i++) {
                Project_Team__c projectTeamRec = new Project_Team__c();
                projectTeamRec.Project__c = listProjects[i].Id;
                projectTeamRec.Team_Member__c = uWrap.listofCommunityUser[1].Id;
                setProId.add(listProjects[i].Id);
                listProjectTeam.add(projectTeamRec);
            } // End of for
            Test.startTest();
            insert listProjectTeam;
            Test.stopTest();
            // Assert: Check if Project share records has been created
            List<Project__Share> listProShare = new List<Project__Share>();
            listProShare = [SELECT Id
                              FROM Project__Share
                             WHERE ParentId IN :setProId
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listProShare.size() >= listProjects.size(),'Project share records are not created for Partner Community User');
            // Assert: Check if Work share records has been created
            List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
            listWorkShare = [SELECT Id
                               FROM VDLC_Work__Share
                              WHERE ParentId IN :setWorkId
                                AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listWorkShare.size() > 0,'Work share records are not created for Partner Community User');
            // Assert: Check if Deliverable share records has been created
            List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
            listDelShare = [SELECT Id
                              FROM Deliverable__Share
                             WHERE ParentId IN :setDelId
                               AND UserorGroupId = :uWrap.listofCommunityUser[1].Id];    
            System.assert(listDelShare.size() > 0,'Deliverable share records are not created for Partner Community User');
        } // End of System.runAs block
    }
}