/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        260819         VennScience_BFL_Amruta     This is the test class for ProjectTriggerHandler apex class.
**********************************************************************************************************************************************************/
@isTest
public class ProjectTriggerHandlerTest {
    /**
    * Method Name : createCommunityChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of createCommunityChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void createCommunityChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        insert getProjectRecord;
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                 WHERE Id = :getProjectRecord.Id];
        // Check if chatter group has been created
        System.assert(updatedProjectRecord.Project_Chatter_Group__c != null, 'Chatter group is not created for inserted Project record');
    }
    
    /**
    * Method Name : syncProjectContactRoleWithChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of syncProjectContactRoleWithChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void syncProjectContactRoleWithChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        insert getProjectRecord;
        // Create community user and get the associated contact id
        Id getContactId = TestDataFactory.createCommunityUser();
        // Get Project Contact Role record
        Project_Contact_Role__c proConRoleRecord = new Project_Contact_Role__c();
        proConRoleRecord = TestDataFactory.createProjectContactRole();
        proConRoleRecord.Contact__c = getContactId;
        proConRoleRecord.Project__c = getProjectRecord.Id;
        insert proConRoleRecord;
        System.debug('proConRoleRecord=========='+proConRoleRecord);
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                    WHERE Id = :getProjectRecord.Id];
        // 270819 - T-00281 - VennScience_BFL_Amruta - Call handler method
        System.debug('getProjectRecord====='+getProjectRecord.Id);
        //Test.startTest();
        ProjectTriggerHandler.syncProjectContactRoleWithChatterGroup(new List<Project__c>{getProjectRecord},new Set<Id>{getProjectRecord.Id},
                                                              new Map<Id,Id>{updatedProjectRecord.Id => updatedProjectRecord.Project_Chatter_Group__c},
                                                              new Set<Id>{updatedProjectRecord.Project_Chatter_Group__c});
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :updatedProjectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        //System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
    }
    
    
    /**
    * Method Name : syncTeamMembersWithChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of syncTeamMembersWithChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void syncTeamMembersWithChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        insert getProjectRecord;
        // Create community user and get the associated contact id
        Id getContactId = TestDataFactory.createCommunityUser();
        User commUser = new User();
        commUser = [SELECT Id
                      FROM User
                     WHERE ContactId = :getContactId LIMIT 1];
        // Get Project Contact Role record
        agf__ADM_Scrum_Team_Member__c teamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        teamMemberRecord = TestDataFactory.createTeamMembers();
        teamMemberRecord.agf__Member_Name__c = commUser.Id;
        teamMemberRecord.Project__c = getProjectRecord.Id;
        agf__ADM_Scrum_Team__c teamRecord = new agf__ADM_Scrum_Team__c();
        teamRecord = TestDataFactory.createTeam(); 
        insert teamRecord;
        teamMemberRecord.agf__Scrum_Team__c = teamRecord.Id;
        teamMemberRecord.Add_to_Chatter_Group__c = true;
        insert teamMemberRecord;
        System.debug('teamMemberRecord=========='+teamMemberRecord);
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                    WHERE Id = :getProjectRecord.Id];
        // 270819 - T-00281 - VennScience_BFL_Amruta - Call handler method
        System.debug('getProjectRecord====='+getProjectRecord.Id);
        //Test.startTest();
        ProjectTriggerHandler.syncTeamMembersWithChatterGroup(new List<Project__c>{getProjectRecord},new Set<Id>{getProjectRecord.Id},
                                                              new Map<Id,Id>{updatedProjectRecord.Id => updatedProjectRecord.Project_Chatter_Group__c},
                                                              new Set<Id>{updatedProjectRecord.Project_Chatter_Group__c});
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :updatedProjectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
    }
    
    /**
    * Method Name : createProjectShareTest
    * Parameters  : 
    * Description : Used to test the scenarios if Project Support is populated then share project with it
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 270819
    **/
    @isTest
    public static void createProjectShareSupportUserTest() {
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;

        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        //getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(1,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareTest1
    * Parameters  : 
    * Description : Used to test the scenarios if Consultant and Project Support is populated then share project with both
    * Created By  : VennScience_BFL_Monali
    * Created On  : 170919
    **/
    @isTest
    public static void createProjectShareTest1() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        
        update getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUser.Id
                                                      OR UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(2,projectShareList.size());
    }
    
    /**
    * Method Name : createProjectShareConsultantTest
    * Parameters  : 
    * Description : Used to test the scenarios if Consultant is populated and Project Support is not populated then share project with it
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 270819
    **/
    @isTest
    public static void createProjectShareConsultantTest() {
        
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        //getProjectRecord.Project_Support__c = projectSupportUser.Id;
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUser.Id];
        System.assertEquals(1,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareTest
    * Parameters  : 
    * Description : Used to test the scenarios if Consultant and Project Support is populated then share project with both
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 2870819
    **/
    @isTest
    public static void createProjectShareTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUser.Id
                                                      OR UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(2,projectShareList.size());
    }
    
    /**
    * Method Name : createNewProjectShareTest
    * Parameters  : 
    * Description : Used to test the scenarios if Consultant and Project Support is populated then share project with both
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 170919
    **/
    @isTest
    public static void createNewProjectShareTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        //getProjectRecord.Project_Support__c = projectSupportUser.Id;
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUser.Id
                                                      OR UserOrGroupId = :projectSupportUser.Id];
        //System.assertEquals(2,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareWithUpdatedProjectSupportTest
    * Parameters  : 
    * Description : Used to test the scenarios when Project Support is changed then share project with the updated user
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 2870819
    **/
    @isTest
    public static void createProjectShareWithUpdatedProjectSupportTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        insert getProjectRecord;
        // Get another Project Support user
        User projectSupportUserToBeUpdated = TestDataFactory.createProjectSupportUser();
        insert projectSupportUserToBeUpdated;
        Test.startTest();
        getProjectRecord.Project_Support__c = projectSupportUserToBeUpdated.Id;
        update getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :projectSupportUserToBeUpdated.Id];
        System.assertEquals(1,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareWithUpdatedConsultantTest
    * Parameters  : 
    * Description : Used to test the scenarios when Consultant is changed then share project with the updated user
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 2870819
    **/
    @isTest
    public static void createProjectShareWithUpdatedConsultantTest() {
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        insert getProjectRecord;
        // Get another Consultant user
        User consultantUserToBeUpdated = TestDataFactory.createConsultantUser();
        insert consultantUserToBeUpdated;
        Test.startTest();
        getProjectRecord.Delivery_Operations_Associate__c = consultantUserToBeUpdated.Id;
        update getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUserToBeUpdated.Id];
        System.assertEquals(1,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareWithUpdatedProjectSupportAsNullTest
    * Parameters  : 
    * Description : Used to test the scenarios when Project Support is set as null then remove the sharing with the user
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 2870819
    **/
    @isTest
    public static void createProjectShareWithUpdatedProjectSupportAsNullTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        insert getProjectRecord;
        Test.startTest();
        getProjectRecord.Project_Support__c = null;
        update getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(0,projectShareList.size());
    }
    /**
    * Method Name : createProjectShareWithUpdatedConsultantAsNullTest
    * Parameters  : 
    * Description : Used to test the scenarios when Consultant is set as null then remove the sharing with the user
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 2870819
    **/
    @isTest
    public static void createProjectShareWithUpdatedConsultantAsNullTest() {
        
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        insert getProjectRecord;
        Test.startTest();
        getProjectRecord.Delivery_Operations_Associate__c = null;
        update getProjectRecord;
        Test.stopTest();
        
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :consultantUser.Id];
        System.assertEquals(0,projectShareList.size());
    }
    /**
    * Method Name : updateCommunityChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of updateCommunityChatterGroupTest method
    * Created By  : VennScience_BFL_Monali
    * Created On  : 170919
    **/
    
    @isTest
    public static void updateCommunityChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Create_Chatter_Group__c = false;
        insert getProjectRecord;
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Client_Project_Name__c,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                 WHERE Id = :getProjectRecord.Id];
        // update chatter group.
        updatedProjectRecord.Client_Project_Name__c = 'Test Project - 002';
        updatedProjectRecord.Create_Chatter_Group__c = true;
        ProjectTriggerHandler.isRecursiveCall = false;
        Update updatedProjectRecord;
        // Check if chatter group has been created
        System.assert(updatedProjectRecord.Project_Chatter_Group__c != null, 'Chatter group is not created for inserted Project record');
    }
    
    
    /**
    * Method Name : syncMultipleProjectContactRoleWithChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of syncMultipleProjectContactRoleWithChatterGroupTest method
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 170919
    **/
    
    @isTest
    public static void syncMultipleProjectContactRoleWithChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        insert getProjectRecord;
        // Create community user and get the associated contact id
        Id getContactId = TestDataFactory.createCommunityUser();
        // Get Project Contact Role record
        Project_Contact_Role__c proConRoleRecord = new Project_Contact_Role__c();
        proConRoleRecord = TestDataFactory.createProjectContactRole();
        proConRoleRecord.Contact__c = getContactId;
        proConRoleRecord.Project__c = getProjectRecord.Id;
        insert proConRoleRecord;
        System.debug('proConRoleRecord=========='+proConRoleRecord);
        Id getNewContactId = TestDataFactory.createNewCommunityUser();
        
        Project_Contact_Role__c proConRoleRecord1 = new Project_Contact_Role__c();
        proConRoleRecord1 = TestDataFactory.createNewProjectContactRole();
        proConRoleRecord1.Contact__c = getContactId;
        proConRoleRecord1.Project__c = getProjectRecord.Id;
        insert proConRoleRecord1;
        
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                    WHERE Id = :getProjectRecord.Id];
        // 170919 - T-00281 - VennScience_BFL_Monali - Call handler method
        System.debug('getProjectRecord====='+getProjectRecord.Id);
        //Test.startTest();
        ProjectTriggerHandler.syncProjectContactRoleWithChatterGroup(new List<Project__c>{getProjectRecord},new Set<Id>{getProjectRecord.Id},
                                                              new Map<Id,Id>{updatedProjectRecord.Id => updatedProjectRecord.Project_Chatter_Group__c},
                                                              new Set<Id>{updatedProjectRecord.Project_Chatter_Group__c});
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :updatedProjectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        //System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
    }
    
    /**
    * Method Name : syncMultipleTeamMembersWithChatterGroupTest
    * Parameters  : 
    * Description : Used to test the scenarios of syncMultipleTeamMembersWithChatterGroupTest method
    * Created By  : VennScience_BFL_Monali
    * Created On  : 170919
    **/
    
    @isTest
    public static void syncMultipleTeamMembersWithChatterGroupTest() {
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        insert getProjectRecord;
        // Create community user and get the associated contact id
        Id getContactId = TestDataFactory.createCommunityUser();
        User commUser = new User();
        commUser = [SELECT Id
                      FROM User
                     WHERE ContactId = :getContactId LIMIT 1];
         // Get Project Contact Role record
        agf__ADM_Scrum_Team_Member__c teamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        teamMemberRecord = TestDataFactory.createTeamMembers();
        teamMemberRecord.agf__Member_Name__c = commUser.Id;
        teamMemberRecord.Project__c = getProjectRecord.Id;
        agf__ADM_Scrum_Team__c teamRecord = new agf__ADM_Scrum_Team__c();
        teamRecord = TestDataFactory.createTeam(); 
        insert teamRecord;
        teamMemberRecord.agf__Scrum_Team__c = teamRecord.Id;
        teamMemberRecord.Add_to_Chatter_Group__c = true;
        insert teamMemberRecord;
        System.debug('teamMemberRecord=========='+teamMemberRecord);
        
        CollaborationGroup collaborationGroupRecord1 = TestDataFactory.createCollaborationGroup();
        collaborationGroupRecord1.Name = 'Test New group 1';
        insert collaborationGroupRecord1;
        
        getProjectRecord.Client_Project_Name__c = 'Test Project - 002';
        getProjectRecord.Project_Chatter_Group__c = collaborationGroupRecord1.Id;
        ProjectTriggerHandler.isRecursiveCall = false;
        update getProjectRecord;
        // Fetch Project record updated values
        Project__c updatedProjectRecord = new Project__c();
        updatedProjectRecord = [SELECT Id,
                                       Project_Chatter_Group__c
                                  FROM Project__c
                                    WHERE Id = :getProjectRecord.Id];
        // 270819 - T-00281 - VennScience_BFL_Amruta - Call handler method
        System.debug('getProjectRecord====='+getProjectRecord.Id);
        //Test.startTest();
        ProjectTriggerHandler.syncTeamMembersWithChatterGroup(new List<Project__c>{getProjectRecord},new Set<Id>{getProjectRecord.Id},
                                                              new Map<Id,Id>{updatedProjectRecord.Id => updatedProjectRecord.Project_Chatter_Group__c},
                                                              new Set<Id>{updatedProjectRecord.Project_Chatter_Group__c});
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :updatedProjectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        //System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
    }
    
    
    /**
    * Method Name : createProjectListShareSupportUserTest
    * Parameters  : 
    * Description : Used to test the scenarios if Project Support is populated then share project with it
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 170919
    **/
    @isTest
    public static void createProjectListShareSupportUserTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        User projectSupportUser1 = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser1;

        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        //getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        
        Project__c getProjectRecord1 = new Project__c();
        getProjectRecord1 = TestDataFactory.createProject();
        getProjectRecord1.Project_Support__c = projectSupportUser1.Id;
        //getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
       
        insert getProjectRecord1;
        
        
        Test.stopTest();
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(1,projectShareList.size());
    }
    @isTest
    public static void shareProjectRecordsWithUserTest() {
        // Create Project Support User
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        // Create another Project Support User to be updated
        User projectSupportUser2 = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser2;
        // Create Consultant User
        User consultantUser = TestDataFactory.createConsultantUser();
        insert consultantUser;
        // Create Consultant User to be updated
        User consultantUserToUpdate = TestDataFactory.createConsultantUser();
        insert consultantUserToUpdate;
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Support__c = projectSupportUser.Id;
        getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecord;
        Test.stopTest();
        // Create share Project record for Project Support user
        Project__Share shareProjectRecord = new Project__Share();
        shareProjectRecord.RowCause = 'Manual';
        shareProjectRecord.AccessLevel = 'Read';
        shareProjectRecord.UserOrGroupId = projectSupportUser2.Id;
        shareProjectRecord.ParentId = getProjectRecord.Id;
        insert shareProjectRecord;
        // Create share Project record for Consultant user
        Project__Share shareProjectRecordForConsultant = new Project__Share();
        shareProjectRecordForConsultant.RowCause = 'Manual';
        shareProjectRecordForConsultant.AccessLevel = 'Read';
        shareProjectRecordForConsultant.UserOrGroupId = consultantUserToUpdate.Id;
        shareProjectRecordForConsultant.ParentId = getProjectRecord.Id;
        insert shareProjectRecordForConsultant;
        
        // Fake update for Project record
        getProjectRecord.Project_Support__c = projectSupportUser2.Id;
        getProjectRecord.Delivery_Operations_Associate__c = consultantUserToUpdate.Id;
        update getProjectRecord;
        // Fetch Project Share records
        List<Project__Share> projectShareListForSupportUser = [SELECT Id,
                                                                      ParentId,
                                                                      RowCause,
                                                                      AccessLevel,
                                                                      UserOrGroupId
                                                                 FROM Project__Share
                                                                WHERE UserOrGroupId = :projectSupportUser2.Id];
        List<Project__Share> projectShareListForConsultantUser = [SELECT Id,
                                                                         ParentId,
                                                                         RowCause,
                                                                         AccessLevel,
                                                                         UserOrGroupId
                                                                    FROM Project__Share
                                                                   WHERE UserOrGroupId = :projectSupportUser2.Id];
        // Asserts
        System.assert(projectShareListForSupportUser.size() > 1,'Project is not shared for updated Project support user');
        System.assert(projectShareListForConsultantUser.size() > 1,'Project is not shared for updated Consultant user');
    }
    /**
    * Method Name : createProjectListShareUserTest
    * Parameters  : 
    * Description : Used to test the scenarios if Project Support is populated then share project with it
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 1510919
    **/
    @isTest
    public static void createProjectListShareUserTest() {
        
        User projectSupportUser = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser;
        
        User projectSupportUser1 = TestDataFactory.createProjectSupportUser();
        insert projectSupportUser1;

        List<Project__c> getProjectRecordList = new List<Project__c>();
        getProjectRecordList = TestDataFactory.createProjectList();
        getProjectRecordList[0].Project_Support__c = projectSupportUser.Id;
        getProjectRecordList[1].Project_Support__c = projectSupportUser.Id;
        getProjectRecordList[2].Project_Support__c = projectSupportUser.Id;
        //getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
        Test.startTest();
        insert getProjectRecordList;
        
        Project__c getProjectRecord1 = new Project__c();
        getProjectRecord1 = TestDataFactory.createProject();
        getProjectRecord1.Project_Support__c = projectSupportUser1.Id;
        //getProjectRecord.Delivery_Operations_Associate__c = consultantUser.Id;
       
        insert getProjectRecord1;
        
        
        Test.stopTest();
        List<Project__Share> projectShareList = [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE UserOrGroupId = :projectSupportUser.Id];
        System.assertEquals(3,projectShareList.size());
    }
}