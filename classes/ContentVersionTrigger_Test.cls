/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        260719         VennScience_BFL_Amruta       This is the test class for ContentVersionTrigger
*                               
**********************************************************************************************************************************************************/
@isTest
public class ContentVersionTrigger_Test {
	/**
    * Method Name : updateFileVisibilityForTasksPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of updateFileVisibilityForTasks method of ContentVersionTriggerHandler class.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260719
    **/
    @isTest
    public static void updateFileVisibilityForTasksPositiveTest() {
        
        // Variable Declarations
        List<ContentVersion> listContentVersion = new List<ContentVersion>();
        String fileVisibility = 'AllUsers';
        ContentVersion objContentVersion = new ContentVersion();
        
        // Insert Content Version record
        ContentVersion conVersionToBeInserted = new ContentVersion();
        conVersionToBeInserted = TestDataFactory.createContentVersion();
        insert conVersionToBeInserted;
        
        // Insert Work record
        agf__ADM_Work__c objWorkToBeInserted = new agf__ADM_Work__c();
        objWorkToBeInserted = TestDataFactory.createWork();
        insert objWorkToBeInserted;
        
        // Insert Task record
        agf__ADM_Task__c objTaskToBeInserted = new agf__ADM_Task__c();
        objTaskToBeInserted = TestDataFactory.createTask();
        objTaskToBeInserted.agf__Work__c = objWorkToBeInserted.Id;
        insert objTaskToBeInserted;
        
        // Fetch ContentDocument related to inserted ContentVersion record
        if(conVersionToBeInserted.Id != null) {
        	objContentVersion = [SELECT Id,
                                    	ContentDocumentId
                               	   FROM ContentVersion
                              	  WHERE Id = :conVersionToBeInserted.Id];
            listContentVersion.add(objContentVersion);
    	}
        
        // Insert ContentDocumentLink record
        ContentDocumentLink objContentDocLinkToBeInserted = new ContentDocumentLink();
        objContentDocLinkToBeInserted = TestDataFactory.createContentDocLink();
        objContentDocLinkToBeInserted.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocLinkToBeInserted.LinkedEntityId = objTaskToBeInserted.Id;
        objContentDocLinkToBeInserted.Visibility = 'InternalUsers';
        insert objContentDocLinkToBeInserted;
        
        Test.startTest();
        // Call handler method
        ContentVersionTriggerHandler.updateFileVisibilityForTasks(listContentVersion);
        Test.stopTest();
        
        // Fetch Content Document Link record after method execution
        ContentDocumentLink objConDocLink = new ContentDocumentLink();
        objConDocLink = [SELECT Id,
                        		Visibility
                           FROM ContentDocumentLink
                          WHERE Id = :objContentDocLinkToBeInserted.Id];
        // Assert: Check if file Visibility is updated
        System.assertEquals(fileVisibility,objConDocLink.Visibility,'File Visibility not updated');
    }
    /**
    * Method Name : updateFileVisibilityForTasksNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of updateFileVisibilityForTasks method of ContentVersionTriggerHandler class.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260719
    **/
    @isTest
    public static void updateFileVisibilityForTasksNegativeTest() {
        
        // Variable Declarations
        List<ContentVersion> listContentVersion = new List<ContentVersion>();
        String fileVisibility = 'InternalUsers';
        ContentVersion objContentVersion = new ContentVersion();
        
        // Insert Content Version record
        ContentVersion conVersionToBeInserted = new ContentVersion();
        conVersionToBeInserted = TestDataFactory.createContentVersion();
        insert conVersionToBeInserted;
        
        // Insert Work record
        agf__ADM_Work__c objWorkToBeInserted = new agf__ADM_Work__c();
        objWorkToBeInserted = TestDataFactory.createWork();
        insert objWorkToBeInserted;
        
        // Insert Task record
        agf__ADM_Task__c objTaskToBeInserted = new agf__ADM_Task__c();
        objTaskToBeInserted = TestDataFactory.createTask();
        objTaskToBeInserted.agf__Work__c = objWorkToBeInserted.Id;
        insert objTaskToBeInserted;
        
        // Fetch ContentDocument related to inserted ContentVersion record
        if(conVersionToBeInserted.Id != null) {
        	objContentVersion = [SELECT Id,
                                    	ContentDocumentId
                               	   FROM ContentVersion
                              	  WHERE Id = :conVersionToBeInserted.Id];
            listContentVersion.add(objContentVersion);
    	}
        
        // Insert ContentDocumentLink record
        ContentDocumentLink objContentDocLinkToBeInserted = new ContentDocumentLink();
        objContentDocLinkToBeInserted = TestDataFactory.createContentDocLink();
        objContentDocLinkToBeInserted.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocLinkToBeInserted.LinkedEntityId = objWorkToBeInserted.Id;
        objContentDocLinkToBeInserted.Visibility = 'InternalUsers';
        insert objContentDocLinkToBeInserted;
        
        Test.startTest();
        // Call handler method
        ContentVersionTriggerHandler.updateFileVisibilityForTasks(listContentVersion);
        Test.stopTest();
        
        // Fetch Content Document Link record after method execution
        ContentDocumentLink objConDocLink = new ContentDocumentLink();
        objConDocLink = [SELECT Id,
                        		Visibility
                           FROM ContentDocumentLink
                          WHERE Id = :objContentDocLinkToBeInserted.Id];
        // Assert: Check if file Visibility is not updated
        System.assertEquals(fileVisibility,objConDocLink.Visibility,'File Visibility should be equal to InternalUsers');
    }
}