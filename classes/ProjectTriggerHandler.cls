/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0      300719         VennScience_BFL_Amruta       This is the handler class for ProjectTrigger apex trigger.
                               
**********************************************************************************************************************************************************/
public class ProjectTriggerHandler {
    
    // Variable Declarations
    //public static final String NETWORKID = Label.Customer_Community_Network_Id; // Customer Community Id
    public static final String COMMUNITYPROFILENAME = Label.Customer_Community; // Community Profile Name
    public static Boolean isRecursiveCall = false;
    
    /**
    * Method Name : filterProjectRecords
    * Parameters  : param1: List<Project__c>, Map<Id,Project__c>
    * Description : Used to create community chatter group when Project record is created or updated.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 300719
    **/
    public static void filterProjectRecords(List<Project__c> listNewProjects, Map<Id,Project__c> mapOldProjects) {
         
        //System.debug('Inside filterProjectRecords');
        // Variable Declarations
        List<Project__c> listFilteredProjects = new List<Project__c>();
        
        // Filter Project records for creating the chatter groups
        for(Project__c objProject : listNewProjects) {
            // Check if Create Chatter Group checkbox is updated and is set to true
            if(!mapOldProjects.isEmpty() && mapOldProjects.containsKey(objProject.Id)) {
                if(mapOldProjects.get(objProject.Id).Create_Chatter_Group__c == false 
                   && objProject.Create_Chatter_Group__c == true) {
                    listFilteredProjects.add(objProject);
                } // End of inner if
            } else {
                // Add all records if it is an after insert event
                listFilteredProjects.add(objProject);
            } // End of if-else block
        } // End of for
        
        //System.debug('listFilteredProjects========'+listFilteredProjects);
        // Call method to createChatterGroups
        if(!listFilteredProjects.isEmpty()) {
            createCommunityChatterGroup(listFilteredProjects);
        }
    }
    /**
    * Method Name : createCommunityChatterGroup
    * Parameters  : param1: List<Project__c>
    * Description : Used to create community chatter group when Project record is created or updated.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 300719
    **/
    
    public static void createCommunityChatterGroup(List<Project__c> listProjects) {
         
        // Variable Declarations
        List<CollaborationGroup> listCollaborationGroup = new List<CollaborationGroup>();
        List<Project__c> listProjectToUpdate = new List<Project__c>();
        Set<Id> setProjectId = new Set<Id>();
        Set<Id> setGroupId = new Set<Id>();
        Map<String,Id> mapGroupNameVSProjectId = new Map<String,Id>();
        Map<Id,Id> mapProjectIdVSGroupId = new Map<Id,Id>();
        Boolean isGroupInserted = false;
        Id customerCommNetworkId;
        
        // Fetch Network Community Id
        customerCommNetworkId = [SELECT id, name FROM Network Where Name='Customer Community' LIMIT 1].Id;
        //System.debug('customerCommNetworkId ========'+customerCommNetworkId);
        for(Project__c objProject : listProjects) {
            // Create Community Chatter Group
            CollaborationGroup communityChatterGroup = new CollaborationGroup();
            communityChatterGroup.Name = objProject.Name+' Chatter Group'; // Define group name here
            communityChatterGroup.CollaborationType='Private'; // It can be 'Public' or 'Private' 
            communityChatterGroup.NetworkId = customerCommNetworkId; 
            listCollaborationGroup.add(communityChatterGroup); 
            // Populate mapGroupNameVSProjectId map
            mapGroupNameVSProjectId.put(communityChatterGroup.Name,objProject.Id);
            // Populate Project Id set
            setProjectId.add(objProject.Id);
        } // End of for
        //System.debug('listCollaborationGroup========'+listCollaborationGroup);
        
        // Insert Chatter Group
        if(!listCollaborationGroup.isEmpty()) {
            try {
               insert listCollaborationGroup;
               isGroupInserted = true;
            } catch(Exception e) {
                //System.debug('Error Occurred while inserting chatter group records========'+e.getMessage());
            } // End of try-catch block
        } // End of if
        //System.debug('isGroupInserted==========='+isGroupInserted);
        
        // Iterate over inserted Collaboration Group list
        for(CollaborationGroup objGroup : listCollaborationGroup) {
            if(mapGroupNameVSProjectId.containsKey(objGroup.Name)) {
                mapProjectIdVSGroupId.put(mapGroupNameVSProjectId.get(objGroup.Name),objGroup.Id);
            }
            // Populate group id set
            setGroupId.add(objGroup.Id);
        } // End of for
        
        // Iterate over Project list
        for(Project__c objProjectRecord : listProjects) {
            Project__c objProToBeUpdated = new Project__c(Id = objProjectRecord.Id);
            if(mapProjectIdVSGroupId.containsKey(objProjectRecord.Id)) {
                objProToBeUpdated.Project_Chatter_Group__c = mapProjectIdVSGroupId.get(objProjectRecord.Id);
                listProjectToUpdate.add(objProToBeUpdated);
            } // End of if
        } // End of for
        //System.debug('listProjectToUpdate======='+listProjectToUpdate);
        
        // Update Chatter Group Id on Project record
        try {
            isRecursiveCall = true;
            update listProjectToUpdate;
        } catch(Exception e) {
            //System.debug('Error ocurred while updating Project records:'+e.getMessage());
        } // End of try-catch block
        
        // Add Project Contact Roles and Team Members to Chatter Group
        if(isGroupInserted) {
            syncProjectContactRoleWithChatterGroup(listProjects,setProjectId,mapProjectIdVSGroupId,setGroupId);
            syncTeamMembersWithChatterGroup(listProjects,setProjectId,mapProjectIdVSGroupId,setGroupId);
        }
    }
    
    /**
    * Method Name : syncProjectContactRoleWithChatterGroup
    * Parameters  : param1: List<Project__c>, param2: Set<Id>, param3: Map<Id,Id>
    * Description : Used to sync the Project Contact Role users with created Chatter Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 310719
    **/
    
    public static void syncProjectContactRoleWithChatterGroup(List<Project__c> listProjects, Set<Id> setProId,
                                                              Map<Id,Id> mapProIdVSGroupId, Set<Id> setGrpId) {
        
        //System.debug('Inside syncProjectContactRoleWithChatterGroup');
        // Variable Declarations
        List<Project_Contact_Role__c> listProjectConRole = new List<Project_Contact_Role__c>();
        List<User> listCommunityUsers = new List<User>();
        List<CollaborationGroupMember> listMembersToInsert = new List<CollaborationGroupMember>();
        Set<Id> setContactId = new Set<Id>();
        Map<Id,List<Id>> mapContactIdVSGroupIdList = new Map<Id,List<Id>>();
        Map<Id,Id> mapContactIdVSUserId = new Map<Id,Id>();
        //System.debug('setProId========'+setProId);
        // Fetch child Project Contact Roles for Projects
        listProjectConRole = [SELECT Id,
                                     Name,
                                     Contact__c,
                                     Project__c
                                FROM Project_Contact_Role__c
                               WHERE Project__c IN: setProId];
        //System.debug('listProjectConRole==========='+listProjectConRole);
        // Iterate on Project Contact Role list to fetch the related Contacts
        for(Project_Contact_Role__c objProConRole  : listProjectConRole) {
            setContactId.add(objProConRole.Contact__c);
            // Populate contactId vs GroupId map
            if(!mapProIdVSGroupId.isEmpty() && mapProIdVSGroupId.containsKey(objProConRole.Project__c)) {
                if(!mapContactIdVSGroupIdList.containsKey(objProConRole.Contact__c)) {
                    mapContactIdVSGroupIdList.put(objProConRole.Contact__c,new List<Id>{mapProIdVSGroupId.get(objProConRole.Project__c)});
                } else {
                    List<Id> listPreviousGroupId = new List<Id>();
                    listPreviousGroupId.addAll(mapContactIdVSGroupIdList.get(objProConRole.Contact__c));
                    listPreviousGroupId.add(mapProIdVSGroupId.get(objProConRole.Project__c));
                    mapContactIdVSGroupIdList.put(objProConRole.Contact__c, listPreviousGroupId);                    
                } // End of else if block
            } // End of if
        } // End of for
        //System.debug('mapContactIdVSGroupIdList=========='+mapContactIdVSGroupIdList);
        
        // Fetch Users related to Project Contact Role's related Contacts
        listCommunityUsers = [SELECT Id,
                                     Name,
                                     ContactId 
                                FROM User
                               WHERE ContactId IN: setContactId
                                 AND Profile.Name = :COMMUNITYPROFILENAME];
        //System.debug('listCommunityUsers====='+listCommunityUsers);
        // Iterate over users list
        for(User objUser : listCommunityUsers) {
            // Populate contactId vs UserId map
            mapContactIdVSUserId.put(objUser.ContactId,objUser.Id);
        } // End of for
        
        // Iterate over mapContactIdVSUserId keyset
        for(Id contactId : mapContactIdVSUserId.keySet()) {
            for(Id groupId : mapContactIdVSGroupIdList.get(contactId)) {
                CollaborationGroupMember objGroupMember = new CollaborationGroupMember();
                objGroupMember.memberid = mapContactIdVSUserId.get(contactId); //Provide userId here
                objGroupMember.CollaborationGroupId = groupId; //Id of group created above
                listMembersToInsert.add(objGroupMember);   
            }
        } // End of outer for that is contactId for
        //System.debug('listMembersToInsert=========='+listMembersToInsert);
        
        // Insert Chatter Group Members
        try {
            insert listMembersToInsert;
        } catch(Exception e) {
            //System.debug('Error occurred while inserting Project Contact Role as Chatter Group Members:'+e.getMessage());
        } // End of try-catch block
    }
    
    /**
    * Method Name : syncTeamMembersWithChatterGroup
    * Parameters  : param1: List<Project__c>, param2: Set<Id>, param3: Map<Id,Id>
    * Description : Used to sync the Project Contact Role users with created Chatter Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 310719
    **/
    
    public static void syncTeamMembersWithChatterGroup(List<Project__c> listProjects, Set<Id> setProId, 
                                                       Map<Id,Id> mapProIdVSGroupId, Set<Id> setGrpId) {
        
        // Variable Declarations
        List<agf__ADM_Scrum_Team_Member__c> listTeamMembers = new List<agf__ADM_Scrum_Team_Member__c>();
        List<CollaborationGroupMember> listGrpMembersToInsert = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> listCollaborationGrpMem = new List<CollaborationGroupMember>();                                                  
        List<User> listUsers = new List<User>();
        Set<Id> setUserId = new Set<Id>();
        Set<Id> setCommUserId = new Set<Id>();
        Map<Id,List<Id>> mapUserIdVSGroupIdList = new Map<Id,List<Id>>();
        Map<Id,List<Id>> mapGroupIdVSUsersList = new Map<Id,List<Id>>();
        
        // Fetch team members related to Project records
        listTeamMembers = [SELECT Id,
                                  agf__Member_Name__c,
                                  Add_to_Chatter_Group__c,
                                  Project__c
                             FROM agf__ADM_Scrum_Team_Member__c
                            WHERE Project__c IN :setProId];
        //System.debug('listTeamMembers========='+listTeamMembers);
        // Iterate over Team Members list 
        for(agf__ADM_Scrum_Team_Member__c objTeamMember : listTeamMembers) {
            if(objTeamMember.Add_to_Chatter_Group__c) {
                setUserId.add(objTeamMember.agf__Member_Name__c);
                if(!mapProIdVSGroupId.isEmpty() && mapProIdVSGroupId.containsKey(objTeamMember.Project__c)) {
                    if(!mapUserIdVSGroupIdList.containsKey(objTeamMember.agf__Member_Name__c)) {
                        mapUserIdVSGroupIdList.put(objTeamMember.agf__Member_Name__c,new List<Id>{mapProIdVSGroupId.get(objTeamMember.Project__c)});
                    } else {
                        List<Id> listPreviousGroupId = new List<Id>();
                        listPreviousGroupId.addAll(mapUserIdVSGroupIdList.get(objTeamMember.agf__Member_Name__c));
                        listPreviousGroupId.add(mapProIdVSGroupId.get(objTeamMember.Project__c));
                        mapUserIdVSGroupIdList.put(objTeamMember.agf__Member_Name__c, listPreviousGroupId);                    
                    } // End of else if block
                } // End of if that is if mapProIdVSGroupId is not empty
            } // End of if that is if Add_to_Chatter_Group__c is true
        } // End of for
        //System.debug('mapUserIdVSGroupIdList========='+mapUserIdVSGroupIdList);
        //System.debug('setUserId======='+setUserId);
        
        // Fetch Users related to Team Members that are to be added into the Project Chatter Group
        listUsers = [SELECT Id,
                            Name
                       FROM User
                      WHERE Id IN :setUserId
                        AND Profile.Name = :COMMUNITYPROFILENAME];
        // Iterate over users list
        for(User objCommUser : listUsers) {
            setCommUserId.add(objCommUser.Id);
        }
        // Fetch Collaboration Group Members
        listCollaborationGrpMem = [SELECT Id,
                                          MemberId,
                                          CollaborationGroupId
                                     FROM CollaborationGroupMember
                                    WHERE CollaborationGroupId IN :setGrpId];
        //System.debug('listCollaborationGrpMem==========='+listCollaborationGrpMem);
        // Iterate over CollaborationMembersList
        for(CollaborationGroupMember objCollGrpMem : listCollaborationGrpMem) {
            // Populate mapGroupIdVSUsersList map
            if(!mapGroupIdVSUsersList.containsKey(objCollGrpMem.CollaborationGroupId)) {
                mapGroupIdVSUsersList.put(objCollGrpMem.CollaborationGroupId,new List<Id>{objCollGrpMem.MemberId});
            } else {
                List<Id> listPreviousUserId = new List<Id>();
                listPreviousUserId.addAll(mapGroupIdVSUsersList.get(objCollGrpMem.CollaborationGroupId));
                listPreviousUserId.add(objCollGrpMem.MemberId);
                mapGroupIdVSUsersList.put(objCollGrpMem.CollaborationGroupId, listPreviousUserId);                    
            } // End of else if block
        } // End of for
        //System.debug('mapGroupIdVSUsersList========'+mapGroupIdVSUsersList);
                                                           
        // Iterate over mapUserIdVSGroupIdList keyset
        for(Id userId : mapUserIdVSGroupIdList.keySet()) {
            for(Id groupId : mapUserIdVSGroupIdList.get(userId)) {
                // Check if current user if Customer Community user
                if(setCommUserId.contains(userId)) {
                    // Check if current user is not already a part of current group
                    if(!mapGroupIdVSUsersList.isEmpty() && mapGroupIdVSUsersList.containsKey(groupId) 
                       && !mapGroupIdVSUsersList.get(groupId).contains(userId)) {
                        // Add current user to current Chatter Group
                        CollaborationGroupMember objGroupMember = new CollaborationGroupMember();
                        objGroupMember.memberid = userId; //Provide userId here
                        objGroupMember.CollaborationGroupId = groupId; //Id of group created above
                        listGrpMembersToInsert.add(objGroupMember);
                    } else if(mapGroupIdVSUsersList.isEmpty()) {
                        // Add current user to current Chatter Group
                        CollaborationGroupMember objGroupMember = new CollaborationGroupMember();
                        objGroupMember.memberid = userId; //Provide userId here
                        objGroupMember.CollaborationGroupId = groupId; //Id of group created above
                        listGrpMembersToInsert.add(objGroupMember);
                    } else if(!mapGroupIdVSUsersList.isEmpty() && !mapGroupIdVSUsersList.containsKey(groupId)) {
                        // Add current user to current Chatter Group
                        CollaborationGroupMember objGroupMember = new CollaborationGroupMember();
                        objGroupMember.memberid = userId; //Provide userId here
                        objGroupMember.CollaborationGroupId = groupId; //Id of group created above
                        listGrpMembersToInsert.add(objGroupMember);
                    }// End of inner if-else block
                } // End of outer if
            } // End of inner for
        } // End of outer for
        //System.debug('listGrpMembersToInsert========'+listGrpMembersToInsert);
        
        // Insert Chatter Group Members
        try {
            insert listGrpMembersToInsert;
        } catch(Exception e) {
            //System.debug('Error occurred while inserting team members as Chatter Group Members:'+e.getMessage());
        } // End of try-catch block
    }
    
    
    /**
    * Method Name : shareProjectRecords
    * Parameters  : param1: List<Project__c>, param2: Map<Id,Project__c>
    * Description : Used to share the Project users with created Project share.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 260819
    **/
    public static void shareProjectRecords(List<Project__c> listOfNewProjects, Map<Id,Project__c> mapOldProjects) {
        Map<Id, Set<Id>> mapOfUser = new Map<Id, Set<Id>>();
        Set<Id> userIdSet = new Set<Id>();
        Map<Id, Set<Id>> ProjectShareMap = new Map<Id, Set<Id>>();
        for(Project__c project : listOfNewProjects) {
            if(project.Project_Support__c != null ) {
                if(!mapOfUser.containsKey(project.Id)) {
                    userIdSet = new Set<Id>();
                    userIdSet.add(project.Project_Support__c );
                    mapOfUser.put(project.id, userIdSet);
                    
                } else {
                    userIdSet = new Set<Id>();
                    userIdSet = mapOfUser.get(project.id);
                    userIdSet.add(project.Project_Support__c );
                    mapOfUser.put(project.id, userIdSet);
                    
                }
            } 
            
            if(project.Delivery_Operations_Associate__c != null ) {
                if(!mapOfUser.containsKey(project.Id)) {
                    userIdSet = new Set<Id>();
                    userIdSet.add(project.Delivery_Operations_Associate__c );
                    mapOfUser.put(project.id, userIdSet);
                } else {
                    userIdSet = new Set<Id>();
                    userIdSet = mapOfUser.get(project.id);
                    userIdSet.add(project.Delivery_Operations_Associate__c );
                    mapOfUser.put(project.id, userIdSet);
                }
            }
            if(!mapOldProjects.isEmpty()){
                if(project.Project_Support__c != null 
                   && project.Project_Support__c != mapOldProjects.get(project.Id).Project_Support__c
                ) {
                    if(!mapOfUser.containsKey(project.Id)) {
                        userIdSet = new Set<Id>();
                        userIdSet.add(project.Project_Support__c );
                        mapOfUser.put(project.id, userIdSet);
                    } else {
                        userIdSet = new Set<Id>();
                        userIdSet = mapOfUser.get(project.id);
                        userIdSet.add(project.Project_Support__c );
                        mapOfUser.put(project.id, userIdSet);
                    }
                    if(!ProjectShareMap.containsKey(project.Id)) {
                        userIdSet = new Set<Id>();
                        userIdSet.add(mapOldProjects.get(project.Id).Project_Support__c);
                        ProjectShareMap.put(project.id, userIdSet);
                    } else {
                        userIdSet = new Set<Id>();
                        userIdSet = ProjectShareMap.get(project.id);
                        userIdSet.add(mapOldProjects.get(project.Id).Project_Support__c );
                        ProjectShareMap.put(project.id, userIdSet);
                    }
                    
                } else if(project.Project_Support__c == null 
                          && mapOldProjects.get(project.Id).Project_Support__c != null) {
                    if(!ProjectShareMap.containsKey(project.Id)) {
                        userIdSet = new Set<Id>();
                        userIdSet.add(mapOldProjects.get(project.Id).Project_Support__c);
                        ProjectShareMap.put(project.id, userIdSet);
                    } else {
                        userIdSet = new Set<Id>();
                        userIdSet = ProjectShareMap.get(project.id);
                        userIdSet.add(mapOldProjects.get(project.Id).Project_Support__c );
                        ProjectShareMap.put(project.id, userIdSet);
                    }
                }       
                if(project.Delivery_Operations_Associate__c != null 
                   && project.Delivery_Operations_Associate__c != mapOldProjects.get(project.Id).Delivery_Operations_Associate__c
                 ) {    
                     if(!mapOfUser.containsKey(project.Id)) {
                         userIdSet = new Set<Id>();
                         userIdSet.add(project.Delivery_Operations_Associate__c );
                         mapOfUser.put(project.id, userIdSet);
                     } else {
                         userIdSet = new Set<Id>();
                         userIdSet = mapOfUser.get(project.id);
                         userIdSet.add(project.Delivery_Operations_Associate__c );
                         mapOfUser.put(project.id, userIdSet);
                     }
                     if(!ProjectShareMap.containsKey(project.Id)) {
                        userIdSet = new Set<Id>();
                        userIdSet.add(mapOldProjects.get(project.Id).Delivery_Operations_Associate__c);
                        ProjectShareMap.put(project.id, userIdSet);
                    } else {
                        userIdSet = new Set<Id>();
                        userIdSet = ProjectShareMap.get(project.id);
                        userIdSet.add(mapOldProjects.get(project.Id).Delivery_Operations_Associate__c );
                        ProjectShareMap.put(project.id, userIdSet);
                    }
                } else if(project.Delivery_Operations_Associate__c == null 
                          && mapOldProjects.get(project.Id).Delivery_Operations_Associate__c != null) {
                    if(!ProjectShareMap.containsKey(project.Id)) {
                        userIdSet = new Set<Id>();
                        userIdSet.add(mapOldProjects.get(project.Id).Delivery_Operations_Associate__c);
                        ProjectShareMap.put(project.id, userIdSet);
                    } else {
                        userIdSet = new Set<Id>();
                        userIdSet = ProjectShareMap.get(project.id);
                        userIdSet.add(mapOldProjects.get(project.Id).Delivery_Operations_Associate__c );
                        ProjectShareMap.put(project.id, userIdSet);
                    }
                }  
            }
        }
        if(ProjectShareMap.size() > 0)
            removeShareProjectRecords(ProjectShareMap);
        
        if(mapOfUser.size() > 0)
            shareProjectRecordsWithUser(mapOfUser);
    }
    
    /**
    * Method Name : shareProjectRecordsWithUser
    * Parameters  : param1: Map<Id,Set<Id>>
    * Description : Used to share the Project users with created Project share.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 260819
    **/
    public static void shareProjectRecordsWithUser(Map<Id,Set<Id>> userMapOfIds) {
        List<Project__Share> projectShareList = new List<Project__Share>();
        Map<Id,Set<Id>> mapOfProjectShare = new Map<Id,Set<Id>>();
        Set<Id> userIdSet = new Set<Id>();
        if(userMapOfIds.size() > 0) {            
            for(Project__Share pShare : [ SELECT Id,
                                                 RowCause,
                                                 AccessLevel,
                                                 UserOrGroupId,
                                                 ParentId 
                                         FROM Project__Share
                                         WHERE ParentId IN : userMapOfIds.Keyset()]
             ) {
                for(Id userId : userMapOfIds.get(pShare.ParentId)) {
                    if(pShare.AccessLevel != 'Edit' && pShare.UserOrGroupId == userId) {
                        if(!mapOfProjectShare.containsKey(pShare.ParentId)) {
                            userIdSet = new Set<Id>();
                            userIdSet.add(pShare.UserOrGroupId);
                            mapOfProjectShare.put(pShare.ParentId, userIdSet);
                          } else {
                              userIdSet = new Set<Id>();
                              userIdSet = mapOfProjectShare.get(pShare.ParentId);
                              userIdSet.add(pShare.UserOrGroupId);
                              mapOfProjectShare.put(pShare.ParentId, userIdSet);
                          }
                    }
                }    
            }

            for(Id projectId : userMapOfIds.Keyset()) {
                for(Id userId : userMapOfIds.get(projectId)) {
                    Project__Share projectShare = new Project__Share();
                    projectShare.ParentId = projectId;
                    projectShare.UserOrGroupId = userId;
                    projectShare.RowCause = Schema.Project__Share.RowCause.Project_Team__c;
                    projectShare.AccessLevel = 'Edit';
                    projectShareList.add(projectShare);
                }
            }//End of for

            for(Id projectId : mapOfProjectShare.Keyset()) {
                for(Id userId : mapOfProjectShare.get(projectId)) {
                    Project__Share projectShare = new Project__Share();
                    projectShare.ParentId = projectId;
                    projectShare.UserOrGroupId = userId;
                    projectShare.RowCause = Schema.Project__Share.RowCause.Project_Team__c;
                    projectShare.AccessLevel = 'Edit';
                    projectShareList.add(projectShare);
                }
            }//End of for
            
            if(projectShareList.size() > 0)
                insert projectShareList;
        }
        
    }
    
    /**
    * Method Name : removeShareProjectRecords
    * Parameters  : param1: List<Project__c> 
    * Description : Used to Delete shared the Project Record.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 260819
    **/
    public static void removeShareProjectRecords(Map<Id, Set<Id>> ProjectShareMap) {
        if(!ProjectShareMap.isEmpty()) {
            List<Project__Share> projectShareListToDelete = new List<Project__Share>();
            try { 
                for(Project__Share projectShare : [SELECT Id,
                                                       ParentId,
                                                       RowCause,
                                                       AccessLevel,
                                                       UserOrGroupId
                                                   FROM Project__Share
                                                   WHERE ParentId IN :ProjectShareMap.keySet()]
                ){
                    for(Id userGroupId : ProjectShareMap.get(projectShare.ParentId) ){
                        if(projectShare.UserOrGroupId == String.valueOf(userGroupId).substring(0, 15)){
                            projectShareListToDelete.add(projectShare);
                        }
                    }
                    
                }
                
                delete projectShareListToDelete;
            } catch(Exception e) {
                System.debug('==ERROR in DELETION =='+e);
            }
        }//End of if
    }
}