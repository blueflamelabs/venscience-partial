/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        141019         VennScience_BFL_Monali                              This is the test class for DeliverableTriggerHandler apex class. 
*                               
**********************************************************************************************************************************************************/
@isTest
public class DeliverableTriggerHandlerTest {
    
    /**
    * Method Name : shareDeliverableRecordPositivetest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 141019
    **/
    @isTest
    public static void shareDeliverableRecordPositivetest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
            Project__c projectRecord = TestDataFactory.createNewProject();
            insert projectRecord;
            
            Project_Team__c projectTeamRec = TestDataFactory.createProjectTeamRec(projectRecord.Id, uWrap.communityUser.Id);
            insert projectTeamRec;
            
            Deliverable__c deliverableRec = TestDataFactory.createDeliverableRec(projectRecord.Id);
            
            Test.startTest();
            insert deliverableRec;
            Test.stopTest();
            
            List<Deliverable__Share> deliverableShareRecordList = [SELECT Id
                                                                   FROM Deliverable__Share
                                                                   WHERE ParentId =: deliverableRec.Id];
            System.assertEquals(1, deliverableShareRecordList.size());
        }
    }
    
    /**
    * Method Name : shareDeliverableRecordNegativetest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 141019
    **/
    @isTest
    public static void shareDeliverableRecordNegativetest() {
        
        Project__c projectRecord = TestDataFactory.createNewProject();
        insert projectRecord;
        
        Deliverable__c deliverableRec = TestDataFactory.createDeliverableRec(projectRecord.Id);
        
        Test.startTest();
        insert deliverableRec;
        Test.stopTest();
        
        List<Deliverable__Share> deliverableShareRecordList = [SELECT Id
                                                              FROM Deliverable__Share
                                                              WHERE ParentId =: deliverableRec.Id];
        System.assertEquals(0, deliverableShareRecordList.size());
    }
    
    /**
    * Method Name : shareDeliverableRecordPositiveBulktest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 141019
    **/
    @isTest
    public static void shareDeliverableRecordPositiveBulktest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createMultiplePartnerCommunityUserTest();
        System.runAs(uWrap.adminUser) {
            Project__c projectRecord = TestDataFactory.createNewProject();
            insert projectRecord;
            
            Project_Team__c projectTeamRec = TestDataFactory.createProjectTeamRec(projectRecord.Id, uWrap.listofCommunityUser[0].Id);
            insert projectTeamRec;
            
            List<Deliverable__c> deliverableRecList = TestDataFactory.createDeliverableListTest(projectRecord.Id, 5);
            
            Test.startTest();
            insert deliverableRecList;
            Test.stopTest();
            
            List<Deliverable__Share> deliverableShareRecordList = [SELECT Id
                                                                   FROM Deliverable__Share
                                                                   WHERE ParentId = :deliverableRecList[0].Id];
            System.assertEquals(1, deliverableShareRecordList.size());
        }
    }
    
    /**
    * Method Name : updateDeliverableRecordPositivetest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 161019
    **/
    @isTest
    public static void updateDeliverableRecordPositivetest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
            
            User communityUser = TestDataFactory.createPartnerCommunityUserTest();
            
            Project__c projectRecord = TestDataFactory.createNewProject();
            insert projectRecord;
            System.debug('projectRecord - >'+projectRecord);
            
            Project_Team__c projectTeamRec = TestDataFactory.createProjectTeamRec(projectRecord.Id, uWrap.communityUser.Id);
            insert projectTeamRec;
            
            Project__c projectRecord1 = TestDataFactory.createNewProject();
            insert projectRecord1;
			System.debug('projectRecord1 - >'+projectRecord1); 
            
            Project_Team__c projectTeamRec1 = TestDataFactory.createProjectTeamRec(projectRecord1.Id, communityUser.Id);
            insert projectTeamRec1;
            
            Deliverable__c deliverableRec = TestDataFactory.createDeliverableRec(projectRecord.Id);
            insert deliverableRec;
            
            List<Deliverable__c> updatedDeliverableList = [SELECT Id, 
                                                                  Project__c
                                                          FROM Deliverable__c
                                                          WHERE Id = :deliverableRec.Id];
            System.debug('updatedDeliverableList - >'+updatedDeliverableList);
            updatedDeliverableList[0].Project__c = projectRecord1.Id; 
            Test.startTest();
            update updatedDeliverableList;
            Test.stopTest();
            
            System.debug('after updatedDeliverableList - >'+updatedDeliverableList);
            
            List<Deliverable__Share> deliverableShareRecordList = [SELECT Id,
                                                                          ParentId
                                                                   FROM Deliverable__Share];
            System.assertEquals(1, deliverableShareRecordList.size());
        }
    }
    
    /**
    * Method Name : updateDeliverableRecordtest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 161019
    **/
    @isTest
    public static void updateDeliverableRecordtest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
            
            User communityUser = TestDataFactory.createPartnerCommunityUserTest();
            
            Project__c projectRecord = TestDataFactory.createNewProject();
            insert projectRecord;
            System.debug('projectRecord - >'+projectRecord);
            
            //Project_Team__c projectTeamRec = TestDataFactory.createProjectTeamRec(projectRecord.Id, uWrap.communityUser.Id);
           // insert projectTeamRec;
            
            Project__c projectRecord1 = TestDataFactory.createNewProject();
            insert projectRecord1;
			System.debug('projectRecord1 - >'+projectRecord1); 
            
            Project_Team__c projectTeamRec1 = TestDataFactory.createProjectTeamRec(projectRecord1.Id, communityUser.Id);
            insert projectTeamRec1;
            
            Deliverable__c deliverableRec = TestDataFactory.createDeliverableRec1();
            deliverableRec.Project__c = projectRecord1.Id;
            insert deliverableRec;
            
            List<Deliverable__c> updatedDeliverableList = [SELECT Id, 
                                                                  Project__c
                                                          FROM Deliverable__c
                                                          WHERE Id = :deliverableRec.Id];
            System.debug('updatedDeliverableList - >'+updatedDeliverableList);
            updatedDeliverableList[0].Project__c = null; 
            Test.startTest();
            update updatedDeliverableList;
            Test.stopTest();
            
            System.debug('after updatedDeliverableList - >'+updatedDeliverableList);
            
            List<Deliverable__Share> deliverableShareRecordList = [SELECT Id,
                                                                          ParentId
                                                                   FROM Deliverable__Share
                                                                   WHERE ParentId = :updatedDeliverableList[0].Id];
            System.assertEquals(0, deliverableShareRecordList.size());
        }
    }
    
    /**
    * Method Name : updateDeliverableWithprojectRecordtest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 161019
    **/
    @isTest
    public static void updateDeliverableWithprojectRecordtest() {
        
        TestDataFactory.UserWrapper uWrap = new TestDataFactory.UserWrapper();
        uWrap = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(uWrap.adminUser) {
            
            User communityUser = TestDataFactory.createPartnerCommunityUserTest();
            
            Project__c projectRecord = TestDataFactory.createNewProject();
            insert projectRecord;
            System.debug('projectRecord - >'+projectRecord);
            
            //Project_Team__c projectTeamRec = TestDataFactory.createProjectTeamRec(projectRecord.Id, uWrap.communityUser.Id);
           // insert projectTeamRec;
            
            Project__c projectRecord1 = TestDataFactory.createNewProject();
            insert projectRecord1;
			System.debug('projectRecord1 - >'+projectRecord1); 
            
            Project_Team__c projectTeamRec1 = TestDataFactory.createProjectTeamRec(projectRecord1.Id, communityUser.Id);
            insert projectTeamRec1;
            
            Deliverable__c deliverableRec = TestDataFactory.createDeliverableRec1();
            insert deliverableRec;
            
            List<Deliverable__c> updatedDeliverableList = [SELECT Id, 
                                                                  Project__c
                                                          FROM Deliverable__c
                                                          WHERE Id = :deliverableRec.Id];
            System.debug('updatedDeliverableList - >'+updatedDeliverableList);
            updatedDeliverableList[0].Project__c = projectRecord1.Id; 
            Test.startTest();
            update updatedDeliverableList;
            Test.stopTest();
            
            System.debug('after updatedDeliverableList - >'+updatedDeliverableList);
            
            List<Deliverable__Share> deliverableShareRecordList = [SELECT Id,
                                                                          ParentId
                                                                   FROM Deliverable__Share
                                                                   WHERE ParentId = :updatedDeliverableList[0].Id];
            System.assertEquals(1, deliverableShareRecordList.size());
        }
    }

}