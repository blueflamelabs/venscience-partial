/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        240619         BFL Users     This class is built to Create TimeSheet Record Related to Task.
**********************************************************************************************************************************************************/ 
public class CreateTimeRelatedTaskCtrl {
    
    //public String projectId;
    
    // to get the Task record Related to Task Id.
    @AuraEnabled
    public static agf__ADM_Task__c getTaskRecord(String recordId) {
        
        agf__ADM_Task__c taskObj = [ SELECT Id, 
                                            Name, 
                                    		agf__Work__r.agf__Epic__r.Project__r.Name,
                                    		agf__Work__r.agf__Epic__r.Project__r.Id
                                    FROM agf__ADM_Task__c 
                                    WHERE Id =: recordId];
        //CreateTimeRelatedTaskCtrl obj = new CreateTimeRelatedTaskCtrl();
        //obj.projectId = taskObj.agf__Work__r.agf__Epic__r.Project__r.Id;
        //System.debug('projectId-->' +obj.projectId);
        
        System.debug('--task--'+taskObj);
        return taskObj;
    }
    
    // to get the current logged in user.
    @AuraEnabled 
    public static User fetchUser() {
        User u = [ SELECT Id, Name
                   FROM user 
                   WHERE Id =: UserInfo.getUserId()];
        //String u =  userInfo.getName();
        return u;
    }
    
    // to get the Deliverable__c records which has status value 'In Progress';
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String projectIdObj) {
        
        
        //CreateTimeRelatedTaskCtrl obj1 = new CreateTimeRelatedTaskCtrl();
        //String projectIdObj = obj1.projectId ;
        System.debug('projectIdObj-->' +projectIdObj);
        
        String cancelValue = 'Cancelled';
        String completeValue = 'Completed';
        
        System.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < Deliverable__c > returnList = new List < Deliverable__c > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name, Status__c from ' +ObjectName + ' WHERE Project__c =: projectIdObj AND Name LIKE: searchKey order by createdDate DESC limit 5';
        List < Deliverable__c > lstOfRecords = Database.query(sQuery);
        
        System.debug('-lstOfRecords-36--'+lstOfRecords);
        for (Deliverable__c obj: lstOfRecords) {
            if(!obj.Status__c.equalsIgnoreCase(cancelValue) && !obj.Status__c.equalsIgnoreCase(completeValue)){ 
                returnList.add(obj);
            }
        }
        system.debug('returnList-->' + returnList);
        return returnList;
    }
    
    // get the picklist value of Category and Type field of Timesheet Object.
    @AuraEnabled
    public static List<String> getPicklistvalues(String objectName, String field_apiname) {
        
        List<String> optionlist = new List<String>();
        
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        optionlist.add('--None--');
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        return optionlist;
    }
    
    // Created TimeSheet Record Which is related to Task.
    @AuraEnabled
    public static successMsg saveTimeSheet(String taskRecord, String userId, String Deliverable,String sheetdate,  
                                       String Category,String type, Double hours, String Description, String project) {
                                           Date mySheetDate = Date.valueOf(sheetdate);
                                           System.debug('--58 date--'+mySheetDate);
                                           System.debug('--59 date--'+Category);
                                           System.debug('--60 date--'+taskRecord);
                                           System.debug('--61 date--'+Deliverable);
                                           System.debug('--61 userId--'+userId);
                                           successMsg msgObj = new successMsg(); 
                                               
                                           try{
                                               Timesheet__c ts = new Timesheet__c();
                                               ts.Consultant__c = userId;
                                               ts.Category__c =   Category ;
                                               ts.Date__c = mySheetDate;
                                               ts.Deliverable__c = Deliverable;
                                               ts.Type__c = type;
                                               ts.Hours__c = hours;
                                               ts.Task__c = taskRecord;
                                               ts.Description__c = Description;
                                               ts.Project__c = project;
                                               
                                               insert ts;
                                               System.debug('--timeRec--'+ts);        
                                               msgObj.succMsg = 'Saved Successfully';
                                               System.debug('--msg--'+msgObj.succMsg); 
                                           }catch(Exception ex){
                                               
                                               if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                                                   String str = ex.getMessage().split(', ')[1];
                                                   System.debug('--str---'+str);
                                                  msgObj.errorMsg = str;  
                                               }
                                              msgObj.errorMsg = ex.getMessage();
                                           }
                                           return msgObj;
                                       }
    // get the selected field values.
    @AuraEnabled
    public static List<String> getselectOptions(String fld){
        Schema.sObjectType objType = Timesheet__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        List<String> options = new List<String>();
        options.add('--None--');
        for (Schema.PicklistEntry f: values) {
            options.add(f.getValue());
        }
        
        return options;
    }
    
    @AuraEnabled
    public static List < sObject > fetchUserLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    @AuraEnabled
    public static List < sObject > getDefaultUserLookUpValues(String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        
        List < sObject > returnList = new List < sObject > ();
       String searchKey = userInfo.getUserId();  
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Id =: searchKey';
        system.debug('sQuery ====' +sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
      
        return returnList;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<RecordsData> fetchRecords( String objectName, String filterField, String searchString ) {
        List<RecordsData> recordsDataList = new List<RecordsData>();
        try {
            String query = 'SELECT Id, ' + filterField+
                ' FROM '+objectName+
                ' WHERE IsActive = true AND '+filterField+' LIKE ' + '\'' + String.escapeSingleQuotes(searchString.trim()) + '%\'' + ' LIMIT 50000';
            
            for(SObject s : Database.query(query)){
                RecordsData recordsData = new RecordsData();
                recordsData.value = String.valueOf(s.get('id'));
                recordsData.label = String.valueOf(s.get(filterField));
                recordsDataList.add(recordsData);
            } 
        } catch (Exception err) {
            if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                throw new AuraHandledException(err.getMessage());
            }
        }
        System.debug('--recordsDataList--'+recordsDataList);
        return recordsDataList;
    }
    @AuraEnabled
    public static List<RecordsData> fetchCurrentUserRecord(String userIds){
        System.debug('--userIds--'+userIds);
        List<RecordsData> recordsDataList = new List<RecordsData>();
        User u = [Select Id,Name From User where Id =: userIds];
       // user u = [Select Id, Name,Title from user where id  = : userInfo.getUserId()]; 
          //  if(u.Title == 'Partner'|| u.Title == 'Principal'){
                RecordsData recordsData = new RecordsData();
                recordsData.value = u.Id;
                recordsData.label = u.Name;
                recordsDataList.add(recordsData);
       // }
        system.debug('recordsDataList ' +recordsDataList);
        return recordsDataList; 
    }
   
    public class RecordsData{
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
    }
    
    public class successMsg{
        @AuraEnabled 
        public String succMsg {get;set;}
        @AuraEnabled 
        public String errorMsg {get;set;}
    }

}