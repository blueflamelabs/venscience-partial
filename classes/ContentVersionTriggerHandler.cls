/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        250719         VennScience_BFL_Amruta       This is the handler class for ContentVersionTrigger
                               
**********************************************************************************************************************************************************/
public Class ContentVersionTriggerHandler {
    
    /**
    * Method Name : updateFileVisibilityForTasks
    * Parameters  : param1: List<ContentVersion>
    * Description : Used to update the file visibility to 'All Users' when the file is uploaded on Task object.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    public static void updateFileVisibilityForTasks(List<ContentVersion> listContentVersion) {
    
        // Variable Declarations
        List<ContentDocumentLink> listContentDocLink = new List<ContentDocumentLink>();
        List<ContentDocumentLink> listContentDocToBeUpdated = new List<ContentDocumentLink>();
        Set<Id> setContentDocumentId = new Set<Id>();
        
        // Iterate on Trigger.New list
        for(ContentVersion objContentVersion : listContentVersion) {
            setContentDocumentId.add(objContentVersion.ContentDocumentId);
        } // End of for
        // Get Task object key prefix
        Schema.DescribeSObjectResult sObjResult = agf__ADM_Task__c.SObjectType.getDescribe();
        String taskKeyPrefix = sObjResult.getKeyPrefix();
        // Fetched the related Content Document Link records
        listContentDocLink = [SELECT Id,
                                     LinkedEntityId,
                                     Visibility
                                FROM ContentDocumentLink
                               WHERE ContentDocumentId IN :setContentDocumentId];
        for(ContentDocumentLink objContentDocLink : listContentDocLink) {
            String linkedEntityIdPrefix = String.valueOf(objContentDocLink.LinkedEntityId).substring(0,3);
            //Check if the prefix matches with requested prefix
            if(String.isNotBlank(linkedEntityIdPrefix) && linkedEntityIdPrefix.equals(taskKeyPrefix)){
                objContentDocLink.Visibility = 'AllUsers';
                listContentDocToBeUpdated.add(objContentDocLink);
            } // End of if
        } // End of for
        // Update the ContentDocumentLink records
        if(!listContentDocToBeUpdated.isEmpty()) {
            try {
                update listContentDocToBeUpdated;
            } catch(Exception e) {
                System.debug('Error occured while updating ContentDocumentLink==========='+e.getMessage());
            } // End of for
        } // End of if   
    }
}