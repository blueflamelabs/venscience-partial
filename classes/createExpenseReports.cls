global class createExpenseReports implements Schedulable{
    
    global void execute(SchedulableContext ctx) {
        
        Expense_Report__c[] expReportsList = new List<Expense_Report__c>();
        //250719 T - 00400 initialise the Set of userId VennScience_BFL_Monali
        Set<Id> setOfOwnerIds = new Set<Id>();
        
        //2590719 T - 00400 initialise the Set of expense report ownerId VennScience_BFL_Monali
        Set<Id> setOfExpOwnerId = new Set<Id>();
        
        //250719 T - 00400 Initialize set of Owner Ids VennScience_BFL_Monali
        Set<Id> setOfIds = new Set<Id>();
        
        //250719 T - 00400 iterate on user list VennScience_BFL_Monali
        String currentMonthYear = String.valueOf(System.Today().month())+'/'+1+'/'+String.valueOf(System.Today().year());
        // System.debug('--currentMonthYear--'+currentMonthYear);
        
        String yearString = string.valueOf(System.Today().year());
        // System.debug('yearString======'+yearString);
        
        Integer thisMonth = Integer.valueOf(System.Today().month());
        
        String thisYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().year());
        // System.debug('--thisYear--'+thisYear);
        
        String nextYear = String.valueOf(System.Today().addMonths(1).month())+'/'+1+'/'+String.valueOf(System.Today().addYears(1).year());
        // System.debug('--nextYear--'+nextYear);
        //210819 T - 00400 Created Map to put the related expense Report VennScience_BFL_Monali.
        Map<Id,List<Expense_Report__c>> mapOfExpenseReport = new Map<Id,List<Expense_Report__c>>();
        List<Expense_Report__c> newExpReportsList = new List<Expense_Report__c>();
        
        List<User> usersList = [SELECT Id FROM User WHERE IsActive = TRUE AND Create_Expense_Reports__c=TRUE];
        // System.debug('users=='+usersList);
        // System.debug('users=size='+usersList.size());
        //250719 T - 00400 iterate on user list to get the related expense reports VennScience_BFL_Monali
        for(User u : usersList) {
            setOfOwnerIds.add(u.Id);
        }
        //250719 T - 00400 query to get the Expense Record related to updated user VennScience_BFL_Monali
        List<Expense_Report__c> currExpList = [SELECT Id, 
                                               Quarter__c,
                                               OwnerId, 
                                               Year__c,Start_Date__c 
                                               FROM Expense_Report__c 
                                               WHERE OwnerId In : setOfOwnerIds 
                                               AND Year__c = :yearString ];
        // System.debug('currExpList======'+currExpList);
        
        for(Expense_Report__c exp : currExpList) {
            //210819 - T- 00400 to filter for the current month report VennScience_BFL_Monali
            if(exp.OwnerId != null) {
                if(!mapOfExpenseReport.containsKey(exp.OwnerId)) {
                    mapOfExpenseReport.put(exp.OwnerId,new List<Expense_Report__c>{exp});
                }else {
                    List<Expense_Report__c> listofexpReport = new List<Expense_Report__c>();
                    listofexpReport.addAll(mapOfExpenseReport.get(exp.OwnerId));
                    listofexpReport.add(exp);
                    // System.debug('---listofexpReport--'+listofexpReport);
                    mapOfExpenseReport.put(exp.OwnerId,listofexpReport);
                }
            }
        }//End of loop
        // System.debug('---mapOfExpenseReport--'+mapOfExpenseReport);
        // System.debug('---mapOfExpenseReport--'+mapOfExpenseReport.values());
        // System.debug('---mapOfExpenseReport--'+mapOfExpenseReport.values().size());
        for(User user : usersList) {
            if(!mapOfExpenseReport.containsKey(user.Id)) {
                setOfIds.add(user.Id);
            }
        }
        //210819 - T- 00400 to filter for the current month report VennScience_BFL_Monali
        
        if(mapOfExpenseReport.keyset() != null) {
            // System.debug('inside if part');
            for(Id userId : mapOfExpenseReport.keySet()) {
                Boolean isCurrentMonth = false;
                Boolean isNextMonth = false;
                // System.debug('inside user keyset======'+userId);
                // System.debug('mapOfExpenseReport.get(userId)======'+mapOfExpenseReport.get(userId));
                for(Expense_Report__c expReport : mapOfExpenseReport.get(userId)) {
                    // System.debug('inside expense report======='+expReport);
                    if(expReport.Start_Date__c == date.parse(currentMonthYear)) {
                        // System.debug('current month expense report');
                        isCurrentMonth = true;
                    }//End of if 
                    // System.debug('thisYear= value='+thisYear);
                    if(expReport.Start_Date__c == date.parse(thisYear)) {
                        // System.debug('next month expense report');
                        isNextMonth = true;
                    }//End of if
                }
                // System.debug('isCurrentMonth=='+isCurrentMonth);
                // System.debug('isNextMonth=='+isNextMonth);
                if(!isCurrentMonth && isNextMonth) {
                    // System.debug('created current month expense report');
                    Expense_Report__c exp1 = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp1.Start_Date__c = date.parse(currentMonthYear);
                        exp1.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp1.Start_Date__c = date.parse(nextYear);
                        exp1.Year__c = String.valueOf(System.Today().addYears(1).year());
                    }
                    expReportsList.add(exp1);
                    // System.debug('expReportsList==='+expReportsList);
                }
                if(isCurrentMonth && !isNextMonth) {
                    // System.debug('created next month expense report');
                    Expense_Report__c exp = new Expense_Report__c(
                        Name='Temp Name', 
                        Status__c = 'Open',
                        OwnerId = userId);
                    if(thisMonth!=12){
                        exp.Start_Date__c = date.parse(thisYear);
                        exp.Year__c = String.valueOf(System.Today().year());
                    }
                    else{
                        exp.Start_Date__c = date.parse(nextYear);
                        exp.Year__c = String.valueOf(System.Today().addYears(1).year());
                        
                    }
                    expReportsList.add(exp);
                    // System.debug('expReportsList==='+expReportsList);
                }
            }
            insert expReportsList;
            // System.debug('expReportsList=after insert=='+expReportsList); 
        }
        //230819 T - 00400 Create Expense report for those users which has no Expense report  VennScience_BFL_Monali
        if(!setOfIds.isEmpty()) {
            // System.debug('set is not empty'); 
            for(Id userId : setOfIds) {
                
                Expense_Report__c exp = new Expense_Report__c(
                    Name='Temp Name', 
                    Status__c = 'Open',
                    OwnerId = userId);
                if(thisMonth!=12){
                    exp.Start_Date__c = date.parse(thisYear);
                    exp.Year__c = String.valueOf(System.Today().year());
                }
                else{
                    exp.Start_Date__c = date.parse(nextYear);
                    exp.Year__c = String.valueOf(System.Today().addYears(1).year());
                    
                }
                newExpReportsList.add(exp);
                
                //250719 T - 00400 add new  VennScience_BFL_Monali
                Expense_Report__c exp1 = new Expense_Report__c(
                    Name='Temp Name', 
                    Status__c = 'Open',
                    OwnerId = userId);
                if(thisMonth!=12){
                    exp1.Start_Date__c = date.parse(currentMonthYear);
                    exp1.Year__c = String.valueOf(System.Today().year());
                }//End of of
                else{
                    exp1.Start_Date__c = date.parse(nextYear);
                    exp1.Year__c = String.valueOf(System.Today().addYears(1).year());
                    
                }//End of else
                newExpReportsList.add(exp1);
                // System.debug('exp1===exp1=='+exp1); 
                
            }//End for loop
            insert newExpReportsList;
            // System.debug('newExpReportsList===expReports=='+newExpReportsList); 

        }
    }
}