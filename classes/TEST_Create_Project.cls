@IsTest (SeeAllData=false) private class TEST_Create_Project{

    /* This is a basic test which simulates the primary positive case for the 
       save method on an Estimate. */

private static testMethod void myUnitTest() {

Account acc=new Account(Name='test');
insert acc;                
   
    // create the opportunity
        Opportunity opp1 = new Opportunity(
        name='Test Opp 1',
        StageName = 'Closed Won',
        CloseDate = Date.newInstance(2009,01,01), 
        Service__c='Implementation',
        SOW_Date__c=Date.newInstance(2009,01,01),
        SOW_Expiration_Date__c=Date.newInstance(2009,01,01),
        State_Tax_Purposes__c='NH',
        Create_Project__c=TRUE
               
              
    );
    insert opp1;
    
 opp1 =  [SELECT Id,StageName,Service__c FROM Opportunity 
             WHERE Id = :opp1.Id];
             
    System.assertEquals('Closed Won', opp1.StageName);
  
   // add the line item
    Project__c proj = new Project__c(
    Stage__c= 'Pre-Kickoff',
    //SOW_Date__c= opp1.SOW_Date__c,
    Opportunity__c = opp1.id,
    Name='Test Proj'
    );
    insert proj;
  
          }
}