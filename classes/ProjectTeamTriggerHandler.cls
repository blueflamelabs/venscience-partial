/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        071019         VennScience_BFL_Amruta                              This is the handler class for ProjectTeamTrigger. Used to filter the 
*                                                                                   Project Team records as per the required criteria.
*                               
**********************************************************************************************************************************************************/
public class ProjectTeamTriggerHandler {
    
	/**
    * Method Name : filterProjectTeamRecords
    * Parameters  : param1: List<Project_Team__c>, param2: Map<Id,Project_Team__c>
    * Description : Used to filter the Project Team records as per the required criteria.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 071019
    **/
    public static void filterProjectTeamRecords(List<Project_Team__c> listProjectTeam, Map<Id,Project_Team__c> projectTeamOldMap) {
    	// Variable Declarations
        //List<Project_Team__c> listFilteredProjectTeam = new List<Project_Team__c>();
        Set<Id> setProjectTeamId = new Set<Id>();
        Map<Id,Set<Id>> mapTeamMemIdVSProjectIdSet = new Map<Id,Set<Id>>();
        
        // Iterate over Project Team list(Trigger.New)
        for(Project_Team__c projectTeamRecord : listProjectTeam) {
            // Filter Project Team record to check if Team Member is populated or not
            if(projectTeamOldMap.isEmpty() && projectTeamRecord.Team_Member__c != null) {
                // Populate set of Project Team Id
                setProjectTeamId.add(projectTeamRecord.Id);
            } // End of if
            // Filter Project Team record to check if Team Member is updated and is removed
            if(!projectTeamOldMap.isEmpty() && 
               projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c != projectTeamRecord.Team_Member__c &&
               projectTeamRecord.Team_Member__c == null) {
                // Populate map of Team Member Id VS Set of parent Project Id
                if(!mapTeamMemIdVSProjectIdSet.containsKey(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c)) {
                    mapTeamMemIdVSProjectIdSet.put(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c,
                                                   new Set<Id>{projectTeamOldMap.get(projectTeamRecord.Id).Project__c});       
                } else {
                    Set<Id> setPreviousProId = new Set<Id>();
                    setPreviousProId = mapTeamMemIdVSProjectIdSet.get(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c);
                	setPreviousProId.add(projectTeamOldMap.get(projectTeamRecord.Id).Project__c);
                    mapTeamMemIdVSProjectIdSet.put(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c,
                                                   setPreviousProId);
                } // End of if-else block
            } // End of if
            // Filter Project Team record to check if Team Member is updated
            if(!projectTeamOldMap.isEmpty() && 
               projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c != projectTeamRecord.Team_Member__c != null &&
               projectTeamRecord.Team_Member__c != null) {
                // Populate set of Project Team Id
                setProjectTeamId.add(projectTeamRecord.Id);
                // Populate map of Team Member Id VS Set of parent Project Id
                if(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c != null) {
                    if(!mapTeamMemIdVSProjectIdSet.containsKey(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c)) {
                        mapTeamMemIdVSProjectIdSet.put(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c,
                                                       new Set<Id>{projectTeamOldMap.get(projectTeamRecord.Id).Project__c});       
                    } else {
                        Set<Id> setPreviousProId = new Set<Id>();
                        setPreviousProId = mapTeamMemIdVSProjectIdSet.get(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c);
                        setPreviousProId.add(projectTeamOldMap.get(projectTeamRecord.Id).Project__c);
                        mapTeamMemIdVSProjectIdSet.put(projectTeamOldMap.get(projectTeamRecord.Id).Team_Member__c,
                                                       setPreviousProId);
                    } // End of if-else block       
                } // End of ouer if 
            } // End of if
        } // End of for
        
        // @Reminder: Remove debug logs
        // System.debug('setProjectTeamId======'+setProjectTeamId);
        // System.debug('mapTeamMemIdVSProjectIdSet======'+mapTeamMemIdVSProjectIdSet);
        // Call Controller method to share Project, Work and Deliverable records with added Project Team's team member
        if(!setProjectTeamId.isEmpty()) {
            ProjectTeamTriggerController.shareRecordWithProjectTeamMember(setProjectTeamId);
    	} // End of if
        
        // Call Controller method to remove share records for Project, Work and Deliverable records with removed team member
        if(!mapTeamMemIdVSProjectIdSet.isEmpty()) {
            ProjectTeamTriggerController.removeShareRecordWithProjectTeamMember(mapTeamMemIdVSProjectIdSet);
        } // End of if
    }
}