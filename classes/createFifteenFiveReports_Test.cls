@isTest
private class createFifteenFiveReports_Test {

    // Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().

    @testSetup
    static void setupTestData(){
        test.startTest();
        
        // 031019 - VennScience_BFL_Amruta - Modified Create_Performance_Reviews__c = true in User's test data
        User user_Obj = new User(Username = 'TestUser7330720190424143759@codecoverage.com', LastName = 'LastName804', 
                                 Email = 'Email24@test.com', Alias = 'Alias913', CommunityNickname = 'cNickName61741', 
                                 IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', 
                                 EmailEncodingKey = 'UTF-8', ProfileId = '00ei0000000pfmZ', LanguageLocaleKey = 'en_US',
                                 Create_Expense_Reports__c = false, Create_Performance_Reviews__c = true);
        Insert user_Obj; 
        X15_5_Report__c x15_5_report_Obj = new X15_5_Report__c(Name = 'Temp Name', Date__c = Date.today(), Employee__c = user_Obj.id, OwnerId = user_Obj.id, Status__c = 'Submitted to Employee');
        Insert x15_5_report_Obj; 
        test.stopTest();
    }
    static testMethod void test_execute_UseCase1(){
        List<User> user_Obj  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c from User];
        List<X15_5_Report__c> x15_5_report_Obj  =  [SELECT Name,Date__c,Employee__c,Status__c from X15_5_Report__c];
        createFifteenFiveReports obj01 = new createFifteenFiveReports();

    }

    public static String CRON_EXP = '0 0 0 1 3 ? 2022';

    static testmethod void testScheduledJob() {
        List<User> users = [SELECT Id FROM User WHERE IsActive = TRUE AND create_performance_reviews__c = TRUE ];

        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
        CRON_EXP, 
        new createFifteenFiveReports());      
        // Verify the scheduled job has not run yet.
        List<X15_5_Report__c> lt = [SELECT Id 
        FROM X15_5_Report__c];
        System.assertEquals(1, lt.size(), 'Reports Exist Before Job Runs');
        // Stopping the test will run the job synchronously
        Test.stopTest();
    }

}