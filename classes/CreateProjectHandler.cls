/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                                      Description
*     1.0        090719         VennScience_BFL_Amruta www.vennscience.com     This class is the handler class of CreateProject Trigger.
**********************************************************************************************************************************************************/ 
public class CreateProjectHandler {

    // Variable Declarations
    List<Project__c> ProjectList = new List<Project__c>();
    Map<string,string> mapOppIds = new Map<string,string>();
    List<Opportunity> Opportunitylist =new List<Opportunity>();
    List<Deliverable__c> DeliverableList =new List<Deliverable__c>();
    Map<string,list<opportunity>> mapOfStrQuota = new Map<string,list<opportunity>>();


    /**
    * Method Name : insertProjectForOpp
    * Parameters  : List<Opportunity>, Map<Id,Opportunity>
    * Description : This method is used to create Project records for each Opportunity where Delivery Status 
    *               has been set to Approved
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 090719
    **/
    public void insertProjectForOpp(List<Opportunity> listNewOpp, Map<Id,Opportunity> oldOppMap) {
		
        
        // Variable Declarations
        Set<Id> setExistingProjectId = new Set<Id>();
        List<Project__c> listExistingProjects = new List<Project__c>();
        List<Project__c> listProjectsToBeUpdated = new List<Project__c>();
        Map<Id,Id> mapOppIdVSForcastedManagerId = new Map<Id,Id>();
        
        // Iterate over new opportunity list
        for(Opportunity objOpp : listNewOpp) {
            if(!oldOppMap.isEmpty() && objOpp.delivery_status__c  != oldOppMap.get(objOpp.id).delivery_status__c && 
               objOpp.delivery_status__c=='Approved' && objOpp.Active_Project__c == null) {

                // Called from after update trigger
                Project__c proj = new Project__c();
                // Calling method to set the Project record values
                proj = setProjectValues(objOpp);
                ProjectList.add(proj);
            } else if(oldOppMap.isEmpty() && objOpp.delivery_status__c=='Approved' && objOpp.Active_Project__c == null) {

                // Called from after insert trigger
                Project__c proj = new Project__c();
                // Calling method to set the Project record values
                proj = setProjectValues(objOpp);
                ProjectList.add(proj);
            } else if(!oldOppMap.isEmpty() && objOpp.delivery_status__c=='Approved' && objOpp.Active_Project__c != null) {
                // T-00402 - 090819 - VennScience_BFL_Amruta - Add existing project into Project Id set
                setExistingProjectId.add(objOpp.Active_Project__c);
                mapOppIdVSForcastedManagerId.put(objOpp.Id,objOpp.Forecasted_Project_Lead__c);
            }// End of if-else block

            // Find quater of opportunity 
            if(!oldOppMap.isEmpty() && (objOpp.delivery_status__c  != oldOppMap.get(objOpp.id).delivery_status__c || 
               objOpp.closedate != oldOppMap.get(objOpp.id).closedate)) {
                if(objOpp.Active_Project__c != null && objOpp.delivery_status__c == 'Approved') {
                   mapOppIds.put(objOpp.id,objOpp.Active_Project__c);
                } // End of inner if
                String qutrStr = '';
                if(1 <= objOpp.closedate.month() && 3 >= objOpp.closedate.month())
                    qutrStr = 'Q1 '+string.valueof(objOpp.closedate.Year());
                if(4 <= objOpp.closedate.month() && 6 >= objOpp.closedate.month())
                    qutrStr = 'Q2 '+string.valueof(objOpp.closedate.Year());
                if(7 <= objOpp.closedate.month() && 9 >= objOpp.closedate.month())
                    qutrStr = 'Q3 '+string.valueof(objOpp.closedate.Year());
                if(10 <= objOpp.closedate.month() && 12 >= objOpp.closedate.month())
                    qutrStr = 'Q4 '+string.valueof(objOpp.closedate.Year());
                if(qutrStr != '' && objOpp.stagename != 'Closed Lost'){
                    if(!mapOfStrQuota.keyset().contains(qutrStr)) {
                        mapOfStrQuota.put(qutrStr,new list<opportunity>());
                    } // End of inner if
                    mapOfStrQuota.get(qutrStr).add(objOpp);
                }
            } // End of if
        } // End of for
        
        // T-00402 - 090819 - VennScience_BFL_Amruta - Fetch the existing Projects
        listExistingProjects = [SELECT Id,
                               		   Name,
                               		   Opportunity__c
                                  FROM Project__c
                                 WHERE Id IN :setExistingProjectId];
        System.debug('listExistingProjects========='+listExistingProjects);
		// T-00402 - 090819 - VennScience_BFL_Amruta - Iterate over existing Projects  
        for(Project__c projectRecord : listExistingProjects) {
        	mapOppIds.put(projectRecord.Opportunity__c,projectRecord.id);
            if(!mapOppIdVSForcastedManagerId.isEmpty() && mapOppIdVSForcastedManagerId.containsKey(projectRecord.Opportunity__c)) {
                projectRecord.ownerid = mapOppIdVSForcastedManagerId.get(projectRecord.Opportunity__c);
            	listProjectsToBeUpdated.add(projectRecord);
            } // End of if
    	} // End of for 
        
        // // T-00402 - 090819 - VennScience_BFL_Amruta - Update Project
        if(!listProjectsToBeUpdated.isEmpty()) {
            try {
                update listProjectsToBeUpdated;
            } catch(Exception e) {
                System.debug('Error occurred while updating opportunity records========='+e.getMessage());
            } // End of try-catch block
        } // End of if
        // Insert Projects
        if(!ProjectList.isEmpty() || !listExistingProjects.isEmpty()) {
            try {
                if(!ProjectList.isEmpty()) {
                	insert ProjectList;   
                } // End of if
                for(Project__c proj : ProjectList) {
                    mapOppIds.put(proj.Opportunity__c,proj.id);
                    opportunity opp = new opportunity(id=proj.Opportunity__c);
                    opp.Active_Project__c = proj.id;
                    Opportunitylist.add(opp);
                }

                if(!Opportunitylist.isEmpty()) {
                    try {
                        update Opportunitylist;
                    } catch(Exception e) {
                        System.debug('Exception occurred while opportunity update======='+e.getMessage());
                    }
                }

                /*call apex class to create contentdocumentlink*/
                createContentDocumentLink obj = new createContentDocumentLink();
                obj.createContentDocumentLink(mapOppIds); 

                /*create invoices for milestones*/
                obj.createInvoices(mapOppIds);

                /*create contact roles on project*/
                obj.createContactRoles(mapOppIds);

                /*create ongoing support deliverable*/
                obj.createOngoingSupport(mapOppIds);
            } catch(Exception e) {
                System.debug('Error occurred while inserting projects====='+e.getMessage());
            }
        } // End of ProjectList if
    }
    /**
    * Method Name : setProjectValues
    * Parameters  : Opportunity
    * Description : This method is used to set the values for Project records for each Opportunity where Delivery Status 
    *               has been set to Approved
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 100719
    **/
    public Project__c setProjectValues(Opportunity objOpportunity) {
        List<Project__c> listProjectToBeInserted = new List<Project__c>();
        Project__c objProject = new Project__c();
        objProject.Opportunity__c = objOpportunity.Id;
        objProject.Account__c = objOpportunity.Accountid;
        objProject.Name = objOpportunity.Name;
        objProject.Stage__c = 'Pre-Kickoff';
        objProject.Edition__c = objOpportunity.Edition__c;
        objProject.RecordTypeID = objOpportunity.Project_Record_Type__c;
        objProject.Service__c = objOpportunity.Service__c;
        objProject.Hourly_Rate__c = objOpportunity.Hourly_Rate__c;
        objProject.SOW_Date__c = objOpportunity.SOW_Date__c;
        objProject.State_Tax_Purposes__c = objOpportunity.Corporation_Location__c;
        objProject.Expiration_Date__c = objOpportunity.SOW_Expiration_Date__c;
        objProject.Number_of_Users__c = objOpportunity.Number_of_Users__c;
        objProject.Salesforce_AE__c = objOpportunity.Salesforce_AE__c; 
        objProject.Service__c = objOpportunity.Service__c;
        objProject.Payment_Term_Days__c = objOpportunity.Payment_Term_Days__c;
        objProject.Payment_Term_Period__c = objOpportunity.Payment_Term_Period__c;   
        objProject.Commissionable_Rep__c  = objOpportunity.ownerid; 
        objProject.Expected_Weeks__c = objOpportunity.Expected_of_Weeks__c;    
        objProject.ownerid = objOpportunity.Forecasted_Project_Lead__c;
        objProject.Project_Support__c = objOpportunity.Forecasted_Project_Support__c;
        objProject.Complexity_Level__c = objOpportunity.Complexity_Level__c;
        objProject.Kickoff_Type__c = objOpportunity.Kickoff_Type__c;
        return objProject;
    }
}