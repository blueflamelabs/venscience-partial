/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        071019         VennScience_BFL_Amruta                              This is the Controller class for ProjectTeamTrigger. Used to perform all 
*                                                                                   the business logic related to Project_Team__c records.
*                               
**********************************************************************************************************************************************************/
public class ProjectTeamTriggerController {
    
    // Variable Declarations
    public static final String PARTNERCOMMPROFILE = Label.Partner_Community; // Partner Community Profile Name
    
	/**
    * Method Name : shareRecordWithProjectTeamMember
    * Parameters  : param1: Set<Id>
    * Description : Used to share Project, related Work and Deliverable records with all users which belongs to the
    *               same role as that of ProjectTeam's TeamMember.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 071019
    **/
    public static void shareRecordWithProjectTeamMember(Set<Id> setProjectTeamId) {
        // Variable Declarations
        List<Project_Team__c> listProjectTeam = new List<Project_Team__c>();
        List<VDLC_Work__c> listWork = new List<VDLC_Work__c>();
        List<User> listSameRoleUsers = new List<User>();
        List<Project__c> listProject = new List<Project__c>();
        List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
        List<Project__Share> listProjectShare = new List<Project__Share>();
        List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
        Set<Id> setProjectId = new Set<Id>();
        Map<Id,Set<Id>> mapProjectIdVSTeamMemberRoleId = new Map<Id,Set<Id>>();
        Map<Id,set<Id>> mapRoleIdVSWorkIdSet = new Map<Id,set<Id>>();
        Map<Id,Set<Id>> mapRoleIdVSUserIdSetForWork = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapUserIdVSWorkIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapRoleIdVSProjectIdSet = new Map<Id,Set<Id>>();
        Map<Id,List<Deliverable__c>> mapRoleIdVSDeliverableList = new Map<Id,List<Deliverable__c>>();
        Map<Id,Set<Id>> mapRoleIdVSUserIdSetForProject = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapUserIdVSProjectIdSet = new Map<Id,Set<Id>>();
        Map<Id,List<Deliverable__c>> mapUserIdVSDelList = new Map<Id,List<Deliverable__c>>();
        Map<Id,List<Deliverable__c>> mapProIdVSListDeliverables = new Map<Id,List<Deliverable__c>>();
        
        // Fetch Project Team records
        listProjectTeam = [SELECT Id,
                                  Project__c,
                                  Team_Member__c,
                                  Team_Member__r.UserRoleId
                             FROM Project_Team__c
                            WHERE Id IN :setProjectTeamId
                              AND Team_Member__r.Profile.Name = :PARTNERCOMMPROFILE];
        // Iterate over Project Team records
        for(Project_Team__c projectTeamRec : listProjectTeam) {
            // Populate project id set
            setProjectId.add(projectTeamRec.Project__c);
            // Populate map of ProjectId VS TeamMemberRoleId
            if(!mapProjectIdVSTeamMemberRoleId.containsKey(projectTeamRec.Project__c)) {
                mapProjectIdVSTeamMemberRoleId.put(projectTeamRec.Project__c, new Set<Id>{projectTeamRec.Team_Member__r.UserRoleId});
            } else {
                Set<Id> setPreviousRoleId = new Set<Id>();
                setPreviousRoleId = mapProjectIdVSTeamMemberRoleId.get(projectTeamRec.Project__c);
                setPreviousRoleId.add(projectTeamRec.Team_Member__r.UserRoleId);
                mapProjectIdVSTeamMemberRoleId.put(projectTeamRec.Project__c, setPreviousRoleId);
            } // End of if-else block
            
            // Populate map of RoleId VS ProjectIdSet
            if(!mapRoleIdVSProjectIdSet.containsKey(projectTeamRec.Team_Member__r.UserRoleId)) {
                mapRoleIdVSProjectIdSet.put(projectTeamRec.Team_Member__r.UserRoleId, new Set<Id>{projectTeamRec.Project__c});
            } else {
                Set<Id> setPreviousWorkId = new Set<Id>();
                setPreviousWorkId = mapRoleIdVSProjectIdSet.get(projectTeamRec.Team_Member__r.UserRoleId);
                setPreviousWorkId.add(projectTeamRec.Project__c);
                mapRoleIdVSProjectIdSet.put(projectTeamRec.Team_Member__r.UserRoleId, setPreviousWorkId);
            } // End of if-else block
        } // End of for
        
        // Fetch Project records and related Deliverables
        listProject = [SELECT Id,
                             (SELECT Id FROM Estimates__r)
                        FROM Project__c
                       WHERE Id IN :mapProjectIdVSTeamMemberRoleId.keySet()];
        // Iterate over Project records
        for(Project__c proRecord : listProject) {
            if(proRecord.Estimates__r.size() > 0) {
                mapProIdVSListDeliverables.put(proRecord.Id, proRecord.Estimates__r);
            } // End of if
        } // End of for
        // Iterate over mapProjectIdVSTeamMemberRoleId keyset
        for(Id proId : mapProjectIdVSTeamMemberRoleId.keySet()) {
            if(mapProIdVSListDeliverables.containsKey(proId)) {
                for(Id roleId : mapProjectIdVSTeamMemberRoleId.get(proId)) {
                    mapRoleIdVSDeliverableList.put(roleId, mapProIdVSListDeliverables.get(proId));
                } // End of inner for
            } // End of if
        } // End of outer for
        Set<Id> setRoleId = new Set<Id>();
        // Iterate over mapProjectIdVSTeamMemberRoleId values
        for(Set<Id> roleIdSet : mapProjectIdVSTeamMemberRoleId.values()) {
            setRoleId.addAll(roleIdSet);
        } // End of outer for
        
        // Fetch Work records and related Tasks
        listWork = [SELECT Id,
                           Project__c,
                           (SELECT Id,
                                   Assigned_To__r.UserRoleId
                              FROM Tasks__r
                             WHERE Assigned_To__c != null
                               AND Assigned_To__r.UserroleId IN :setRoleId)
                      FROM VDLC_Work__c
                     WHERE Project__c IN :mapProjectIdVSTeamMemberRoleId.keySet()];
        // Iterate over work records
        for(VDLC_Work__c workRecord : listWork) {
            for(VDLC_Task__c taskRecord : workRecord.Tasks__r) {
                // Check if task.AssignedTo user has same role as that of Work.ProjectTeam.TeamMember
                if(mapProjectIdVSTeamMemberRoleId.get(workRecord.Project__c).contains(taskRecord.Assigned_To__r.UserroleId)) {
                    if(!mapRoleIdVSWorkIdSet.containsKey(taskRecord.Assigned_To__r.UserroleId)) {
                        mapRoleIdVSWorkIdSet.put(taskRecord.Assigned_To__r.UserroleId, new Set<Id>{workRecord.Id});
                    } else {
                        Set<Id> setPreviousWorkId = new Set<Id>();
                        setPreviousWorkId = mapRoleIdVSWorkIdSet.get(taskRecord.Assigned_To__r.UserroleId);
                        setPreviousWorkId.add(workRecord.Id);
                        mapRoleIdVSWorkIdSet.put(taskRecord.Assigned_To__r.UserroleId, setPreviousWorkId);
                    } // End of if-else block
                } // End of outer if               
            } // End of Task for
        } // End of VDLC_Work__c for
        
        // Fetch all Users that belongs to the same role as that of ProjectTeam.TeamMember
        listSameRoleUsers = [SELECT Id,
                                    UserRoleId
                               FROM User
                              WHERE (UserRoleId IN :mapRoleIdVSWorkIdSet.keySet()
                                 OR UserRoleId IN :mapRoleIdVSProjectIdSet.keySet())
                                AND IsActive = true];
        // Iterate over users list
        for(User userRecord : listSameRoleUsers) {
            if(mapRoleIdVSWorkIdSet.containsKey(userRecord.UserRoleId)) {
                if(!mapRoleIdVSUserIdSetForWork.containsKey(userRecord.UserRoleId)) {
                    mapRoleIdVSUserIdSetForWork.put(userRecord.UserRoleId, new Set<Id>{userRecord.Id});
                } else {
                    Set<Id> previousUserIdSet = new Set<Id>();
                    previousUserIdSet = mapRoleIdVSUserIdSetForWork.get(userRecord.UserRoleId);
                    previousUserIdSet.add(userRecord.Id);
                    mapRoleIdVSUserIdSetForWork.put(userRecord.UserRoleId, previousUserIdSet);
                } // End of if-else block
            } // End of outer mapRoleIdVSWorkIdSet if
            if(mapRoleIdVSProjectIdSet.containsKey(userRecord.UserRoleId)) {
                if(!mapRoleIdVSUserIdSetForProject.containsKey(userRecord.UserRoleId)) {
                    mapRoleIdVSUserIdSetForProject.put(userRecord.UserRoleId, new Set<Id>{userRecord.Id});
                } else {
                    Set<Id> previousUserSet = new Set<Id>();
                    previousUserSet = mapRoleIdVSUserIdSetForProject.get(userRecord.UserRoleId);
                    previousUserSet.add(userRecord.Id);
                    mapRoleIdVSUserIdSetForProject.put(userRecord.UserRoleId, previousUserSet);
                } // End of if-else mapRoleIdVSProjectIdSet block
            } // End of outer if
        } // End of for 
        
        // Iterate over mapRoleIdVSWorkIdSet
        for(Id roleId : mapRoleIdVSWorkIdSet.keySet()) {
            if(mapRoleIdVSUserIdSetForWork.containsKey(roleId)) {
                for(Id userId : mapRoleIdVSUserIdSetForWork.get(roleId)) {
                  mapUserIdVSWorkIdSet.put(userId, mapRoleIdVSWorkIdSet.get(roleId));
              } // End of inner for
            } // End of if
        } // End of outer for 
        
        // Iterate over mapRoleIdVSProjectIdSet
        for(Id roleId : mapRoleIdVSProjectIdSet.keySet()) {
            if(mapRoleIdVSUserIdSetForProject.containsKey(roleId)) {
                for(Id userId : mapRoleIdVSUserIdSetForProject.get(roleId)) {
                  mapUserIdVSProjectIdSet.put(userId, mapRoleIdVSProjectIdSet.get(roleId));
              } // End of inner for
            } // End of if
        } // End of outer for
        
        // Iterate over mapRoleIdVSProjectIdSet
        for(Id roleId : mapRoleIdVSDeliverableList.keySet()) {
            if(mapRoleIdVSUserIdSetForProject.containsKey(roleId)) {
                for(Id userId : mapRoleIdVSUserIdSetForProject.get(roleId)) {
                  mapUserIdVSDelList.put(userId, mapRoleIdVSDeliverableList.get(roleId));
              } // End of inner for
            } // End of if
        } // End of outer for
        
        // Share Project records with All Users that belongs to the same role as that of ProjectTeam.TeamMember
        for(Id userId : mapUserIdVSProjectIdSet.keySet()) {
            for(Id projectId : mapUserIdVSProjectIdSet.get(userId)) {
                Project__Share projectShareRecord = new Project__Share();
                projectShareRecord.ParentId = projectId;
                projectShareRecord.AccessLevel = 'Edit';
                projectShareRecord.UserOrGroupId = userId;
                projectShareRecord.RowCause = Schema.Project__Share.RowCause.Partner_Community__c;
                listProjectShare.add(projectShareRecord);
            } // End of inner for that is mapUserIdVSProjectIdSet value for
        } // End of outer for that is mapUserIdVSProjectIdSet keySet for
        
        // Share Work records with All Users that belongs to the same role as that of ProjectTeam.TeamMember
        for(Id userId : mapUserIdVSWorkIdSet.keySet()) {
            for(Id workId : mapUserIdVSWorkIdSet.get(userId)) {
                VDLC_Work__Share workShareRecord = new VDLC_Work__Share();
                workShareRecord.ParentId = workId;
                workShareRecord.AccessLevel = 'Edit';
                workShareRecord.UserOrGroupId = userId;
                workShareRecord.RowCause = Schema.VDLC_Work__Share.RowCause.Partner_Community__c;
                listWorkShare.add(workShareRecord);
            } // End of inner for that is mapUserIdVSWorkIdSet value for
        } // End of outer for that is mapUserIdVSWorkIdSet keySet for 
        
        // Share Deliverable records with All Users that belongs to the same role as that of ProjectTeam.TeamMember
        for(Id userId : mapUserIdVSDelList.keySet()) {
            for(Deliverable__c delRecord : mapUserIdVSDelList.get(userId)) {
                Deliverable__Share delShareRecord = new Deliverable__Share();
                delShareRecord.ParentId = delRecord.Id;
                delShareRecord.AccessLevel = 'Edit';
                delShareRecord.UserOrGroupId = userId;
                delShareRecord.RowCause = Schema.Deliverable__Share.RowCause.Partner_Community__c;
                listDelShare.add(delShareRecord);
            } // End of inner for that is mapUserIdVSWorkIdSet value for
        } // End of outer for that is mapUserIdVSWorkIdSet keySet for
        
        // @Reminder: Remove debug logs
        //System.debug('listProjectShare========='+listProjectShare);
        //System.debug('listWorkShare========='+listWorkShare);
        //System.debug('listDelShare========='+listDelShare);
        // Insert Project Share records
        if(!listProjectShare.isEmpty()) {
            insert listProjectShare;
            // @Reminder: Remove debug log
            //System.debug('Project share inserted');
        } // End of if
        
        // Insert Work Share records
        if(!listWorkShare.isEmpty()) {
            insert listWorkShare;
            // @Reminder: Remove debug log
            //System.debug('Work share inserted');
        } // End of if
        
        // Insert Deliverable Share records
        if(!listDelShare.isEmpty()) {
            insert listDelShare;
            // @Reminder: Remove debug log
            //System.debug('Deliverable share inserted');
        } // End of if
    }
    /**
    * Method Name : removeShareRecordWithProjectTeamMember
    * Parameters  : param1: Map<Id,Set<Id>>
    * Description : Used to remove the share records of Project, related Work and Deliverable records ProjectTeam's deleted 
    *               TeamMember only if no other project team member belongs to the same role.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 071019
    **/
    public static void removeShareRecordWithProjectTeamMember(Map<Id,Set<Id>> mapUserIdVSProjectIdSet) {
        
        // Variable Declarations
        String projectRowCause = Schema.Project__Share.RowCause.Partner_Community__c;
        String workRowCause = Schema.VDLC_Work__Share.RowCause.Partner_Community__c;
        String delRowCause = Schema.Deliverable__Share.RowCause.Partner_Community__c;
        Boolean isUserWithSameRoleExists = false;
        List<Project__Share> listProjectShare = new List<Project__Share>();
        List<Project__Share> listProjectShareToBeDeleted = new List<Project__Share>();
        List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
        List<Deliverable__Share> listDelShare = new List<Deliverable__Share>();
        Set<Id> setProjectIdToDelete = new Set<Id>();
        Set<Id> setFinalProjectIdToDelete = new Set<Id>();
        Set<Id> setWorkIdToDelete = new Set<Id>();
        Set<Id> setDeliverableIdToDelete = new Set<Id>();                                                         
        Map<Id,Id> mapUserIdVSRoleId = new Map<Id,Id>();
        Map<Id,Id> mapProjectIdVSRoleId = new Map<Id,Id>();
        Map<Id,Set<Id>> mapRoleIdVSProjectIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapAllUserIdVSProjectIdSet = new Map<Id,Set<Id>>();
                                                                  
        // Iterate over lost of community users
        for(User commUserRecord : [SELECT Id,
                                         UserRoleId
                                    FROM User
                                   WHERE Id IN :mapUserIdVSProjectIdSet.keySet()
                                     AND Profile.Name = :PARTNERCOMMPROFILE
                                     AND IsActive = true]
        ) {
            // Populate map of UserId VS RoleId
            mapUserIdVSRoleId.put(commUserRecord.Id, commUserRecord.UserRoleId);                                                         
        } // End of for
        
        // Iterate over map of UserId VS RoleId
        for(Id userId : mapUserIdVSRoleId.keySet()) {
            if(mapUserIdVSProjectIdSet.containsKey(userId)) {
                for(Id proId : mapUserIdVSProjectIdSet.get(userId)) {
                    // Populate map of ProjectId VS RoleId
                    mapProjectIdVSRoleId.put(proId, mapUserIdVSRoleId.get(userId));
                } // End of inner for
                // Populate map of RoleId VS Set of ProjectId
                mapRoleIdVSProjectIdSet.put(mapUserIdVSRoleId.get(userId), mapUserIdVSProjectIdSet.get(userId));
            } // End of if 
        } // End of for
        
        // Fetch all users that belongs to the same role as that of updated/removed team member
        for(User userRecord : [SELECT Id,
                                      UserRoleId
                                 FROM User
                                WHERE UserRoleId IN :mapRoleIdVSProjectIdSet.keySet()
                                  AND IsActive = true]) {
            if(mapRoleIdVSProjectIdSet.containsKey(userRecord.UserRoleId)) {
                // Populate map of userId VS ProjectIdSet
                mapAllUserIdVSProjectIdSet.put(userRecord.Id, mapRoleIdVSProjectIdSet.get(userRecord.UserRoleId));                          
            } // End of if   
        } // End of for
        // Iterate over list of Projects
        for(Project__c projectRecord : [SELECT Id,
                                               (SELECT Id,
                                                       Team_Member__c,
                                                       Team_Member__r.UserRoleId
                                                  FROM Project_Team__r
                                                 WHERE Team_Member__c != null)
                                          FROM Project__c
                                         WHERE Id IN :mapProjectIdVSRoleId.keySet()]
        ) {
            isUserWithSameRoleExists = false;
            for(Project_Team__c proTeamRecord : projectRecord.Project_Team__r) {
                // Check if Project's existing Team Member has same role as that of the removed team member's role
                if(proTeamRecord.Team_Member__r.UserRoleId == mapProjectIdVSRoleId.get(projectRecord.Id)) {
                    isUserWithSameRoleExists = true;    
                } // End of if
            } // End of Project Team for
            if(!isUserWithSameRoleExists) {
                // Populate set of Project Id whose share records needs to be deleted
                setProjectIdToDelete.add(projectRecord.Id);
            } // End of if
        } // End of Project for                                                        
        // Fetch Project Share rows
        // @Reminder: Remove debug log
        // System.debug('projectRowCause======='+projectRowCause);
        listProjectShare = [SELECT Id,
                                   UserOrGroupId,
                                   ParentId
                              FROM Project__Share
                             WHERE ParentId IN :setProjectIdToDelete
                               AND UserOrGroupId IN :mapAllUserIdVSProjectIdSet.keySet()
                               AND RowCause = :projectRowCause];
        // @Reminder: Remove debug log
        // System.debug('listProjectShare=========='+listProjectShare);
        // Iterate over Project Share records
        for(Project__Share proShareRecord : listProjectShare) {
            if(mapAllUserIdVSProjectIdSet.containsKey(proShareRecord.UserOrGroupId) &&
               mapAllUserIdVSProjectIdSet.get(proShareRecord.UserOrGroupId).contains(proShareRecord.ParentId)) {
            	setFinalProjectIdToDelete.add(proShareRecord.ParentId);
                listProjectShareToBeDeleted.add(proShareRecord);
            } // End of if
        } // End of for
        // @Reminder: Remove debug log
        //System.debug('listProjectShareToBeDeleted=========='+listProjectShareToBeDeleted);
        
        // Fetch project related Work records
        for(VDLC_Work__c workRecord : [SELECT Id
                                         FROM VDLC_Work__c
                                        WHERE Project__c IN :setFinalProjectIdToDelete]) {
        	// Populate set of Work Id whose share records needs to be deleted
        	setWorkIdToDelete.add(workRecord.Id);                                                              
        } // End of for                                                         
        // Fetch Work Share rows
        // @Reminder: Remove debug log
        //System.debug('workRowCause======='+workRowCause);
        listWorkShare = [SELECT Id
                           FROM VDLC_Work__Share
                          WHERE ParentId IN :setWorkIdToDelete
                            AND UserOrGroupId IN :mapAllUserIdVSProjectIdSet.keySet()
                            AND RowCause = :projectRowCause];
        // @Reminder: Remove debug log
        //System.debug('listWorkShare=========='+listWorkShare);
        
        // Fetch project related Deliverable records
        for(Deliverable__c delRecord : [SELECT Id
                                         FROM Deliverable__c
                                        WHERE Project__c IN :setFinalProjectIdToDelete]) {
        	// Populate set of Deliverable Id whose share records needs to be deleted
        	setDeliverableIdToDelete.add(delRecord.Id);                                                              
        } // End of for
        // Fetch Work Share rows
        // @Reminder: Remove debug log
        //System.debug('delRowCause======='+delRowCause);
        listDelShare = [SELECT Id
                          FROM Deliverable__Share
                         WHERE ParentId IN :setDeliverableIdToDelete
                           AND UserOrGroupId IN :mapAllUserIdVSProjectIdSet.keySet()
                           AND RowCause = :delRowCause];
        // @Reminder: Remove debug log
        //System.debug('listDelShare=========='+listDelShare);
                                                                  
        // Delete Project Share rows
        if(!listProjectShare.isEmpty()) {
        	delete listProjectShareToBeDeleted;
            // @Reminder: Remove debug log
            //System.debug('Project share rows has been deleted');
        } // End of if
        // Delete Work Share rows
        if(!listWorkShare.isEmpty()) {
        	delete listWorkShare;
            // @Reminder: Remove debug log
            //System.debug('Work share rows has been deleted');
        } // End of if
        // Delete Deliverable Share rows
        if(!listDelShare.isEmpty()) {
        	delete listDelShare;
            // @Reminder: Remove debug log
            //System.debug('Deliverable share rows has been deleted');
        } // End of if
    }
}