/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        260819         VennScience_BFL_Amruta     This is the test class for TeamMemberTriggerHandler apex class.
**********************************************************************************************************************************************************/
@isTest
public class TeamMemberTriggerHandlerTest {
    /**
    * Method Name : addToChatterGroupPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of addChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void addToChatterGroupPositiveTest() {
        // Insert Team member record
        agf__ADM_Scrum_Team__c getTeamRecord = new agf__ADM_Scrum_Team__c();
        getTeamRecord = TestDataFactory.createTeam();
        insert getTeamRecord;
        // Get Collaboration record
        CollaborationGroup colGroupRecord = new CollaborationGroup();
        colGroupRecord = TestDataFactory.createCollaborationGroup();
        insert colGroupRecord;
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        //getProjectRecord.Project_Chatter_Group__c = colGroupRecord.Id;
        insert getProjectRecord;
        // Fetch inserted Project record
        Project__c projectRecord = new Project__c();
        projectRecord = [SELECT Id,
                                Project_Chatter_Group__c
                           FROM Project__c
                          WHERE  Id = :getProjectRecord.Id];
        // Insert community user and get contact id
         Id getContactId = TestDataFactory.createCommunityUser();
        // Fetch community user
        List<User> listCommunityUser = new List<User>();
        listCommunityUser = [SELECT Id
                               FROM User
                              WHERE ContactId = :getContactId
                              LIMIT 1];
        //System.debug('listCommunityUser======'+listCommunityUser[0].Id);
        // Insert Team Member record
        agf__ADM_Scrum_Team_Member__c getTeamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        getTeamMemberRecord = TestDataFactory.createTeamMembers();
        getTeamMemberRecord.Project__c = getProjectRecord.Id;
        getTeamMemberRecord.agf__Scrum_Team__c = getTeamRecord.Id;
        getTeamMemberRecord.Add_to_Chatter_Group__c = true;
        getTeamMemberRecord.agf__Member_Name__c = listCommunityUser[0].Id;
        Test.startTest();
        insert getTeamMemberRecord;
        Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :projectRecord.Project_Chatter_Group__c];
        //System.debug('listGroupMembers========'+listGroupMembers);
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
    } 
    /**
    * Method Name : addToChatterGroupNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of addChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void addToChatterGroupNegativeTest() {
        // Insert Team member record
        agf__ADM_Scrum_Team__c getTeamRecord = new agf__ADM_Scrum_Team__c();
        getTeamRecord = TestDataFactory.createTeam();
        insert getTeamRecord;
        // Get Collaboration record
        CollaborationGroup colGroupRecord = new CollaborationGroup();
        colGroupRecord = TestDataFactory.createCollaborationGroup();
        insert colGroupRecord;
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Chatter_Group__c = colGroupRecord.Id;
        insert getProjectRecord;
        // Insert community user and get contact id
         Id getContactId = TestDataFactory.createCommunityUser();
        // Fetch community user
        List<User> listCommunityUser = new List<User>();
        listCommunityUser = [SELECT Id
                               FROM User
                              WHERE ContactId = :getContactId
                              LIMIT 1];
        // Insert Team Member record
        agf__ADM_Scrum_Team_Member__c getTeamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        getTeamMemberRecord = TestDataFactory.createTeamMembers();
        getTeamMemberRecord.Project__c = getProjectRecord.Id;
        getTeamMemberRecord.agf__Scrum_Team__c = getTeamRecord.Id;
        getTeamMemberRecord.Add_to_Chatter_Group__c = false;
        getTeamMemberRecord.agf__Member_Name__c = listCommunityUser[0].Id;
        Test.startTest();
        insert getTeamMemberRecord;
        Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :colGroupRecord.Id];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() == 0,'Community Member has not added to the Project Chatter Group'); 
    }
    /**
    * Method Name : removeFromChatterGroupPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of removeFromChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void removeFromChatterGroupPositiveTest() {
        // Insert Team member record
        agf__ADM_Scrum_Team__c getTeamRecord = new agf__ADM_Scrum_Team__c();
        getTeamRecord = TestDataFactory.createTeam();
        insert getTeamRecord;
        // Get Collaboration record
        CollaborationGroup colGroupRecord = new CollaborationGroup();
        colGroupRecord = TestDataFactory.createCollaborationGroup();
        insert colGroupRecord;
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Chatter_Group__c = colGroupRecord.Id;
        insert getProjectRecord;
        // Fetch inserted Project record
        Project__c projectRecord = new Project__c();
        projectRecord = [SELECT Id,
                                Project_Chatter_Group__c
                           FROM Project__c
                          WHERE  Id = :getProjectRecord.Id];
        // Insert community user and get contact id
         Id getContactId = TestDataFactory.createCommunityUser();
        // Fetch community user
        List<User> listCommunityUser = new List<User>();
        listCommunityUser = [SELECT Id
                               FROM User
                              WHERE ContactId = :getContactId
                              LIMIT 1];
        // Insert Team Member record
        agf__ADM_Scrum_Team_Member__c getTeamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        getTeamMemberRecord = TestDataFactory.createTeamMembers();
        getTeamMemberRecord.Project__c = getProjectRecord.Id;
        getTeamMemberRecord.agf__Scrum_Team__c = getTeamRecord.Id;
        getTeamMemberRecord.Add_to_Chatter_Group__c = true;
        getTeamMemberRecord.agf__Member_Name__c = listCommunityUser[0].Id;
        Test.startTest();
        insert getTeamMemberRecord;
        Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :projectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
        // Update team member
        getTeamMemberRecord.agf__Member_Name__c = null;
        //Test.startTest();
        update getTeamMemberRecord;
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listUpdatedGroupMembers = new List<CollaborationGroupMember>();
        listUpdatedGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :colGroupRecord.Id];
        Set<Id> setUpdatedMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listUpdatedGroupMembers) {
            setUpdatedMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listUpdatedMemberUsers = new List<User>();
        listUpdatedMemberUsers = [SELECT Id
                                    FROM User
                                   WHERE ContactId != null
                                     AND Id IN :setUpdatedMemberId];
        System.assert(listUpdatedMemberUsers.size() == 0,'Community Member has not been removed from the Project Chatter Group');
    }
    /**
    * Method Name : removeFromChatterGroupNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of removeFromChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void removeFromChatterGroupNegativeTest() {
        // Insert Team member record
        agf__ADM_Scrum_Team__c getTeamRecord = new agf__ADM_Scrum_Team__c();
        getTeamRecord = TestDataFactory.createTeam();
        insert getTeamRecord;
        // Get Collaboration record
        CollaborationGroup colGroupRecord = new CollaborationGroup();
        colGroupRecord = TestDataFactory.createCollaborationGroup();
        insert colGroupRecord;
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Chatter_Group__c = colGroupRecord.Id;
        insert getProjectRecord;
        // Insert community user and get contact id
         Id getContactId = TestDataFactory.createCommunityUser();
        // Fetch community user
        List<User> listCommunityUser = new List<User>();
        listCommunityUser = [SELECT Id
                               FROM User
                              WHERE ContactId = :getContactId
                              LIMIT 1];
        // Insert Team Member record
        agf__ADM_Scrum_Team_Member__c getTeamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        getTeamMemberRecord = TestDataFactory.createTeamMembers();
        getTeamMemberRecord.Project__c = getProjectRecord.Id;
        getTeamMemberRecord.agf__Scrum_Team__c = getTeamRecord.Id;
        getTeamMemberRecord.Add_to_Chatter_Group__c = false;
        getTeamMemberRecord.agf__Member_Name__c = listCommunityUser[0].Id;
        Test.startTest();
        insert getTeamMemberRecord;
        Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :colGroupRecord.Id];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() == 0,'Community Member has been added to the Project Chatter Group');
        // Update team member
        getTeamMemberRecord.agf__Member_Name__c = null;
        //Test.startTest();
        update getTeamMemberRecord;
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listUpdatedGroupMembers = new List<CollaborationGroupMember>();
        listUpdatedGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :colGroupRecord.Id];
        Set<Id> setUpdatedMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listUpdatedGroupMembers) {
            setUpdatedMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listUpdatedMemberUsers = new List<User>();
        listUpdatedMemberUsers = [SELECT Id
                                    FROM User
                                   WHERE ContactId != null
                                     AND Id IN :setUpdatedMemberId];
        System.assert(listUpdatedMemberUsers.size() == 0,'Community Member has not been removed from the Project Chatter Group');
    }
    /**
    * Method Name : bulkTeamMemberTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of removeFromChatterGroup method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 260819
    **/
    
    @isTest
    public static void bulkTeamMemberTest() {
        // Insert Team member record
        agf__ADM_Scrum_Team__c getTeamRecord = new agf__ADM_Scrum_Team__c();
        getTeamRecord = TestDataFactory.createTeam();
        insert getTeamRecord;
        // Get Collaboration record
        CollaborationGroup colGroupRecord = new CollaborationGroup();
        colGroupRecord = TestDataFactory.createCollaborationGroup();
        insert colGroupRecord;
        // Insert Project record
        Project__c getProjectRecord = new Project__c();
        getProjectRecord = TestDataFactory.createProject();
        getProjectRecord.Project_Chatter_Group__c = colGroupRecord.Id;
        insert getProjectRecord;
        // Fetch inserted Project record
        Project__c projectRecord = new Project__c();
        projectRecord = [SELECT Id,
                                Project_Chatter_Group__c
                           FROM Project__c
                          WHERE  Id = :getProjectRecord.Id];
        // Insert community user and get contact id
         Id getContactId = TestDataFactory.createCommunityUser();
        // Fetch community user
        List<User> listCommunityUser = new List<User>();
        listCommunityUser = [SELECT Id
                               FROM User
                              WHERE ContactId = :getContactId
                              LIMIT 1];
        // Insert Team Member record
        agf__ADM_Scrum_Team_Member__c getTeamMemberRecord = new agf__ADM_Scrum_Team_Member__c();
        getTeamMemberRecord = TestDataFactory.createTeamMembers();
        getTeamMemberRecord.Project__c = getProjectRecord.Id;
        getTeamMemberRecord.agf__Scrum_Team__c = getTeamRecord.Id;
        getTeamMemberRecord.Add_to_Chatter_Group__c = true;
        getTeamMemberRecord.agf__Member_Name__c = listCommunityUser[0].Id;
        Test.startTest();
        insert getTeamMemberRecord;
        Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listGroupMembers = new List<CollaborationGroupMember>();
        listGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :projectRecord.Project_Chatter_Group__c];
        Set<Id> setMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listGroupMembers) {
            setMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listMemberUsers = new List<User>();
        listMemberUsers = [SELECT Id
                             FROM User
                            WHERE ContactId != null
                              AND Id IN :setMemberId];
        System.assert(listMemberUsers.size() > 0,'Community Member has not been added to the Project Chatter Group');
        // Update team member
        getTeamMemberRecord.agf__Member_Name__c = null;
        //Test.startTest();
        update getTeamMemberRecord;
        //Test.stopTest();
        // Fetch inserted group members for the Project Chatter group
        List<CollaborationGroupMember> listUpdatedGroupMembers = new List<CollaborationGroupMember>();
        listUpdatedGroupMembers = [SELECT Id,
                                   MemberId
                              FROM CollaborationGroupMember
                             WHERE CollaborationGroupId = :colGroupRecord.Id];
        Set<Id> setUpdatedMemberId = new Set<Id>();
        // Check if Project Contact Role associated user has been added to Chatter Group
        for(CollaborationGroupMember grpMemberRecord : listUpdatedGroupMembers) {
            setUpdatedMemberId.add(grpMemberRecord.MemberId);
        }
        List<User> listUpdatedMemberUsers = new List<User>();
        listUpdatedMemberUsers = [SELECT Id
                                    FROM User
                                   WHERE ContactId != null
                                     AND Id IN :setUpdatedMemberId];
        System.assert(listUpdatedMemberUsers.size() == 0,'Community Member has not been removed from the Project Chatter Group');
    }
    
}