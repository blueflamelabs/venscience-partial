global class createFifteenFiveReports implements Schedulable{

  global void execute(SchedulableContext ctx) {
      
      // 031019 - VennScience_BFL_Amruta - Variable Declarations
      List<PermissionSetAssignment> listExistingPermissionAssignment = new List<PermissionSetAssignment>();
      Set<Id> setUserId = new Set<Id>();
      Set<Id> setExisitingAssignedUserId = new Set<Id>();
      // Upto here
      
      Set<Id> usersId = new Set<Id>();
      List<User> users = [SELECT Id, Name, Profile.Name FROM User WHERE IsActive = TRUE AND create_Performance_Reviews__c = TRUE ];
      //090719 T - 00400 added debug  VennScience_BFL_Monali
      //System.debug('users=='+users);
    
      // 031019 - VennScience_BFL_Amruta - Iterate over Users list
      for(User userRecord : users) {
          setUserId.add(userRecord.Id);
      } // End of for
      
      
	  List<PermissionSetAssignment> lstPermissionSetAssignment = new List<PermissionSetAssignment>();
      
      PermissionSet  permissionSetId = [Select Id From PermissionSet Where Name = 'Performance_Reviews_Users' LIMIT 1]; 
      System.debug('permissionSetId=='+permissionSetId);
      
      // 031019 - VennScience_BFL_Amruta - Fetch PermissionSetAssignment for Performance_Reviews_Users Permission Set
      listExistingPermissionAssignment = [SELECT Id,
                                                 AssigneeId
                                            FROM PermissionSetAssignment
                                           WHERE AssigneeId IN :setUserId
                                             AND PermissionSetId = :permissionSetId.Id];
      // 031019 - VennScience_BFL_Amruta - Iterate over existing permission set list
      for(PermissionSetAssignment permissionSetAssRecord : listExistingPermissionAssignment) {
          setExisitingAssignedUserId.add(permissionSetAssRecord.AssigneeId);
      } // End of for
      for(User u : users) {
          PermissionSetAssignment psa = new PermissionSetAssignment();
          // 031019 - VennScience_BFL_Amruta - Check if User assignment already exists for Performance_Reviews_Users permission set
          if(!setExisitingAssignedUserId.contains(u.Id)) {
              psa.AssigneeId = u.Id;
              psa.PermissionSetId = permissionSetId.Id;
              lstPermissionSetAssignment.add(psa);
              usersId.add(u.Id);
          }
          // 031019 - VennScience_BFL_Amruta - Commented below code
          /*
          PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = permissionSetId.Id);
		  */
        } // End of for
        
        if(!lstPermissionSetAssignment.isEmpty()) {
        	insert lstPermissionSetAssignment;
        }
        if(!usersId.isEmpty()) {
        	CreatePerformanceReview(usersId);
        }
  }
 
  @future 
  static void CreatePerformanceReview (Set<Id> usersId) {
      X15_5_Report__c[] fifteenFiveReports = new List<X15_5_Report__c>();

    for(User u : [Select Id, Name FROM User Where Id IN : usersId]) { 
        X15_5_Report__c exp = new X15_5_Report__c(
                                  Name ='Temp Name', 
                              Date__c  = System.today(),
                              Status__c = 'Submitted to Employee',
                              Employee__c = u.Id);
        fifteenFiveReports.add(exp);
      }
      // 031019 - VennScience_BFL_Amruta - Added isRunningTest check to avoid Mixed DML Exception in test class
      if(!Test.isRunningTest()) {
          insert fifteenFiveReports;
      }
      //System.debug('fifteenFiveReports=='+fifteenFiveReports);
	}
}