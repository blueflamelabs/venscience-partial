/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy   Description
*     1.0        041019         VennScience_BFL_Amruta                              This is the Controller class for TaskTrigger. Used to perform all 
*                                                                                   the business logic for VDLC_Task__c.
*                               
**********************************************************************************************************************************************************/
public class TaskTriggerController {
	/**
    * Method Name : shareTaskRelatedWorkWithUsers
    * Parameters  : param1: Set<Id>
    * Description : Used to share the parent Work record with all the Users that belongs to the same role as that 
    *               of Assigned To User.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 041019
    **/
    public static void shareTaskRelatedWorkWithUsers(Set<Id> setTaskId) {
        // Variable Declarations
        List<VDLC_Task__c> listTask = new List<VDLC_Task__c>();
        List<User> listUsersWithSameRole = new List<User>();
        List<VDLC_Work__Share> listWorkShare = new List<VDLC_Work__Share>();
        Map<Id,Set<Id>> mapRoleIdVSWorkIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapRoleIdVSUserIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapUserIdVSWorkIdSet = new Map<Id,Set<Id>>();
        
        // Fetch Task records
        listTask = [SELECT Id,
                           Work__c,
                           Assigned_To__r.UserRoleId
                      FROM VDLC_Task__c
                     WHERE Id IN :setTaskId];
        // Iterate over list of Tasks
        for(VDLC_Task__c taskRecord : listTask) {
            if(!mapRoleIdVSWorkIdSet.containsKey(taskRecord.Assigned_To__r.UserRoleId)) {
                mapRoleIdVSWorkIdSet.put(taskRecord.Assigned_To__r.UserRoleId, new Set<Id>{taskRecord.Work__c});
                System.debug('mapRoleIdVSWorkIdSet ->'+mapRoleIdVSWorkIdSet);
            } else {
                Set<Id> setPreviousWorkId = new Set<Id>();
                setPreviousWorkId = mapRoleIdVSWorkIdSet.get(taskRecord.Assigned_To__r.UserRoleId);
                setPreviousWorkId.add(taskRecord.Work__c);
                mapRoleIdVSWorkIdSet.put(taskRecord.Assigned_To__r.UserRoleId, setPreviousWorkId);
            } // End of if-else block
        }
        // Fetch all the Users that belongs to the same role as that of Task's Assigned To User
        listUsersWithSameRole = [SELECT Id,
                                        UserRoleId
                                   FROM User
                                  WHERE UserRoleId IN :mapRoleIdVSWorkIdSet.keySet()
                                    AND IsActive = true];
        
        // Iterate over list of Users with same role
        for(User userRecord : listUsersWithSameRole) {
            // Populate map of RoleId VS Set of User Id
            if(!mapRoleIdVSUserIdSet.containsKey(userRecord.UserRoleId)) {
                mapRoleIdVSUserIdSet.put(userRecord.UserRoleId, new Set<Id>{userRecord.Id});
            } else {
                Set<Id> setPreviousUserId = new Set<Id>();
                setPreviousUserId = mapRoleIdVSUserIdSet.get(userRecord.UserRoleId);
                setPreviousUserId.add(userRecord.Id);
                mapRoleIdVSUserIdSet.put(userRecord.UserRoleId, setPreviousUserId);
            } // End of if-else block
        } // End of for
        // Iterate over mapRoleIdVSWorkIdSet
        for(Id roleId : mapRoleIdVSWorkIdSet.keySet()) {
            if(mapRoleIdVSUserIdSet.containsKey(roleId)) {
                for(Id userId : mapRoleIdVSUserIdSet.get(roleId)) {
                	mapUserIdVSWorkIdSet.put(userId, mapRoleIdVSWorkIdSet.get(roleId));
            	} // End of inner for
            } // End of if
        } // End of outer for
        
        // Share Work records with All Users that belongs to the same role as that of Assigned To User
        for(Id userId : mapUserIdVSWorkIdSet.keySet()) {
            for(Id workId : mapUserIdVSWorkIdSet.get(userId)) {
                VDLC_Work__Share workShareRecord = new VDLC_Work__Share();
                workShareRecord.ParentId = workId;
                workShareRecord.AccessLevel = 'Read';
                system.debug('11 Main Class workId -> '+workId);
                system.debug('11 Main Class user -> '+userId);
                workShareRecord.UserOrGroupId = userId;
                workShareRecord.RowCause = Schema.agf__ADM_Work__Share.RowCause.Partner_Community__c;
                listWorkShare.add(workShareRecord);
            } // End of inner for that is mapUserIdVSWorkIdSet value for
        } // End of outer for that is mapUserIdVSWorkIdSet keySet for 
        System.debug('Work share Before inserted=='+listWorkShare);
        // Insert Work Share records
        if(!listWorkShare.isEmpty()) {
            insert listWorkShare;
            // @Reminder: Remove debug log
             System.debug('Work share inserted=='+listWorkShare);
        } // End of if
    }
}