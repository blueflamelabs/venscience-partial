@isTest
private class createProject_Test {
    
    @isTest static void test_method_one() {
        Account acct = new Account(Name='Test Account',
                                        RecordTypeId='012i0000000FFqd',
                                        Type='Prospect');
        insert acct;
        Contact cont = new Contact(FirstName='Test',
                                        LastName='Test',
                                        RecordTypeId='012i0000000Dqvf',
                                        AccountId=acct.Id);
        insert cont;
        Opportunity opp = new Opportunity(Name=acct.Name + ' Opportunity',
                                        StageName='Identified',
                                        CloseDate=Date.valueOf('2017-10-01 00:00:00'),
                                        AccountId=acct.Id,
                                        Service_Type__c='Implementation',
                                        Service__c='Fixed Price Plus');
        insert opp;
        Milestone__c mstone = new Milestone__c(Type__c='Kickoff',
                                        Payment_Perc__c=10,
                                        Opportunity__c=opp.Id);
        insert mstone;
        Deliverable__c delv = new Deliverable__c(Name='D1',
                                        Type__c='Fixed Price',
                                        Opportunity__c=opp.Id,
                                        Deliverable_Overview__c='test',
                                        Status__c='Scoped');
        insert delv;
        OpportunityContactRole contRole = new OpportunityContactRole(ContactId=cont.Id,
                                        OpportunityId=opp.Id,
                                        Role='Decision Maker',
                                        IsPrimary=TRUE);
        insert contRole;
        ContentVersion doc = new ContentVersion(Title = 'Penguins',
                                        PathOnClient = 'Penguins.jpg',
                                        VersionData = Blob.valueOf('Test Content'),
                                        IsMajorVersion = true);
        insert doc;
        ContentVersion doc2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :doc.Id LIMIT 1];
        ContentDocumentLink cdLink = new ContentDocumentLink(ContentDocumentId=doc2.ContentDocumentId,
                                        LinkedEntityId=opp.Id,
                                        ShareType = 'V',
                                        Visibility = 'AllUsers');
        insert cdLink;


        // Perform test
        Test.startTest();
        Opportunity oppty = new Opportunity(id=opp.Id);
        oppty.StageName='Closed Won';
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Expected of Weeks to avoid validation
        oppty.Expected_of_Weeks__c = 1;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Hourly Rate for Closed Won opportunity to avoid validation
        oppty.Hourly_Rate__c = 10000;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Kickoff Type for Closed Won opportunity to avoid validation
        oppty.Kickoff_Type__c = 'No Kickoff';
        oppty.Delivery_Status__c='Approved';
        oppty.SOW_Date__c=System.today();
        oppty.Forecasted_Project_Lead__c ='005i0000000Nmyb';
        oppty.OwnerId ='005i0000000Nmyb';
        // System.debug(oppty);
        update oppty;
        /*Project__c testProject = new Project__c([select Id, Name, Opportunity__c from Project__c where Opportunity__c = :oppty.Id]);*/
            
            
        Test.stopTest();

        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        
        /*System.assertEquals(opp.Id, testProject.Opportunity__c);*/

    }
    
    @isTest static void test_method_two() {
Account acct = new Account(Name='Test Account',
                                        RecordTypeId='012i0000000FFqd',
                                        Type='Prospect');
        insert acct;
        Contact cont = new Contact(FirstName='Test',
                                        LastName='Test',
                                        RecordTypeId='012i0000000Dqvf',
                                        AccountId=acct.Id);
        insert cont;
        Opportunity opp = new Opportunity(Name=acct.Name + ' Opportunity',
                                        StageName='Identified',
                                        CloseDate=Date.valueOf('2017-01-01 00:00:00'),
                                        AccountId=acct.Id,
                                        Service_Type__c='Implementation',
                                        Service__c='Fixed Price Plus');
        insert opp;
        Milestone__c mstone = new Milestone__c(Type__c='Kickoff',
                                        Payment_Perc__c=10,
                                        Opportunity__c=opp.Id);
        insert mstone;
        Deliverable__c delv = new Deliverable__c(Name='D1',
                                        Type__c='Fixed Price',
                                        Opportunity__c=opp.Id,
                                        Deliverable_Overview__c='test',
                                        Status__c='Scoped');
        insert delv;
        OpportunityContactRole contRole = new OpportunityContactRole(ContactId=cont.Id,
                                        OpportunityId=opp.Id,
                                        Role='Decision Maker',
                                        IsPrimary=TRUE);
        insert contRole;
        ContentVersion doc = new ContentVersion(Title = 'Penguins',
                                        PathOnClient = 'Penguins.jpg',
                                        VersionData = Blob.valueOf('Test Content'),
                                        IsMajorVersion = true);
        insert doc;
        ContentVersion doc2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :doc.Id LIMIT 1];
        ContentDocumentLink cdLink = new ContentDocumentLink(ContentDocumentId=doc2.ContentDocumentId,
                                        LinkedEntityId=opp.Id,
                                        ShareType = 'V',
                                        Visibility = 'AllUsers');
        insert cdLink;


        // Perform test
        Test.startTest();
        Opportunity oppty = new Opportunity(id=opp.Id);
        oppty.StageName='Closed Won';
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Expected of Weeks to avoid validation
        oppty.Expected_of_Weeks__c = 1;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Hourly Rate for Closed Won opportunity to avoid validation
        oppty.Hourly_Rate__c = 10000;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Kickoff Type for Closed Won opportunity to avoid validation
        oppty.Kickoff_Type__c = 'No Kickoff';
        oppty.Delivery_Status__c='Approved';
        oppty.SOW_Date__c=System.today();
        oppty.Forecasted_Project_Lead__c ='005i0000000Nmyb';
        oppty.OwnerId ='005i0000000Nmyb';
        // System.debug(oppty);
        update oppty;
        /*Project__c testProject = new Project__c([select Id, Name, Opportunity__c from Project__c where Opportunity__c = :oppty.Id]);*/
            
            
        Test.stopTest();
    }

    @isTest static void test_method_three() {
Account acct = new Account(Name='Test Account',
                                        RecordTypeId='012i0000000FFqd',
                                        Type='Prospect');
        insert acct;
        Contact cont = new Contact(FirstName='Test',
                                        LastName='Test',
                                        RecordTypeId='012i0000000Dqvf',
                                        AccountId=acct.Id);
        insert cont;
        Opportunity opp = new Opportunity(Name=acct.Name + ' Opportunity',
                                        StageName='Identified',
                                        CloseDate=Date.valueOf('2017-04-01 00:00:00'),
                                        AccountId=acct.Id,
                                        Service_Type__c='Implementation',
                                        Service__c='Fixed Price Plus');
        insert opp;
        Milestone__c mstone = new Milestone__c(Type__c='Kickoff',
                                        Payment_Perc__c=10,
                                        Opportunity__c=opp.Id);
        insert mstone;
        Deliverable__c delv = new Deliverable__c(Name='D1',
                                        Type__c='Fixed Price',
                                        Opportunity__c=opp.Id,
                                        Deliverable_Overview__c='test',
                                        Status__c='Scoped');
        insert delv;
        OpportunityContactRole contRole = new OpportunityContactRole(ContactId=cont.Id,
                                        OpportunityId=opp.Id,
                                        Role='Decision Maker',
                                        IsPrimary=TRUE);
        insert contRole;
        ContentVersion doc = new ContentVersion(Title = 'Penguins',
                                        PathOnClient = 'Penguins.jpg',
                                        VersionData = Blob.valueOf('Test Content'),
                                        IsMajorVersion = true);
        insert doc;
        ContentVersion doc2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :doc.Id LIMIT 1];
        ContentDocumentLink cdLink = new ContentDocumentLink(ContentDocumentId=doc2.ContentDocumentId,
                                        LinkedEntityId=opp.Id,
                                        ShareType = 'V',
                                        Visibility = 'AllUsers');
        insert cdLink;


        // Perform test
        Test.startTest();
        Opportunity oppty = new Opportunity(id=opp.Id);
        oppty.StageName='Closed Won';
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Expected of Weeks to avoid validation
        oppty.Expected_of_Weeks__c = 1;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Hourly Rate for Closed Won opportunity to avoid validation
        oppty.Hourly_Rate__c = 10000;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Kickoff Type for Closed Won opportunity to avoid validation
        oppty.Kickoff_Type__c = 'No Kickoff';
        oppty.Delivery_Status__c='Approved';
        oppty.SOW_Date__c=System.today();
        oppty.Forecasted_Project_Lead__c ='005i0000000Nmyb';
        oppty.OwnerId ='005i0000000Nmyb';
        // System.debug(oppty);
        update oppty;
        /*Project__c testProject = new Project__c([select Id, Name, Opportunity__c from Project__c where Opportunity__c = :oppty.Id]);*/
            
            
        Test.stopTest();
    }


    @isTest static void test_method_four() {
Account acct = new Account(Name='Test Account',
                                        RecordTypeId='012i0000000FFqd',
                                        Type='Prospect');
        insert acct;
        Contact cont = new Contact(FirstName='Test',
                                        LastName='Test',
                                        RecordTypeId='012i0000000Dqvf',
                                        AccountId=acct.Id);
        insert cont;
        Opportunity opp = new Opportunity(Name=acct.Name + ' Opportunity',
                                        StageName='Identified',
                                        CloseDate=Date.valueOf('2017-07-01 00:00:00'),
                                        AccountId=acct.Id,
                                        Service_Type__c='Implementation',
                                        Service__c='Fixed Price Plus');
        insert opp;
        Milestone__c mstone = new Milestone__c(Type__c='Kickoff',
                                        Payment_Perc__c=10,
                                        Opportunity__c=opp.Id);
        insert mstone;
        Deliverable__c delv = new Deliverable__c(Name='D1',
                                        Type__c='Fixed Price',
                                        Opportunity__c=opp.Id,
                                        Deliverable_Overview__c='test',
                                        Status__c='Scoped');
        insert delv;
        OpportunityContactRole contRole = new OpportunityContactRole(ContactId=cont.Id,
                                        OpportunityId=opp.Id,
                                        Role='Decision Maker',
                                        IsPrimary=TRUE);
        insert contRole;
        ContentVersion doc = new ContentVersion(Title = 'Penguins',
                                        PathOnClient = 'Penguins.jpg',
                                        VersionData = Blob.valueOf('Test Content'),
                                        IsMajorVersion = true);
        insert doc;
        ContentVersion doc2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :doc.Id LIMIT 1];
        ContentDocumentLink cdLink = new ContentDocumentLink(ContentDocumentId=doc2.ContentDocumentId,
                                        LinkedEntityId=opp.Id,
                                        ShareType = 'V',
                                        Visibility = 'AllUsers');
        insert cdLink;


        // Perform test
        Test.startTest();
        Opportunity oppty = new Opportunity(id=opp.Id);
        oppty.StageName='Closed Won';
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Expected of Weeks to avoid validation
        oppty.Expected_of_Weeks__c = 1;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Hourly Rate for Closed Won opportunity to avoid validation
        oppty.Hourly_Rate__c = 10000;
        // 100719 - T - 00402 - VennScience_BFL_Amruta - Added Kickoff Type for Closed Won opportunity to avoid validation
        oppty.Kickoff_Type__c = 'No Kickoff';
        oppty.Delivery_Status__c='Approved';
        oppty.SOW_Date__c=System.today();
        oppty.Forecasted_Project_Lead__c ='005i0000000Nmyb';
        oppty.OwnerId ='005i0000000Nmyb';
        // System.debug(oppty);
        update oppty;
        /*Project__c testProject = new Project__c([select Id, Name, Opportunity__c from Project__c where Opportunity__c = :oppty.Id]);*/
            
            
        Test.stopTest();
    }
    
}