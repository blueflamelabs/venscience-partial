@isTest
private class createExpenseReports_Test {

    // Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().

    @testSetup
    static void setupTestData(){
        test.startTest();
        User user_Obj = new User(Username = 'TestUser7330720190424143759@codecoverage.com', LastName = 'LastName804', 
                                 Email = 'Email24@test.com', Alias = 'Alias913', CommunityNickname = 'cNickName61741', 
                                 IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq_AL', EmailEncodingKey = 'UTF-8', 
                                 ProfileId = '00ei0000000pfmZ', LanguageLocaleKey = 'en_US',Create_Expense_Reports__c = true, Create_Performance_Reviews__c = false);
        Insert user_Obj; 
        
        test.stopTest();
    }
    static testMethod void test_execute_UseCase(){
        List<User> user_Obj  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                 from User Where Username = 'TestUser7330720190424143759@codecoverage.com'];
        List<Expense_Report__c> expList = [SELECT Id 
                                              FROM Expense_Report__c
                                              WHERE OwnerId =: user_Obj[0].Id];
        test.startTest();
        createExpenseReports obj01 = new createExpenseReports();
        String CRON_EXP = '0 1 * * * ? 2022';
        String jobId = System.schedule('ScheduledApexTest',CRON_EXP, new createExpenseReports());
        System.assertEquals(2, expList.size());
        test.stoptest();
        
    }
    
    static testMethod void test_execute_UseCase1(){
        List<User> user_Obj  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                 from User Where Username = 'TestUser7330720190424143759@codecoverage.com'];
        test.startTest();
        List<Expense_Report__c> expList = [SELECT Id 
                                              FROM Expense_Report__c
                                              WHERE OwnerId =: user_Obj[0].Id];
        Delete expList[0];
        
        createExpenseReports obj01 = new createExpenseReports();
        String CRON_EXP = '0 1 * * * ? 2022';
        String jobId = System.schedule('ScheduledApexTest1',CRON_EXP, new createExpenseReports());
        System.assertEquals(2, expList.size());
        test.stoptest();
        
    }
    
    static testMethod void test_execute_UseCase2(){
        List<User> user_Obj  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                 from User Where Username = 'TestUser7330720190424143759@codecoverage.com'];
        test.startTest();
        List<Expense_Report__c> expList = [SELECT Id 
                                              FROM Expense_Report__c
                                              WHERE OwnerId =: user_Obj[0].Id];
        Delete expList[1];
        
        createExpenseReports obj01 = new createExpenseReports();
        String CRON_EXP = '0 1 * * * ? 2022';
        String jobId = System.schedule('ScheduledApexTest2',CRON_EXP, new createExpenseReports());
        System.assertEquals(2, expList.size());
        test.stoptest();
        
    }
    
    static testMethod void test_execute_UseCase3(){
        List<User> user_Obj  =  [SELECT Username,LastName,Email,Alias,CommunityNickname,IsActive,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ProfileId,LanguageLocaleKey,Create_Expense_Reports__c,Create_Performance_Reviews__c 
                                 from User Where Username = 'TestUser7330720190424143759@codecoverage.com'];
        test.startTest();
        List<Expense_Report__c> expList = [SELECT Id 
                                              FROM Expense_Report__c
                                              WHERE OwnerId =: user_Obj[0].Id];
        Delete expList;
        
        createExpenseReports obj01 = new createExpenseReports();
        String CRON_EXP = '0 1 * * * ? 2022';
        String jobId = System.schedule('ScheduledApexTest3',CRON_EXP, new createExpenseReports());
        System.assertEquals(2, expList.size());
        test.stoptest();
        
    }

	
}