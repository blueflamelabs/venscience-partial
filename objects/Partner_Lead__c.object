<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Partner_Lead_Lightning_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>Closed_Won_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Closed_Won_Compact_Layout</fullName>
        <fields>PL__c</fields>
        <fields>Type__c</fields>
        <fields>ACV__c</fields>
        <fields>PVS_Expiration_Date__c</fields>
        <fields>Partner_Community_Close_Date__c</fields>
        <fields>Account_Executive__c</fields>
        <fields>Partner_Alliance_Manager__c</fields>
        <label>Closed Won Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>PL#
Partner Oppty Name
Customer Account
Partner Account
Oppty
Project
Status (Identified, Submitted, Accepted, Rejected, Closed Won, Closed Lost)
Type (Sourced Revenue, Joint Sales)</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>ACV__c</fullName>
        <externalId>false</externalId>
        <label>ACV</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Account_Executive__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Used to track the Salesforce AE responsible for closing this deal.</inlineHelpText>
        <label>Account Executive</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Partner Leads</relationshipLabel>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Customer_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The prospect or client this lead is submitted for.</inlineHelpText>
        <label>Customer Account</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.Type</field>
                <operation>equals</operation>
                <value>Prospect, Client, Former Client</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Partner Leads (Customer Account)</relationshipLabel>
        <relationshipName>Partner_Leads1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Customer_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Customer Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Partner Leads (Customer Contact)</relationshipLabel>
        <relationshipName>Partner_Leads1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Goals__c</fullName>
        <defaultValue>&quot;1. What are the customers&apos; technical requirements?&quot; &amp;BR()&amp;
&quot;2. What are the customer&apos;s business requirements?&quot; &amp;BR()&amp;
&quot;3. What solution is the customer using now?&quot; &amp;BR()&amp;
&quot;4. What other solutions are the customer evaluating?&quot; &amp;BR()</defaultValue>
        <externalId>false</externalId>
        <label>Goals</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Next_Steps__c</fullName>
        <externalId>false</externalId>
        <label>Next Steps</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>PL_Created_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>PL Created By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PL__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Salesforce&apos;s Partner Lead Number (e.g. PL-01234567)</inlineHelpText>
        <label>PL#</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PVS_Expiration_Date__c</fullName>
        <externalId>false</externalId>
        <formula>ADDMONTHS( Partner_Community_Close_Date__c ,12)</formula>
        <label>PVS Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Pains__c</fullName>
        <externalId>false</externalId>
        <label>Pains</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Partner_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The VennScience Partner this Lead is for (e.g. Salesforce)</inlineHelpText>
        <label>Partner Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please select an Account with Type set to Partner.</errorMessage>
            <filterItems>
                <field>Account.Type</field>
                <operation>equals</operation>
                <value>Partner</value>
            </filterItems>
            <infoMessage>Partner Accounts</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Partner Leads</relationshipLabel>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Partner_Alliance_Action_Item__c</fullName>
        <externalId>false</externalId>
        <label>Partner Alliance Action Item</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Partner_Alliance_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Partner Alliance Manager</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Partner Leads (Partner Alliance Manager)</relationshipLabel>
        <relationshipName>Partner_Leads2</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Partner_Alliance_Status__c</fullName>
        <defaultValue>&apos;No Review Needed&apos;</defaultValue>
        <externalId>false</externalId>
        <label>Partner Alliance Status</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>No Review Needed</fullName>
                    <default>false</default>
                    <label>No Review Needed</label>
                </value>
                <value>
                    <fullName>Needs Review</fullName>
                    <default>false</default>
                    <label>Needs Review</label>
                </value>
                <value>
                    <fullName>Working</fullName>
                    <default>false</default>
                    <label>Working</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Partner_Community_Close_Date__c</fullName>
        <externalId>false</externalId>
        <label>Partner Community Close Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Partner_Community_Created_Date__c</fullName>
        <externalId>false</externalId>
        <label>Partner Community Created Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Partner_Opportunity_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Salesforce&apos;s Opportunity Name</inlineHelpText>
        <label>Partner Opportunity Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Partner_Sales_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Partner Sales Notes</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Potential_Sale_Value__c</fullName>
        <externalId>false</externalId>
        <label>Potential Sale Value</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Less than $5,000</fullName>
                    <default>false</default>
                    <label>Less than $5,000</label>
                </value>
                <value>
                    <fullName>$5,001 - $25,000</fullName>
                    <default>false</default>
                    <label>$5,001 - $25,000</label>
                </value>
                <value>
                    <fullName>$25,001 - $50,000</fullName>
                    <default>false</default>
                    <label>$25,001 - $50,000</label>
                </value>
                <value>
                    <fullName>$50,001 - $100,000</fullName>
                    <default>false</default>
                    <label>$50,001 - $100,000</label>
                </value>
                <value>
                    <fullName>More than $100,001</fullName>
                    <default>false</default>
                    <label>More than $100,001</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Primary_Product_of_Interest__c</fullName>
        <externalId>false</externalId>
        <label>Primary Product of Interest</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Analytics Cloud</fullName>
                    <default>false</default>
                    <label>Analytics Cloud</label>
                </value>
                <value>
                    <fullName>B2B Commerce</fullName>
                    <default>false</default>
                    <label>B2B Commerce</label>
                </value>
                <value>
                    <fullName>Chatter</fullName>
                    <default>false</default>
                    <label>Chatter</label>
                </value>
                <value>
                    <fullName>Commerce Cloud</fullName>
                    <default>false</default>
                    <label>Commerce Cloud</label>
                </value>
                <value>
                    <fullName>Community Cloud</fullName>
                    <default>false</default>
                    <label>Community Cloud</label>
                </value>
                <value>
                    <fullName>CPQ</fullName>
                    <default>false</default>
                    <label>CPQ</label>
                </value>
                <value>
                    <fullName>Data.com</fullName>
                    <default>false</default>
                    <label>Data.com</label>
                </value>
                <value>
                    <fullName>Data Studio</fullName>
                    <default>false</default>
                    <label>Data Studio</label>
                </value>
                <value>
                    <fullName>Desk.com</fullName>
                    <default>false</default>
                    <label>Desk.com</label>
                </value>
                <value>
                    <fullName>Financial Services Cloud</fullName>
                    <default>false</default>
                    <label>Financial Services Cloud</label>
                </value>
                <value>
                    <fullName>Health Cloud</fullName>
                    <default>false</default>
                    <label>Health Cloud</label>
                </value>
                <value>
                    <fullName>Heroku</fullName>
                    <default>false</default>
                    <label>Heroku</label>
                </value>
                <value>
                    <fullName>Marketing Cloud</fullName>
                    <default>false</default>
                    <label>Marketing Cloud</label>
                </value>
                <value>
                    <fullName>Pardot</fullName>
                    <default>false</default>
                    <label>Pardot</label>
                </value>
                <value>
                    <fullName>Quip</fullName>
                    <default>false</default>
                    <label>Quip</label>
                </value>
                <value>
                    <fullName>Sales Cloud</fullName>
                    <default>false</default>
                    <label>Sales Cloud</label>
                </value>
                <value>
                    <fullName>Salesforce.org HEDA</fullName>
                    <default>false</default>
                    <label>Salesforce.org HEDA</label>
                </value>
                <value>
                    <fullName>Salesforce.org NPSP</fullName>
                    <default>false</default>
                    <label>Salesforce.org NPSP</label>
                </value>
                <value>
                    <fullName>Salesforce1 Platform</fullName>
                    <default>false</default>
                    <label>Salesforce1 Platform</label>
                </value>
                <value>
                    <fullName>Salesforce DMP</fullName>
                    <default>false</default>
                    <label>Salesforce DMP</label>
                </value>
                <value>
                    <fullName>Salesforce Platform</fullName>
                    <default>false</default>
                    <label>Salesforce Platform</label>
                </value>
                <value>
                    <fullName>Service Cloud</fullName>
                    <default>false</default>
                    <label>Service Cloud</label>
                </value>
                <value>
                    <fullName>Work.com</fullName>
                    <default>false</default>
                    <label>Work.com</label>
                </value>
                <value>
                    <fullName>AppCloud</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>AppCloud</label>
                </value>
                <value>
                    <fullName>Foundation - NGO Connect</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Foundation - NGO Connect</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Purchasing_Timeframe__c</fullName>
        <externalId>false</externalId>
        <label>Purchasing Timeframe</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Immediate</fullName>
                    <default>false</default>
                    <label>Immediate</label>
                </value>
                <value>
                    <fullName>1-3 months</fullName>
                    <default>false</default>
                    <label>1-3 months</label>
                </value>
                <value>
                    <fullName>3-6 months</fullName>
                    <default>false</default>
                    <label>3-6 months</label>
                </value>
                <value>
                    <fullName>6-12 months</fullName>
                    <default>false</default>
                    <label>6-12 months</label>
                </value>
                <value>
                    <fullName>12+ months</fullName>
                    <default>false</default>
                    <label>12+ months</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Revenue_Share_Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Used to track a revenue share or referral bonus to be paid by the vendor to VennScience.</inlineHelpText>
        <label>Revenue Share Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Unpaid</fullName>
                    <default>false</default>
                    <label>Unpaid</label>
                </value>
                <value>
                    <fullName>Paid</fullName>
                    <default>false</default>
                    <label>Paid</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Revenue_Share__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Used to track a revenue share or referral bonus to be paid by the vendor to VennScience.</inlineHelpText>
        <label>Revenue Share</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <defaultValue>&apos;Identified&apos;</defaultValue>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Identified</fullName>
                    <default>false</default>
                    <label>Identified</label>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                    <label>Submitted</label>
                </value>
                <value>
                    <fullName>Open</fullName>
                    <default>false</default>
                    <label>Open</label>
                </value>
                <value>
                    <fullName>Lead Rejected</fullName>
                    <default>false</default>
                    <label>Lead Rejected</label>
                </value>
                <value>
                    <fullName>Closed Won</fullName>
                    <default>false</default>
                    <label>Closed Won</label>
                </value>
                <value>
                    <fullName>Closed Lost</fullName>
                    <default>false</default>
                    <label>Closed Lost</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Total_Potential_Users__c</fullName>
        <externalId>false</externalId>
        <label>Total Potential Users</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1-5</fullName>
                    <default>false</default>
                    <label>1-5</label>
                </value>
                <value>
                    <fullName>6-20</fullName>
                    <default>false</default>
                    <label>6-20</label>
                </value>
                <value>
                    <fullName>21-100</fullName>
                    <default>false</default>
                    <label>21-100</label>
                </value>
                <value>
                    <fullName>101-500</fullName>
                    <default>false</default>
                    <label>101-500</label>
                </value>
                <value>
                    <fullName>501-3500</fullName>
                    <default>false</default>
                    <label>501-3500</label>
                </value>
                <value>
                    <fullName>3500+</fullName>
                    <default>false</default>
                    <label>3500+</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Joint Sales</fullName>
                    <default>false</default>
                    <label>Joint Sales</label>
                </value>
                <value>
                    <fullName>Sourced Revenue</fullName>
                    <default>true</default>
                    <label>Sourced Revenue</label>
                </value>
                <value>
                    <fullName>*Credit Required</fullName>
                    <default>false</default>
                    <label>*Credit Required</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VennScience_Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>VennScience Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>Partner Leads</relationshipLabel>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VennScience_Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>VennScience Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Partner Leads</relationshipLabel>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VennScience_Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>VennScience Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Partner Leads</relationshipLabel>
        <relationshipName>Partner_Leads</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Working_with_a_Salesforce_AE__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>If yes, see Account Executive lookup field.</inlineHelpText>
        <label>Working with a Salesforce AE?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Partner Lead</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Status__c</columns>
        <columns>PL__c</columns>
        <columns>Customer_Account__c</columns>
        <columns>Partner_Opportunity_Name__c</columns>
        <columns>Partner_Alliance_Manager__c</columns>
        <columns>Partner_Alliance_Status__c</columns>
        <columns>Account_Executive__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>Type__c</columns>
        <columns>NAME</columns>
        <columns>Partner_Sales_Notes__c</columns>
        <columns>Description__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Open_Partner_Leads</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Type__c</columns>
        <columns>PL__c</columns>
        <columns>Partner_Opportunity_Name__c</columns>
        <columns>Partner_Alliance_Manager__c</columns>
        <columns>Account_Executive__c</columns>
        <columns>Customer_Account__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Identified,Rejected,Closed Lost</value>
        </filters>
        <label>3: Open Partner Leads</label>
    </listViews>
    <listViews>
        <fullName>Partner_Alliance_Review_Needed</fullName>
        <booleanFilter>1 OR 2</booleanFilter>
        <columns>NAME</columns>
        <columns>Partner_Alliance_Manager__c</columns>
        <columns>Partner_Alliance_Status__c</columns>
        <columns>Partner_Alliance_Action_Item__c</columns>
        <columns>PL__c</columns>
        <columns>Partner_Opportunity_Name__c</columns>
        <columns>Type__c</columns>
        <columns>Account_Executive__c</columns>
        <columns>Customer_Account__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Type__c</field>
            <operation>equals</operation>
            <value>*Credit Required</value>
        </filters>
        <filters>
            <field>Partner_Alliance_Status__c</field>
            <operation>equals</operation>
            <value>Needs Review,Working</value>
        </filters>
        <label>2: Partner Alliance Review Needed</label>
    </listViews>
    <listViews>
        <fullName>Sales_Ops_Submission_Needed</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>Account_Executive__c</columns>
        <columns>Customer_Account__c</columns>
        <columns>VennScience_Opportunity__c</columns>
        <columns>VennScience_Project__c</columns>
        <columns>VennScience_Lead__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Identified</value>
        </filters>
        <label>1: Sales Ops Submission Needed</label>
    </listViews>
    <listViews>
        <fullName>Won_Partner_Leads_Current_FY</fullName>
        <columns>NAME</columns>
        <columns>PL__c</columns>
        <columns>Partner_Opportunity_Name__c</columns>
        <columns>ACV__c</columns>
        <columns>PVS_Expiration_Date__c</columns>
        <columns>Type__c</columns>
        <columns>Partner_Alliance_Manager__c</columns>
        <columns>Account_Executive__c</columns>
        <columns>Customer_Account__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </filters>
        <filters>
            <field>Partner_Community_Close_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>2/1/2018</value>
        </filters>
        <filters>
            <field>Partner_Community_Close_Date__c</field>
            <operation>lessThan</operation>
            <value>2/1/2019</value>
        </filters>
        <label>4: Won Partner Leads (Current FY)</label>
    </listViews>
    <listViews>
        <fullName>Won_Partner_Leads_Prior_FY</fullName>
        <columns>NAME</columns>
        <columns>PL__c</columns>
        <columns>Partner_Opportunity_Name__c</columns>
        <columns>ACV__c</columns>
        <columns>PVS_Expiration_Date__c</columns>
        <columns>Type__c</columns>
        <columns>Partner_Alliance_Manager__c</columns>
        <columns>Account_Executive__c</columns>
        <columns>Customer_Account__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </filters>
        <filters>
            <field>Partner_Community_Close_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>2/1/2017</value>
        </filters>
        <filters>
            <field>Partner_Community_Close_Date__c</field>
            <operation>lessThan</operation>
            <value>2/1/2018</value>
        </filters>
        <label>5: Won Partner Leads (Prior FY)</label>
    </listViews>
    <nameField>
        <displayFormat>VSCIPL-{00000}</displayFormat>
        <label>Partner Lead #</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Partner Leads</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PL__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner_Opportunity_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>VennScience_Opportunity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>VennScience_Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
